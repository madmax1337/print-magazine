﻿using System;

namespace PrintMagazine.PresentationLayer.Helpers
{
#pragma warning disable CS1591
    public static class JwtOption
    {
        public static TimeSpan AccessTokenExpiration { get; set; } = TimeSpan.FromMinutes(15);
        public static TimeSpan RefreshTokenExpiration { get; set; } = TimeSpan.FromDays(30);
    }
#pragma warning restore CS1591
}

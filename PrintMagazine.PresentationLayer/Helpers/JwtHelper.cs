﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PrintMagazine.BusinessLogicLayer.Models.Token;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using busineslogic = PrintMagazine.BusinessLogicLayer.Models.Option;

namespace PrintMagazine.PresentationLayer.Helpers
{
#pragma warning disable CS1591
    public class JwtHelper : IJwtHelper
    {
        private readonly busineslogic.JwtOption _jwtOption;

        public JwtHelper(IOptions<busineslogic.JwtOption> jwtOption)
        {
            _jwtOption = jwtOption.Value;
        }
        public static SymmetricSecurityKey GetSymmetricSecurityKey(string key)
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key));
        }

        public TokenModel UserToken(string userId, string email, string role)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return null;
            }

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, userId),
                new Claim(ClaimTypes.Email, email),
                new Claim(ClaimTypes.Role, role)
            };

            var expire = DateTime.Now.Add(JwtOption.AccessTokenExpiration);

            var accessToken = GenerationToken(claims, expire);

            claims = new List<Claim>
            {
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.NameIdentifier, userId),
                    new Claim(ClaimTypes.Email, email),
                    new Claim(ClaimTypes.Role, role)
            };

            expire = DateTime.Now.Add(JwtOption.RefreshTokenExpiration);

            var refreshToken = GenerationToken(claims, expire);

            var tokenModel = new TokenModel
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken
            };

            return tokenModel;
        }

        private string GenerationToken(List<Claim> claims, DateTime expire)
        {
            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(issuer: _jwtOption.Issues,
                                           audience: _jwtOption.Audience,
                                           claims: claims,
                                           expires: expire,
                                           signingCredentials: new SigningCredentials(GetSymmetricSecurityKey(_jwtOption.JwtKey), SecurityAlgorithms.HmacSha256));

            var someToken = new JwtSecurityTokenHandler().WriteToken(jwt);

            return someToken;
        }
    }
#pragma warning restore CS1591
}

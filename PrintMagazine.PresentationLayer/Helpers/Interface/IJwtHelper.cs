﻿using PrintMagazine.BusinessLogicLayer.Models.Token;

namespace PrintMagazine.PresentationLayer.Helpers
{
#pragma warning disable CS1591
    public interface IJwtHelper
    {
        TokenModel UserToken(string userId, string email, string role);
    }
#pragma warning restore CS1591
}

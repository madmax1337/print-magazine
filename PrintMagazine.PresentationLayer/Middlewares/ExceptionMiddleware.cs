﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace PrintMagazine.BusinessLogicLayer.Common
{
#pragma warning disable CS1591
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ExceptionMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            this._next = next;
            this._logger = loggerFactory.CreateLogger<ExceptionMiddleware>();
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception exception)
            {
                ExceptionAsync(exception);
            }
        }

        public void ExceptionAsync(Exception exception)
        {
            if (exception is ArgumentException argumentException)
            {
                _logger.LogError(0, argumentException, argumentException.Message);
            }
            if (exception is InvalidOperationException invalidOperationException)
            {
                _logger.LogError(0, invalidOperationException, invalidOperationException.Message);
            }
            if (exception is SqlException sqlException)
            {
                _logger.LogError(0, sqlException, sqlException.Message);
            }
            if (exception is NullReferenceException nullReferenceException)
            {
                _logger.LogError(0, nullReferenceException, nullReferenceException.Message);
            }
            if (exception is Exception)
            {
                _logger.LogError(0, exception, exception.Message);
            }
        }
    }
#pragma warning restore CS1591
}

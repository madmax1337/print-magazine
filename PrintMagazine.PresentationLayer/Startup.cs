﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PrintMagazine.BusinessLogicLayer.Common;
using System.IO;
using PrintMagazine.PresentationLayer.Helpers;
using PrintMagazine.DataAccessLayer.Initializations;
using PrintMagazine.PresentationLayer.Extension.Swagger;
using PrintMagazine.PresentationLayer.Extensions.Cors;
using PrintMagazine.BusinessLogicLayer.Extensions.Authentication;
using PrintMagazine.BusinessLogicLayer.Extension;

using PrintMagazine.PresentationLayer.Exstension.Option;

namespace PrintMagazine.PresentationLayer
{
#pragma warning disable CS1591
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddServices(Configuration);
            services.AddSwagger();
            services.AddAuthenticationToken(Configuration);
            services.AddScoped<IJwtHelper, JwtHelper>();
            services.AddCors(Configuration);
            services.AddEmailOption(Configuration);
            services.AddJwtOption(Configuration);
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, DatabaseInitialization databaseInitialization)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            } 

            app.UseMiddleware<ExceptionMiddleware>();
            databaseInitialization.InitializationAsync();
            app.UseCors("OriginPolicy");
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {   
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseStatusCodePages();
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            loggerFactory.AddFile(Path.Combine(Directory.GetCurrentDirectory(), "logger.txt"));
            var logger = loggerFactory.CreateLogger("FileLogger");
        }
    }
#pragma warning restore CS1591
}

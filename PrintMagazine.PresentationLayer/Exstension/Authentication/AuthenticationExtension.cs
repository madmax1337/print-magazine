﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using PrintMagazine.PresentationLayer.Helpers;

namespace PrintMagazine.BusinessLogicLayer.Extensions.Authentication
{
    public static class AuthenticationExtension
    {
        public static void AddAuthenticationToken(this IServiceCollection services, IConfiguration configuration)
        {

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = configuration.GetSection("JwtOption").GetSection("Issues").Value,
                        ValidateAudience = true,
                        ValidAudience = configuration.GetSection("JwtOption").GetSection("Audience").Value,
                        ValidateLifetime = true,
                        IssuerSigningKey = JwtHelper.GetSymmetricSecurityKey(configuration.GetSection("JwtOption").GetSection("JwtKey").Value),
                        ValidateIssuerSigningKey = false
                    };
                });
        }
    }
}

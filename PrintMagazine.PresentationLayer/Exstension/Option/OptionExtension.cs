﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PrintMagazine.BusinessLogicLayer.Models.Option;

namespace PrintMagazine.PresentationLayer.Exstension.Option
{
    public static class OptionExtension
    {
        public static void AddEmailOption(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<EmailOption>(configuration.GetSection("EmailOption"));
        }

        public static void AddJwtOption(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<JwtOption>(configuration.GetSection("JwtOption"));
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace PrintMagazine.PresentationLayer.Extensions.Cors
{
    public static class CorsExtension
    {
        public static void AddCors(this IServiceCollection service, IConfiguration configuration)
        {
            var origins = configuration.GetSection("Cors").GetSection("Origins").Value;

            service.AddCors(option=>
            option.AddPolicy("OriginPolicy", b=>b.WithOrigins(origins)
                                                 .AllowAnyHeader()
                                                 .AllowAnyMethod()
                                                 .AllowCredentials())
            );
        }
    }
}

﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PrintMagazine.BusinessLogicLayer.Models.Filter;
using PrintMagazine.BusinessLogicLayer.Models.User;
using PrintMagazine.BusinessLogicLayer.Services.Interfeces;

namespace PrintMagazine.PresentationLayer.Controllers
{
#pragma warning disable CS1591
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody]UserModelItem userModelItem)
        {
            var result = await _userService.UpdateAsync(User, userModelItem);
            return Ok(result);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete([FromBody]UserModelItem userModelItem)
        {
            var result = await _userService.UpdateAsync(User, userModelItem);
            return Ok(result);
        }

        [HttpPost("setLockout")]
        public async Task<IActionResult> SetLocout([FromBody]UserModelItem userModelItem)
        {
            var result = await _userService.SetStatusAsync(userModelItem);
            return Ok(result);
        }


        [HttpPost("viewAll")]
        public async Task<IActionResult> ViewAll([FromBody]UserFilterModel userFilterModel)
        {
            var userModel =await _userService.GetAllAsync(userFilterModel);
            return Ok(userModel);
        }
    }
#pragma warning restore CS1591
}
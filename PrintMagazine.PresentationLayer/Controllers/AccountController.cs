﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PrintMagazine.BusinessLogicLayer.Constant;
using PrintMagazine.BusinessLogicLayer.Helpers.Interfaces;
using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.BusinessLogicLayer.Models.PasswordModels;
using PrintMagazine.BusinessLogicLayer.Models.SingInModel;
using PrintMagazine.BusinessLogicLayer.Models.SingUpModel;
using PrintMagazine.BusinessLogicLayer.Models.Token;
using PrintMagazine.BusinessLogicLayer.Services.Interfeces;
using PrintMagazine.PresentationLayer.Helpers;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PrintMagazine.PresentationLayer.Controllers
{
#pragma warning disable CS1591
    [ApiController]
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IJwtHelper _jwtHelper;
        private readonly IPasswordHelper _passwordHelper;

        public AccountController(IAccountService accountService, IJwtHelper jwtHelper, IPasswordHelper passwordHelper)
        {
            _accountService = accountService;
            _jwtHelper = jwtHelper;
            _passwordHelper = passwordHelper;
        }

        [HttpPost("signIn")]
        public async Task<IActionResult> SignIn([FromBody]SignInModel signInModel)
        {
            var userModel = await _accountService.SignInAsync(signInModel.Email, signInModel.Password);

            if (userModel.Errors.Any())
            {
                return Ok(userModel);
            }

            var tokenModel = _jwtHelper.UserToken(userModel.UserId, signInModel.Email, userModel.Role);

            AddToCookie(tokenModel);

            return Ok(userModel);
        }

        [HttpPost("signUp")]
        public async Task<IActionResult> SignUp([FromBody]SignUpModel signUpModel)
        {
            var confirmEmailModel = await _accountService.SignUpAsync(signUpModel);

            if (confirmEmailModel.Errors.Any())
            {
                return Ok(confirmEmailModel); 
            }

            var callbackUrl = @Url.Action("confirmEmail", "account", new { userId = confirmEmailModel.Id, code = confirmEmailModel.Code }, protocol: HttpContext.Request.Scheme);

            var emailError = await _accountService.EmailSenderAsync(callbackUrl, signUpModel.Email, "");

            return Ok(emailError);
        }

        [HttpPost("forgotPassword")]

        public async Task<IActionResult> ForgotPassword([FromBody]ForgotPasswordModel forgotPasswordModel)
        {
            var code = _passwordHelper.PasswordHelper();

            var baseModel = await _accountService.ForgotPasswordAsync(forgotPasswordModel.Email, code);

            if (baseModel.Errors.Any())
            {
                return Ok(baseModel);
            }

            baseModel = await _accountService.EmailSenderAsync("", forgotPasswordModel.Email, code);

            return Ok(baseModel);
        }

        [HttpGet("logOut")]
        [Authorize]
        public async Task<IActionResult> LogOut()
        {
            var baseModel = new BaseModel();
            await _accountService.LogOutAsync();
            HttpContext.Session.Clear();
            return Ok(baseModel);
        }

        [HttpGet("confirmEmail")]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            var baseModel = await _accountService.ConfirmEmailAsync(userId, code);

            var url = "http://localhost:4200/account/confirmEmail";

            if (baseModel.Errors.Any())
            {
                url += $"errors= {baseModel.Errors.FirstOrDefault()}";
            }

            return Redirect(url);
        }

        [HttpGet("refreshToken")]
        public async Task<IActionResult> RefreshToken()
        {
            var refreshToken = Request.Cookies["refreshToken"];

            var tokenModel = new TokenModel();
            var baseModel = new BaseModel();

            var savedRefreshToken = new JwtSecurityTokenHandler().ReadJwtToken(refreshToken);

            var userId = savedRefreshToken.Claims.FirstOrDefault(claim => claim.Type.Equals(ClaimTypes.NameIdentifier)).Value;
            var role = savedRefreshToken.Claims.FirstOrDefault(claim => claim.Type.Equals(ClaimTypes.Role)).Value;
            var email = savedRefreshToken.Claims.FirstOrDefault(claim => claim.Type.Equals(ClaimTypes.Email)).Value;

            if (savedRefreshToken.ValidTo < DateTime.Now)
            {
                baseModel.Errors.Add(Constants.Errors.TokenError);
                return Ok(baseModel);
            }

            tokenModel =  _jwtHelper.UserToken(userId, email, role);

            AddToCookie(tokenModel);

            return Ok(baseModel);
        }

        private void AddToCookie(TokenModel tokenModel)
        {
            Response.Cookies.Append("accessToken", tokenModel.AccessToken, new CookieOptions()
            {
                Expires = DateTime.Now.Add(JwtOption.AccessTokenExpiration)
            });

            Response.Cookies.Append("refreshToken", tokenModel.RefreshToken, new CookieOptions()
            {
                Expires = DateTime.Now.Add(JwtOption.RefreshTokenExpiration)
            });
        }
    }
#pragma warning restore CS1591
}
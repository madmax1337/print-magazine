﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PrintMagazine.BusinessLogicLayer.Models.Filter;
using PrintMagazine.BusinessLogicLayer.Models.PrintingEditions;
using PrintMagazine.BusinessLogicLayer.Services.Interfeces;
using PrintMagazine.DataAccessLayer.Entities.Enum;

namespace PrintMagazine.PresentationLayer.Controllers
{
#pragma warning disable CS1591
    [ApiController]
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class PrintingEditionController : Controller
    {
        private readonly IPrintingEditionService _printingEditionService;

        public PrintingEditionController(IPrintingEditionService printingEditionService)
        {
            _printingEditionService = printingEditionService;
        }

        [HttpPost("create")]
        [Authorize]
        public async Task<IActionResult> Create([FromBody]PrintingEditionModelItem printingEditionModelItem)
        {
            var printingEditionModel = await _printingEditionService.AddItemsAsync(printingEditionModelItem);
            return Ok(printingEditionModel);
        }

        [HttpPost("update")]
        [Authorize]
        public async Task<IActionResult> Update([FromBody]PrintingEditionModelItem printingEditionModelItem)
        {
            var printingEditionModel = await _printingEditionService.UpdateAsync(printingEditionModelItem);
            return Ok(printingEditionModel);
        }

        [HttpPost("delete")]
        [Authorize]
        public async Task<IActionResult> Delete([FromBody]PrintingEditionModelItem printingEditionModelItem)
        {
            var printingEditionModel = await _printingEditionService.UpdateAsync(printingEditionModelItem);
            return Ok(printingEditionModel); 
        }

        [HttpPost("viewAll")]
        public async Task<IActionResult> ViewAll([FromBody]PrintingEditionFilterModel printingEditionFilterModel)
        {
            var printingEditionModel = await _printingEditionService.GetPrintingEditionModel(printingEditionFilterModel);
            return Ok(printingEditionModel);
        }

        [HttpGet("getInformation")]
        public async Task<IActionResult> GetInformation(long printingEditionId, Curency curency)
        {
            var printingEditionModelItem = await _printingEditionService.GetInfoPrintingEdition(printingEditionId, curency);
            return Ok(printingEditionModelItem);
        }
    }
#pragma warning restore CS1591
}
﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PrintMagazine.BusinessLogicLayer.Models.Authors;
using PrintMagazine.BusinessLogicLayer.Models.Filter;
using PrintMagazine.BusinessLogicLayer.Services.Interfeces;

namespace PrintMagazine.PresentationLayer.Controllers.Base
{
#pragma warning disable CS1591
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _authorService;

        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody]AuthorModelItem authorModelItem)
        {
            var author = await _authorService.UpdateAsync(authorModelItem);
            return Ok(author);
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody]AuthorModelItem authorModelItem)
        {
            var author = await _authorService.CreateAsync(authorModelItem);
            return Ok(author);
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete([FromBody]AuthorModelItem authorModelItem)
        {
            var author = await _authorService.UpdateAsync(authorModelItem);
            return Ok(author);
        }
        [HttpPost("viewAll")]
        public async Task<IActionResult> ViewAll([FromBody]AuthorFilterModel authorFilterModel)
        {
            var authorModel =  await _authorService.GetAllAsync(authorFilterModel);
            return Ok(authorModel);
        }
        [HttpGet("authorForPrintingEdition")]
        public async Task<IActionResult> AuthorForPrintingEdition()
        {
            var authorModel = await _authorService.GetAuthorForPrintingEdition();
            return Ok(authorModel);
        }
    }
#pragma warning restore CS1591
}
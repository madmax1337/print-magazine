﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PrintMagazine.BusinessLogicLayer.Models.Cart;
using PrintMagazine.BusinessLogicLayer.Models.Filter;
using PrintMagazine.BusinessLogicLayer.Models.Order;
using PrintMagazine.BusinessLogicLayer.Services.Interfeces;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PrintMagazine.PresentationLayer.Controllers
{
#pragma warning disable CS1591
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;
        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost("buyPrintingEdition")]
        public async Task<IActionResult> BuyPrintingEdition([FromBody]CartModel cartModel)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type.Equals(ClaimTypes.NameIdentifier)).Value;

            var orderModedlItem = await _orderService.CreateOrderAsync(cartModel, userId);

            return Ok(orderModedlItem);
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody]OrderModelItem orderModelItem)
        {
            var baseModel = await _orderService.UpdateAsync(orderModelItem.OrderId, orderModelItem.TransactionId);
            return Ok(baseModel);
        }

        [HttpPost("viewAll")]
        public async Task<IActionResult> ViewAll([FromBody]OrderFilterModel orderFilterModel)
        {
            var orderModel = await _orderService.GetAllAsync(orderFilterModel);
            return Ok(orderModel);
        }
    }
#pragma warning restore CS1591
}
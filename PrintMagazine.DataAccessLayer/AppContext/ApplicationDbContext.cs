﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Entities.Base;

namespace PrintMagazine.DataAccessLayer.AppContext
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser,UserRole, string>
    {
        public DbSet<Author> Authors { get; set; }
        public DbSet<AuthorInPrintedEdition> AuthorInPrintedEditions { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PrintingEdition> PrintingEditions { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> option):base(option)
        {
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Author>().Property(f => f.Id).ValueGeneratedOnAdd();
            base.OnModelCreating(builder);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using PrintMagazine.DataAccessLayer.AppContext;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using PrintMagazine.DataAccessLayer.Model.PrtintingEdition;
using PrintMagazine.DataAccessLayer.Repositories.Base;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Repositories.EFRepositories
{
    public class AuthorsInPrintedEditionsRepository : BaseEFRepository<AuthorInPrintedEdition>, IAuthorsInPrintedEditionsRepsitory
    {
        public AuthorsInPrintedEditionsRepository(ApplicationDbContext dbContext):base(dbContext)
        {

        }

        public async Task<string> DeleteItemsAsync(long printingEditionId, IEnumerable<PrintingEditionAuthor> authors)
        {
            var authorsInPrintedEditions = new List<AuthorInPrintedEdition>();

            var stringBuilder = new StringBuilder();

            var query = _dbSet.Where(x => x.PrintingEditionsId.Equals(printingEditionId))
                              .Include(x => x.PrintingEditions)
                              .Include(x => x.Author)
                              .GroupBy(x => x.Author)
                              .Select(x=>x.Key)
                              .OrderBy(x => x.Id)
                              .Select(x => x.Id)
                              .ToList();

            var authorIds = authors.OrderBy(x => x.AuthorId).Select(x => x.AuthorId).ToList();

            var result = query.SequenceEqual(authorIds);

            if (result)
            {
                return string.Empty;
            }

            authorsInPrintedEditions = await _dbSet.Where(x => x.PrintingEditionsId.Equals(printingEditionId)).ToListAsync();

            foreach (var item in authorsInPrintedEditions)
            {
                var resultError = await RemoveAsync(item);

                if (!resultError)
                {
                    stringBuilder.Append(item.Id);
                }
            }

            foreach (var item in authors)
            {
                var authorInPrintingEdition = new AuthorInPrintedEdition();

                authorInPrintingEdition.PrintingEditionsId = printingEditionId;
                authorInPrintingEdition.AuthorId = item.AuthorId;

                result = await CreateAsync(authorInPrintingEdition);

                if (!result)
                {
                    stringBuilder.Append(authorInPrintingEdition.Id);
                }
            }

            return stringBuilder.ToString();
        }
    }
}

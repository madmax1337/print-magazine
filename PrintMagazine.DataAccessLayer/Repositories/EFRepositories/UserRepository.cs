﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PrintMagazine.DataAccessLayer.AppContext;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;

namespace PrintMagazine.DataAccessLayer.Repositories.EFRepositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationDbContext _dbContext;
        private readonly DbSet<ApplicationUser> _dbSet;
        public UserRepository(UserManager<ApplicationUser> userManager, ApplicationDbContext dbContext, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _dbContext = dbContext;
            _signInManager = signInManager;
            _dbSet = dbContext.Set<ApplicationUser>();
        }

        public async Task<DateTimeOffset?> CheckLockoutStatusAsync(ApplicationUser user)
        {
            return await _userManager.GetLockoutEndDateAsync(user);
        }

        public async Task<GenericModel<ApplicationUser>> GetAllAsync(UserFilterModelData userFilterModel)
        {
            var query = _dbSet.Where(x=>x.IsRemove.Equals(false)).Select(x => x);

            if (!string.IsNullOrWhiteSpace(userFilterModel.SerchString))
            {
                query = query.Where(x => x.UserName.StartsWith(userFilterModel.SerchString) || x.Email.StartsWith(userFilterModel.SerchString));
            }

            if (userFilterModel.LockoutStatus.Equals(LockoutStatus.Active))
            {
                query = query.Where(x => x.LockoutEnd == null);
            }

            if (userFilterModel.LockoutStatus.Equals(LockoutStatus.Blocked))
            {
                query = _dbSet.Where(x => x.LockoutEnd != null);
            }

            var genericModel = new GenericModel<ApplicationUser>();

            genericModel.ItemsCount = query.Count(); 
            genericModel.Items = await query.Skip((userFilterModel.PageIndex) * userFilterModel.PageSize).Take(userFilterModel.PageSize).ToListAsync();

            return genericModel;
        }

        public async Task AddToRoleAsync(ApplicationUser item, string role)
        {
            await _userManager.AddToRoleAsync(item, role);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<ApplicationUser> GetUserAsync(ClaimsPrincipal principal)
        {
            return await _userManager.GetUserAsync(principal);
        }

        public async Task<IList<string>> GetRoleAsync(ApplicationUser item) 
        {
            return await _userManager.GetRolesAsync(item);
        }

        public async Task<IdentityResult> ConfirmEmailAsync(ApplicationUser item, string code)
        {
            return await _userManager.ConfirmEmailAsync(item, code);
        }

        public async Task<IdentityResult> CreateAsync(ApplicationUser item, string password)
        {
            var result = await _userManager.CreateAsync(item, password);
            await _dbContext.SaveChangesAsync();
            return result;
        }

        public async Task<ApplicationUser> FindByIdAsync(string id)
        {
            return await _userManager.FindByIdAsync(id);
        }

        public async Task<ApplicationUser> FindByNameAsync(string name)
        {
            return await _userManager.FindByNameAsync(name);
        }

        public async Task<IdentityResult> SetLockOutAsync(string email, bool lockout)
        {
            var user = await FindByEmailAsync(email);

            if (user == null)
            {
                return null;
            }

            if (!lockout) 
            {
                return await _userManager.SetLockoutEndDateAsync(user, null);
            } 

            return await _userManager.SetLockoutEndDateAsync(user, DateTime.Now.AddYears(100));
        }
        public async Task<string> GenerateEmailTokenAsync(ApplicationUser item)
        {
            return await _userManager.GenerateEmailConfirmationTokenAsync(item);
        }

        private async Task<string> GeneratePasswordResetTokenAsync(ApplicationUser item)
        {
            return await _userManager.GeneratePasswordResetTokenAsync(item);
        }

        public async Task<bool> IsEmailConfirmedAsync(ApplicationUser item)
        {
            return await _userManager.IsEmailConfirmedAsync(item);
        }

        public async Task<SignInResult> LoginAsync(string email, string password)
        {
            var user = await _userManager.FindByEmailAsync(email);

            if (user == null)
            {
                return null;
            }

            return await _signInManager.CheckPasswordSignInAsync(user, password, lockoutOnFailure:false);
        }

        public async Task<IdentityResult> ChangePasswordAsync(ApplicationUser user, string oldPassword, string newPassword)
        {
            return await _userManager.ChangePasswordAsync(user, oldPassword, newPassword);
        }

        public async Task<IdentityResult> ResetPasswordAsync(ApplicationUser user, string newPassword)
        {
            var token = await GeneratePasswordResetTokenAsync(user);
            return await _userManager.ResetPasswordAsync(user, token, newPassword);
        }

        public async Task LogOutAsync()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<IdentityResult> RemoveAsync(ApplicationUser item)
        {
            var result = await _userManager.DeleteAsync(item);
            return result;
        }

        public async Task<IdentityResult> UpdateAsync(ApplicationUser item)
        {
            var result = await _userManager.UpdateAsync(item);
            return result;
        }

        public async Task<ApplicationUser> FindByEmailAsync(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using PrintMagazine.DataAccessLayer.AppContext;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using PrintMagazine.DataAccessLayer.Model.OrderData;
using PrintMagazine.DataAccessLayer.Repositories.Base;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Repositories.EFRepositories
{
    public class OrderItemRepository : BaseEFRepository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}

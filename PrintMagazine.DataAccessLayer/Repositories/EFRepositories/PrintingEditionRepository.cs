﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PrintMagazine.DataAccessLayer.AppContext;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using PrintMagazine.DataAccessLayer.Model.PrtintingEdition;
using PrintMagazine.DataAccessLayer.Repositories.Base;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;

namespace PrintMagazine.DataAccessLayer.Repositories.EFRepositories
{
    public class PrintingEditionRepository : BaseEFRepository<PrintingEdition>, IPrintingEditionsRepository
    {

        public PrintingEditionRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }

        public async Task<GenericModel<PrintingEditionDataModel>> GetPrintingEditionsAsync(PrintingEditionFilterModelData printinEditionFilterModel)
        {
            var items = new List<PrintingEdition>();

            var genericModel = new GenericModel<PrintingEditionDataModel>();

            Expression<Func<PrintingEdition, object>> expression = x=>x.Id;

            var query = _dbContext.PrintingEditions.Where(x => x.IsRemove.Equals(false)
                                                            && x.Price >= printinEditionFilterModel.MinPrice
                                                            && printinEditionFilterModel.MaxPrice >= x.Price)
                                                    .Include(x => x.AuthorsInPrintedEdition)
                                                    .ThenInclude(x => x.Author).AsQueryable();

            foreach (var item in printinEditionFilterModel.PrintingEditionTypes)
            {
                query = query.Where(x => x.Type != item);
            }

            if (!string.IsNullOrWhiteSpace(printinEditionFilterModel.SerchString))
            {
                query = query.Where(x => x.Title.StartsWith(printinEditionFilterModel.SerchString) || x.AuthorsInPrintedEdition.Select(a => a.Author.Name.StartsWith(printinEditionFilterModel.SerchString)).FirstOrDefault());
            }

            genericModel.ItemsCount = query.Count();

            if (printinEditionFilterModel.SortColumType.Equals(SortColumType.ItemPrice))
            {
                expression = x => x.Price;
            }

            items = await OrderByAsync(printinEditionFilterModel, query, expression);

            genericModel.Items = items.Select(x => new PrintingEditionDataModel
            {
                PrintingEdition = x,
                Authors = x.AuthorsInPrintedEdition.Select(a => new PrintingEditionAuthor
                {
                    AuthorId = a.Author.Id,
                    AuthorName = a.Author.Name
                }).ToList()
            }).ToList();

            return genericModel;
        }

        public async Task<PrintingEditionDataModel> GetPrintingEditionInfoAsync(long printingEditionId)
        {
            return await _dbContext.PrintingEditions.Where(x => x.Id.Equals(printingEditionId))
                                                    .Include(x => x.AuthorsInPrintedEdition)
                                                    .ThenInclude(x => x.Author)
                                                    .Select(x => new PrintingEditionDataModel
                                                    {
                                                        PrintingEdition = x,
                                                        Authors = x.AuthorsInPrintedEdition.Select(a => new PrintingEditionAuthor
                                                        {
                                                            AuthorId = a.Author.Id,
                                                            AuthorName = a.Author.Name
                                                        }).ToList()
                                                    }).FirstOrDefaultAsync();
        }
    }
}

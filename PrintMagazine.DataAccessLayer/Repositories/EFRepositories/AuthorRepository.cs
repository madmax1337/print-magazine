﻿using Microsoft.EntityFrameworkCore;
using PrintMagazine.DataAccessLayer.AppContext;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.AuthorData;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using PrintMagazine.DataAccessLayer.Repositories.Base;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Repositories.EFRepositories
{
    public class AuthorRepository : BaseEFRepository<Author>, IAuthorRepository
    { 
        public AuthorRepository(ApplicationDbContext dbContex):base(dbContex)
        {

        }

        public async Task<GenericModel<AuthorDataModel>> GetAuthorsAsync(AuthorFilterModelData authorFilterModel)
        {
            var items = new List<Author>();

            var genericModel = new GenericModel<AuthorDataModel>();

            Expression<Func<Author, object>> expression = x => x.Id;

            var query = _dbContext.Authors.Where(x => x.IsRemove.Equals(false)).Include(x => x.AuthorsInPrintedEdition).ThenInclude(x => x.PrintingEditions).AsQueryable();

            if (!string.IsNullOrWhiteSpace(authorFilterModel.SerchString))
            {
                query = query.Where(x => x.Name.StartsWith(authorFilterModel.SerchString));
            }

            genericModel.ItemsCount = query.Count();

            if (authorFilterModel.SortColumType.Equals(SortColumType.ItemId))
            {
                items = await OrderByAsync(authorFilterModel, query, expression);
            }

            genericModel.Items = items.Select(x => new AuthorDataModel
            {
                Author = x,
                ProductTitles = x.AuthorsInPrintedEdition.Select(p => p.PrintingEditions.Title).ToList()
            }).ToList();

            return genericModel;
        }
    }
}

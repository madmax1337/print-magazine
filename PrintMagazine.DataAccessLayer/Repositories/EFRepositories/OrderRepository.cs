﻿using Microsoft.EntityFrameworkCore;
using PrintMagazine.DataAccessLayer.AppContext;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using PrintMagazine.DataAccessLayer.Model.OrderData;
using PrintMagazine.DataAccessLayer.Repositories.Base;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Repositories.EFRepositories
{
    public class OrderRepository : BaseEFRepository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }

        public async Task<GenericModel<OrderDataModel>> GetOrderAsync(OrderFilterModelData orderFilterModel)
        {

            var genericModel = new GenericModel<OrderDataModel>();

            var items = new List<Order>();

            Expression<Func<Order, object>> expression = x => x.Id;

            var query = _dbContext.Orders.Include(x => x.User).Include(x => x.OrderItems).ThenInclude(x => x.PrintingEdition).AsQueryable();

            if (!string.IsNullOrWhiteSpace(orderFilterModel.UserId))
            {
                query = query.Where(x => x.UserId.Equals(orderFilterModel.UserId));
            }

            if (orderFilterModel.OrderStatus.Equals(OrderStatus.Payd))
            {
                query = query.Where(x => x.PaymentId != null);
            }

            if (orderFilterModel.OrderStatus.Equals(OrderStatus.Unpayd))
            {
                query = query.Where(x => x.PaymentId == null);
            }

            genericModel.ItemsCount = query.Count();

            if (orderFilterModel.SortColumType.Equals(SortColumType.Date))
            {
                expression = x => x.CreationDate;
            }

            if (orderFilterModel.SortColumType.Equals(SortColumType.ItemPrice))
            {
                expression = x => x.OrderAmout;
            }

            items = await OrderByAsync(orderFilterModel, query, expression);

            genericModel.Items = items.Select(x => new OrderDataModel
            {
                Order = x,
                OrderItems = x.OrderItems.Select(o=>new OrderItemDataModel
                {
                    Title = o.PrintingEdition.Title,
                    PrintingEditionType = o.PrintingEdition.Type,
                    Count = o.Count,
                    Amount = o.Amount
                }).ToList()
            }).ToList();

            return genericModel;
        }
    }
}

﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;
using System.Threading.Tasks;
using PrintMagazine.DataAccessLayer.Entities.Base;
using System.Linq;
using System;
using System.Linq.Expressions;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.AppContext;

namespace PrintMagazine.DataAccessLayer.Repositories.Base
{
    public class BaseEFRepository<TEntity> : IBaseEFRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly ApplicationDbContext _dbContext;
        protected readonly DbSet<TEntity> _dbSet;

        public BaseEFRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = dbContext.Set<TEntity>();
        }

        public async Task<bool> CreateAsync(TEntity item)
        {
            await _dbSet.AddAsync(item);

            var result = await _dbContext.SaveChangesAsync();

            if (result.Equals(0))
            {
                return false;
            }

            return true;
        }

        public async Task<TEntity> FindByIdAsync(long id)
        {
            return await _dbSet.FindAsync(id);
        }

        public async Task<List<TEntity>> GetAllAsync()
        {
            return await _dbSet.Where(x=>x.IsRemove.Equals(false)).ToListAsync();
        }

        public async Task<bool> RemoveAsync(TEntity item)
        {
            _dbSet.Remove(item);

            var result = await _dbContext.SaveChangesAsync();

            if (result.Equals(0))
            {
                return false;
            }

            return true;
        }

        public async Task<bool> UpdateAsync(TEntity item)
        {
             _dbContext.Update(item);

            var result = await _dbContext.SaveChangesAsync();

            if (result.Equals(0))
            {
                return false;
            }

            return true;
        }
        protected async Task<List<TEntity>> OrderByAsync(BaseFilterModelData baseFilter, IQueryable<TEntity> entities, Expression<Func<TEntity, object>> expression)
        {
            if (baseFilter.SortType.Equals(SortType.Asc))
            {
                entities = entities.OrderBy(expression);
            }

            if (baseFilter.SortType.Equals(SortType.Desc))
            {
                entities = entities.OrderByDescending(expression);
            }

            return await entities.Skip((baseFilter.PageIndex) * baseFilter.PageSize).Take(baseFilter.PageSize).ToListAsync();
        }
    }
}

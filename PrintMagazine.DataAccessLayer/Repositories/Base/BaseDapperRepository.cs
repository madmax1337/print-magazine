﻿using PrintMagazine.DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Dapper.Contrib.Extensions;
using System.Linq;

namespace PrintMagazine.DataAccessLayer.Repositories.Base
{
    public class BaseDapperRepository<TEntity> : IBaseEFRepository<TEntity> where TEntity : class
    {

        protected readonly IConfiguration _configuration;

        public BaseDapperRepository(IConfiguration configuration)
        {
            _configuration = configuration; 
        }

        public async Task<bool> CreateAsync(TEntity item)
        {
            using (var connection = Connection)
            {
                var id = await connection.InsertAsync(item);

                if (id.Equals(0))
                {
                    return false;
                }

                return true;
            }
        }

        public async Task<TEntity> FindByIdAsync(long id)
        {
            using (var connection = Connection)
            {
                var item = await connection.GetAsync<TEntity>(id);

                return item;
            }
        }

        public async Task<List<TEntity>> GetAllAsync()
        {
            using (var connection = Connection)
            {
                var items = await connection.GetAllAsync<TEntity>();

                return items.ToList();
            }
        }

        public async Task<bool> RemoveAsync(TEntity item)
        {
            using (var connection = Connection)
            {
                var isSuccess = await connection.DeleteAsync(item);

                return isSuccess;
            }
        }

        public async Task<bool> UpdateAsync(TEntity item)
        {
            using (var connection = Connection )
            {
                var isSuccess = await connection.UpdateAsync(item);

                return isSuccess;
            }
        }

        public SqlConnection Connection 
        {
            get 
            {
                return new SqlConnection(_configuration.GetConnectionString("DefaultConnection"));
            }
        }
    }
}

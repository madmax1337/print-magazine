﻿using PrintMagazine.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.DataAccessLayer.Repositories.Interfaces
{
    public interface IPaymentRepository:IBaseEFRepository<Payment>
    {

    }
}

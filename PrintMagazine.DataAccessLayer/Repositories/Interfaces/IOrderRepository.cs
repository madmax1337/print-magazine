﻿using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using PrintMagazine.DataAccessLayer.Model.OrderData;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Repositories.Interfaces
{
    public interface IOrderRepository:IBaseEFRepository<Order>
    {
        Task<GenericModel<OrderDataModel>> GetOrderAsync(OrderFilterModelData orderFilterModel);
    }
}

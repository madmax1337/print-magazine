﻿using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Model.AuthorData;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Repositories.Interfaces
{
    public interface IAuthorRepository : IBaseEFRepository<Author> 
    {
        Task<GenericModel<AuthorDataModel>> GetAuthorsAsync(AuthorFilterModelData authorFilterModel);
    }
}

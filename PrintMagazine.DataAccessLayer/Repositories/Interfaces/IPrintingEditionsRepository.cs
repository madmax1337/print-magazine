﻿using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using PrintMagazine.DataAccessLayer.Model.PrtintingEdition;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Repositories.Interfaces
{
    public interface IPrintingEditionsRepository:IBaseEFRepository<PrintingEdition>
    {
        Task<PrintingEditionDataModel> GetPrintingEditionInfoAsync(long printingEditionId);
        Task<GenericModel<PrintingEditionDataModel>> GetPrintingEditionsAsync(PrintingEditionFilterModelData printinEditionFilterModel);
    }
}

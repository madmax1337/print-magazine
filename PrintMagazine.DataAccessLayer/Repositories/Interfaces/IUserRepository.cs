﻿using Microsoft.AspNetCore.Identity;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task<DateTimeOffset?> CheckLockoutStatusAsync(ApplicationUser user);
        Task<IdentityResult> CreateAsync(ApplicationUser item, string password);
        Task<IdentityResult> UpdateAsync(ApplicationUser item);
        Task<IdentityResult> RemoveAsync(ApplicationUser item);
        Task<IdentityResult> SetLockOutAsync(string email, bool lockout);
        Task<ApplicationUser> FindByIdAsync(string id);
        Task AddToRoleAsync(ApplicationUser item, string role);
        Task<IList<string>> GetRoleAsync(ApplicationUser item);
        Task<SignInResult> LoginAsync(string email, string password);
        Task<IdentityResult> ResetPasswordAsync(ApplicationUser user, string newPassword);
        Task LogOutAsync();
        Task<IdentityResult> ConfirmEmailAsync(ApplicationUser item, string code);
        Task<string> GenerateEmailTokenAsync(ApplicationUser item);
        Task<bool> IsEmailConfirmedAsync(ApplicationUser item);
        Task<ApplicationUser> FindByNameAsync(string name);
        Task<ApplicationUser> FindByEmailAsync(string email);
        Task<ApplicationUser> GetUserAsync(ClaimsPrincipal principal);
        Task<GenericModel<ApplicationUser>> GetAllAsync(UserFilterModelData userFilterModel);
        Task<IdentityResult> ChangePasswordAsync(ApplicationUser user, string oldPassword, string newPassword);
    }
}

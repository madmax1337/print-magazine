﻿using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using PrintMagazine.DataAccessLayer.Model.PrtintingEdition;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Repositories.Interfaces
{
    public interface IAuthorsInPrintedEditionsRepsitory:IBaseEFRepository<AuthorInPrintedEdition>
    {
        Task<string> DeleteItemsAsync(long printingEditionId, IEnumerable<PrintingEditionAuthor> authors);
    }
}

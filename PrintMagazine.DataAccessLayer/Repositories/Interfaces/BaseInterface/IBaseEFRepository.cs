﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Repositories.Interfaces
{
    public interface IBaseEFRepository<TEntity> where TEntity : class
    {
        Task<bool> CreateAsync(TEntity item);
        Task<bool> UpdateAsync(TEntity item);
        Task<bool> RemoveAsync(TEntity item);
        Task<TEntity> FindByIdAsync(long id);
        Task<List<TEntity>> GetAllAsync();
    }
}

﻿using Dapper;
using Microsoft.Extensions.Configuration;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using PrintMagazine.DataAccessLayer.Model.PrtintingEdition;
using PrintMagazine.DataAccessLayer.Repositories.Base;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Repositories.DapperRepositories
{
    public class PrintingEditionRepository : BaseDapperRepository<PrintingEdition>, IPrintingEditionsRepository
    {
        public PrintingEditionRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<PrintingEditionDataModel> GetPrintingEditionInfoAsync(long printingEditionId)
        {
            var printingEditionInfo = new PrintingEditionDataModel();

            var query = new StringBuilder(@"SELECT *
                        FROM AuthorInPrintedEditions ape
                        INNER JOIN Authors a ON ape.AuthorId = a.Id
                        INNER JOIN (SELECT *
			            FROM PrintingEditions pe
			            WHERE pe.Id = @printingEditionId) p ON ape.PrintingEditionsId = p.Id");

            using (var connction = Connection)
            {
                var result = await connction.QueryAsync<AuthorInPrintedEdition, Author, PrintingEdition, AuthorInPrintedEdition>(query.ToString(),
                       (authorsInPrintedEdition, author, printingEdition) =>
                       {
                           authorsInPrintedEdition.Author = author;
                           authorsInPrintedEdition.PrintingEditions = printingEdition;
                           return authorsInPrintedEdition;
                       },
                       new { printingEditionId },
                       splitOn: "Id, Id, Id"

                       );

                printingEditionInfo = result.GroupBy(x => x.PrintingEditions.Id).Select(group => new PrintingEditionDataModel
                {
                    PrintingEdition = group.Select(element => element.PrintingEditions).FirstOrDefault(),
                    Authors = group.Select(element => new PrintingEditionAuthor
                    {
                        AuthorId = element.Author.Id,
                        AuthorName = element.Author.Name
                    })
                }).FirstOrDefault();
            }

            return printingEditionInfo;
        }

        public async Task<GenericModel<PrintingEditionDataModel>> GetPrintingEditionsAsync(PrintingEditionFilterModelData printinEditionFilterModel)
        {
            var genericModel = new GenericModel<PrintingEditionDataModel>();

            var query = new StringBuilder($@" FROM AuthorInPrintedEditions ape
                                             INNER JOIN Authors a ON ape.AuthorId = a.Id
                                             INNER JOIN(SELECT*
                                             FROM PrintingEditions p
                                             WHERE p.IsRemove = 0 AND p.Price BETWEEN @MinPrice AND @MaxPrice");

            var queryData = new StringBuilder(@"SELECT *");

            var queryCount = new StringBuilder(@"; SELECT COUNT(DISTINCT p.Id)");

            var querySort = new StringBuilder();

            foreach (var item in printinEditionFilterModel.PrintingEditionTypes)
            {
                query.Append($" AND p.Type != { (int) item }");
            }

            if (!string.IsNullOrWhiteSpace(printinEditionFilterModel.SerchString))
            {
                query.Append($@" AND (p.Title LIKE @SerchString+'%'  OR (COALESCE((
                            SELECT TOP(1) CASE
                            WHEN(au.Name LIKE @SerchString+'%')
                            THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT)
                            END
                            FROM AuthorInPrintedEditions aipe
                            INNER JOIN Authors AS au ON aipe.AuthorId = au.Id
                            WHERE p.Id = aipe.PrintingEditionsId
                            ), 0) = 1))");
            }

            if (printinEditionFilterModel.SortColumType.Equals(SortColumType.ItemId))
            {
                querySort.Append($"\n ORDER BY p.Id {printinEditionFilterModel.SortType}");
            }

            if (printinEditionFilterModel.SortColumType.Equals(SortColumType.ItemPrice))
            {
                querySort.Append($"\n ORDER BY p.Price {printinEditionFilterModel.SortType}");
            }

            queryData.Append(query.ToString() + querySort.ToString() + $"\n OFFSET @PageIndex * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY) " +
                                                                       $"p ON ape.PrintingEditionsId = p.Id" + querySort.ToString());

            queryCount.Append(query.ToString() + ") p ON ape.PrintingEditionsId = p.Id");

            using (var connection = Connection)
            {
                var result = await connection.QueryMultipleAsync(queryData.ToString() + queryCount.ToString(), printinEditionFilterModel);

                var printedEditions = result.Read<AuthorInPrintedEdition, Author, PrintingEdition, AuthorInPrintedEdition>(
                    (authorInPrintedEdition, author, printingEdition) =>
                    {
                        authorInPrintedEdition.Author = author;
                        authorInPrintedEdition.PrintingEditions = printingEdition;
                        return authorInPrintedEdition;
                    },
                    splitOn: "Id, Id, Id"
                    );

                genericModel.ItemsCount = result.Read<int>().FirstOrDefault();

                genericModel.Items = printedEditions.GroupBy(x => x.PrintingEditions.Id).Select(group => new PrintingEditionDataModel
                {
                    PrintingEdition = group.Select(element => element.PrintingEditions).FirstOrDefault(),
                    Authors = group.Select(element => new PrintingEditionAuthor
                    {
                        AuthorId = element.Author.Id,
                        AuthorName = element.Author.Name
                    }).ToList()

                }).ToList();
            }

            return genericModel;
        }
    }
}

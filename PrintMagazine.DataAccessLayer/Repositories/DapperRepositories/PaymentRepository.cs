﻿using Microsoft.Extensions.Configuration;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Repositories.Base;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;

namespace PrintMagazine.DataAccessLayer.Repositories.DapperRepositories
{
    public class PaymentRepository: BaseDapperRepository<Payment>, IPaymentRepository
    {
        public PaymentRepository(IConfiguration configuration) : base(configuration)
        {

        }
    }
}

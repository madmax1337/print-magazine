﻿using Dapper;
using Microsoft.Extensions.Configuration;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Model.PrtintingEdition;
using PrintMagazine.DataAccessLayer.Repositories.Base;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Repositories.DapperRepositories
{
    public class AuthorsInPrintedEditionsRepository : BaseDapperRepository<AuthorInPrintedEdition>, IAuthorsInPrintedEditionsRepsitory
    {
        public AuthorsInPrintedEditionsRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<string> DeleteItemsAsync(long printingEditionId, IEnumerable<PrintingEditionAuthor> authors)
        {
            var authorInPrintedEditions = new List<AuthorInPrintedEdition>();

            var stringBuilder = new StringBuilder();

            var queryData = new StringBuilder(@"SELECT *
                                               FROM AuthorInPrintedEditions ape
                                               WHERE ape.PrintingEditionsId = @printingEditionId");

            using (var connection = Connection)
            {
                var resultQuery = await connection.QueryAsync<AuthorInPrintedEdition>(queryData.ToString(), new { printingEditionId });
                authorInPrintedEditions = resultQuery.ToList();
            }

            var authorIds = authors.OrderBy(x => x.AuthorId).Select(x => x.AuthorId).ToList();

            var result = authorInPrintedEditions.Select(x=>x.AuthorId).SequenceEqual(authorIds);

            if (result)
            {
                return string.Empty;
            }

            foreach (var item in authorInPrintedEditions)
            {
                var resultError = await RemoveAsync(item);

                if (!resultError)
                {
                    stringBuilder.Append(item.Id);
                }
            }
            foreach (var item in authors)
            {
                var authorInPrintingEdition = new AuthorInPrintedEdition();

                authorInPrintingEdition.PrintingEditionsId = printingEditionId;
                authorInPrintingEdition.AuthorId = item.AuthorId;
                authorInPrintingEdition.CreationDate = DateTime.Now;

                result = await CreateAsync(authorInPrintingEdition);

                if (!result)
                {
                    stringBuilder.Append(authorInPrintingEdition.Id);
                }
            }

            return stringBuilder.ToString();
        }
    }
}

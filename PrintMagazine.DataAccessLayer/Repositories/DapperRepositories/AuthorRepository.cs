﻿using Dapper;
using Microsoft.Extensions.Configuration;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.AuthorData;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using PrintMagazine.DataAccessLayer.Repositories.Base;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Repositories.DapperRepositories
{
    public class AuthorRepository: BaseDapperRepository<Author>, IAuthorRepository
    {
        public AuthorRepository(IConfiguration configuration): base(configuration)
        {

        }
        public async Task<GenericModel<AuthorDataModel>> GetAuthorsAsync(AuthorFilterModelData authorFilterModel)
        {
            var genericModel = new GenericModel<AuthorDataModel>();

            var query = new StringBuilder(@" FROM AuthorInPrintedEditions ape
                            LEFT JOIN PrintingEditions pe on ape.PrintingEditionsId = pe.Id
                            RIGHT JOIN(SELECT *
                            FROM Authors a
                            WHERE a.IsRemove = 0");

            var queryData = new StringBuilder(@"SELECT ape.Id, a.Id, a.Name, pe.Id, pe.Title");

            var queryCount = new StringBuilder(@";SELECT COUNT(DISTINCT a.Id)");

            var querySort = new StringBuilder();

            if (!string.IsNullOrWhiteSpace(authorFilterModel.SerchString))
            {
                query.Append(@" AND a.Name LIKE @SerchString+'%'");
            }
            if (authorFilterModel.SortColumType.Equals(SortColumType.ItemId))
            {
                querySort.Append("\n" + $@"ORDER BY a.Id {authorFilterModel.SortType}");
            }

            queryData.Append(query.ToString() + querySort.ToString() + "\n" + @"OFFSET @PageSize * @PageIndex ROWS FETCH NEXT @PageSize ROWS ONLY) 
                                                                                 a ON ape.AuthorId = a.Id" + querySort.ToString());

            queryCount.Append(query.ToString() + $") a ON ape.AuthorId = a.Id");

            using (var connection = Connection)
            {
                var result = await connection.QueryMultipleAsync(queryData.ToString() + queryCount.ToString(),  authorFilterModel);

                var authors = result.Read<AuthorInPrintedEdition, Author, PrintingEdition, AuthorInPrintedEdition>(
                    (authorInPrintedEdition, author, printingEdition) =>
                    {
                        authorInPrintedEdition.Author = author;
                        authorInPrintedEdition.PrintingEditions = printingEdition;
                        return authorInPrintedEdition;
                    },
                    splitOn:"Id, Id, Id"
                    );

                genericModel.ItemsCount = result.Read<int>().FirstOrDefault();

                genericModel.Items = authors.GroupBy(x => x.Author.Id).Select(group => new AuthorDataModel()
                {
                    Author = group.Select(element => element.Author).FirstOrDefault(),
                    ProductTitles = group.Where(element=>element.PrintingEditions != null).Select(element => element.PrintingEditions.Title).ToList()
                }).ToList();
            }

            return genericModel;
        }
    }
}

﻿using Dapper;
using Microsoft.Extensions.Configuration;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using PrintMagazine.DataAccessLayer.Model.OrderData;
using PrintMagazine.DataAccessLayer.Repositories.Base;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Repositories.DapperRepositories
{
    public class OrderItemRepository: BaseDapperRepository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(IConfiguration configuration) : base(configuration)
        {

        }
    }
}

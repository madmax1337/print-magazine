﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Model.GenericModel;
using PrintMagazine.DataAccessLayer.Model.OrderData;
using PrintMagazine.DataAccessLayer.Repositories.Base;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;

namespace PrintMagazine.DataAccessLayer.Repositories.DapperRepositories
{
    public class OrderRepository: BaseDapperRepository<Order>, IOrderRepository
    {
        public OrderRepository(IConfiguration configuration) : base(configuration)
        {

        }

        public async Task<GenericModel<OrderDataModel>> GetOrderAsync(OrderFilterModelData orderFilterModel)
        {
            var genericModel = new GenericModel<OrderDataModel>();

            var query = new StringBuilder(@"FROM OrderItems oi
                                             INNER JOIN PrintingEditions pe ON pe.Id = oi.PrintingEditionId
                                             INNER JOIN(SELECT o.Id, o.CreationDate, o.OrderAmout, o.PaymentId, o.UserId
                                             FROM Orders o
                                             WHERE o.IsRemove = 0");

            var queryData = new StringBuilder(@"SELECT *");

            var queryCount = new StringBuilder(@"; SELECT COUNT(DISTINCT o.Id)");

            var querySort = new StringBuilder();

            if (!string.IsNullOrWhiteSpace(orderFilterModel.UserId))
            {
                query.Append ($"\n AND o.UserId = @UserId");
            }

            if (orderFilterModel.OrderStatus.Equals(OrderStatus.Payd))
            {
                query.Append($"\n AND o.PaymentId IS NOT NULL");
            }

            if (orderFilterModel.OrderStatus.Equals(OrderStatus.Unpayd))
            {
                query.Append($"\n AND o.PaymentId IS NULL");
            }

            if (orderFilterModel.SortColumType.Equals(SortColumType.ItemId))
            {
                querySort.Append( $"\n ORDER BY o.Id {orderFilterModel.SortType}");
            }

            if (orderFilterModel.SortColumType.Equals(SortColumType.Date))
            {
                querySort.Append($"\n ORDER BY o.CreationDate {orderFilterModel.SortType}");
            }

            if (orderFilterModel.SortColumType.Equals(SortColumType.ItemPrice))
            {
                querySort.Append($"\n ORDER BY o.OrderAmout {orderFilterModel.SortType}");
            }

            queryData.Append(query.ToString()+ querySort.ToString()+ $"\n OFFSET @PageIndex * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY) " +
                                                                     $"o ON oi.OrderId = o.Id " +
                                                                     $"\n INNER JOIN AspNetUsers u ON o.UserId = u.Id" + querySort.ToString());

            queryCount.Append(query.ToString() + ") o ON oi.OrderId = o.Id" +
                                                 "\n INNER JOIN AspNetUsers u ON o.UserId = u.Id");

            using (var connection = Connection)
            {
                var result = await connection.QueryMultipleAsync(queryData.ToString() + queryCount.ToString(), orderFilterModel);

                var orders = result.Read<OrderItem, PrintingEdition, Order, ApplicationUser, OrderItem>(
                    (orderItem, printingEdition, order, user) =>
                    {
                        order.User = user;
                        orderItem.Order = order;
                        orderItem.PrintingEdition = printingEdition;
                        return orderItem;
                    },
                    splitOn: "Id, Id, Id, Id"
                    );

                genericModel.ItemsCount = result.Read<int>().FirstOrDefault();

                genericModel.Items = orders.GroupBy(x => x.Order.Id).Select(group => new OrderDataModel
                {
                    Order = group.Select(element => element.Order).FirstOrDefault(),
                    OrderItems = group.Select(element => new OrderItemDataModel
                    {
                        Title = element.PrintingEdition.Title,
                        PrintingEditionType = element.PrintingEdition.Type,
                        Count = element.Count,
                        Amount = element.Amount
                    }).ToList()
                }).ToList();
            }

            return genericModel;
        }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using PrintMagazine.DataAccessLayer.AppContext;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintMagazine.DataAccessLayer.Initializations
{
    public class DatabaseInitialization 
    {
        protected readonly ApplicationDbContext _context;
        protected readonly UserManager<ApplicationUser> _userManager;
        protected readonly RoleManager<UserRole> _roleManager;
        private const string _email = "maksim131998@gmail.com";
        private const string _firstName = "Rudyka";
        private const string _lastName = "Maksim";
        private const string _password = "7f34jih434hA";
        private const string _authorName = "Stiven King";

        public DatabaseInitialization(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<UserRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public void InitializationAsync()
        {
            if (!_roleManager.Roles.Any(x => x.Name.Equals(Role.Admin.ToString())))
            {
                var role = new UserRole();

                role.Name = Role.Admin.ToString();

                _roleManager.CreateAsync(role).GetAwaiter().GetResult();
            }

            if (!_roleManager.Roles.Any(x=>x.Name== Role.User.ToString()))
            {
                var role = new UserRole();

                role.Name = Role.User.ToString();

                _roleManager.CreateAsync(role).GetAwaiter().GetResult();
            }

            if (!_userManager.Users.Any(x => x.Email == _email))
            {
                var user = new ApplicationUser();

                user.UserName = _firstName + " " + _lastName;
                user.FirstName = _firstName;
                user.LastName = _lastName;
                user.Email = _email;
                user.EmailConfirmed = true;

                var result = _userManager.CreateAsync(user, _password).GetAwaiter().GetResult();

                _userManager.AddToRoleAsync(user, Role.Admin.ToString()).GetAwaiter().GetResult();

            }

            if (!_context.Authors.Any(x => x.Name == _authorName))
            {
                var author = new Author();

                author.Name = _authorName;

               _context.Authors.AddAsync(author);

               _context.SaveChangesAsync();
            }
        }
    }
}
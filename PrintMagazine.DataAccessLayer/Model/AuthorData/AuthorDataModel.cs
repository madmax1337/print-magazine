﻿using PrintMagazine.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.DataAccessLayer.Model.AuthorData
{
    public class AuthorDataModel
    {
        public Author Author { get; set; }
        public List<string> ProductTitles { get; set; }
        public AuthorDataModel() 
        {
            ProductTitles = new List<string>();
        }
    }
}

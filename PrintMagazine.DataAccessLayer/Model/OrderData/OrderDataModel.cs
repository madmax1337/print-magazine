﻿using PrintMagazine.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.DataAccessLayer.Model.OrderData
{
    public class OrderDataModel
    {
        public Order Order { get; set; }
        public List<OrderItemDataModel> OrderItems {get; set;}
    }
}

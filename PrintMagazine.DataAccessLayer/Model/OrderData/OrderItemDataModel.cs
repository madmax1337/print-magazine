﻿using PrintMagazine.DataAccessLayer.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.DataAccessLayer.Model.OrderData
{
    public class OrderItemDataModel
    {
        public PrintingEditionType PrintingEditionType { get; set; }
        public string Title { get; set; }
        public int Count { get; set; }
        public decimal Amount { get; set; }
    }
}

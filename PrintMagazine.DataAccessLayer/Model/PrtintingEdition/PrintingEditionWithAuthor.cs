﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.DataAccessLayer.Model.PrtintingEdition
{
    public class PrintingEditionAuthor
    {
        public long AuthorId { get; set; }
        public string AuthorName { get; set; }
    }
}

﻿using PrintMagazine.DataAccessLayer.Entities;
using System.Collections.Generic;

namespace PrintMagazine.DataAccessLayer.Model.PrtintingEdition
{
    public class PrintingEditionDataModel
    {
        public IEnumerable<PrintingEditionAuthor> Authors { get; set; }
        public PrintingEdition PrintingEdition { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.DataAccessLayer.Model.GenericModel
{
    public class GenericModel<T> where T : class
    {
        public decimal ItemsCount { get; set; }
        public List<T> Items { get; set; }
    }
}

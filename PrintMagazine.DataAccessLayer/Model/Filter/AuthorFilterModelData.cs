﻿using PrintMagazine.DataAccessLayer.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.DataAccessLayer.Model.Filter
{
    public class AuthorFilterModelData: BaseFilterModelData
    {
        public SortColumType SortColumType { get; set; }
    }
}

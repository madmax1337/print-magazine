﻿using PrintMagazine.DataAccessLayer.Entities.Enum;


namespace PrintMagazine.DataAccessLayer.Model.Filter
{
    public class BaseFilterModelData
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public SortType SortType { get; set; }
        public string SerchString { get; set; }
    }
}

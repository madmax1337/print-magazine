﻿using PrintMagazine.DataAccessLayer.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.DataAccessLayer.Model.Filter
{
    public class PrintingEditionFilterModelData : BaseFilterModelData
    {
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public List<PrintingEditionType> PrintingEditionTypes { get; set; }
        public SortColumType SortColumType { get; set; }
    }
}

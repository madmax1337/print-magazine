﻿using PrintMagazine.DataAccessLayer.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.DataAccessLayer.Model.Filter
{
    public class OrderFilterModelData : BaseFilterModelData
    { 
        public string UserId {get; set;}
        public OrderStatus OrderStatus { get; set; }
        public SortColumType SortColumType { get; set; }
    }
}

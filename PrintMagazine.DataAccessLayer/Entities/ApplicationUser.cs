﻿using Microsoft.AspNetCore.Identity;
using PrintMagazine.DataAccessLayer.Entities.Base;
using PrintMagazine.DataAccessLayer.Entities.Enum;

namespace PrintMagazine.DataAccessLayer.Entities
{
    public class ApplicationUser: IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsRemove { get; set; }
        public string Image { get; set; }
    }
}

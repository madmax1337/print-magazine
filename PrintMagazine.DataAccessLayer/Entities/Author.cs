﻿using Dapper.Contrib.Extensions;
using PrintMagazine.DataAccessLayer.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PrintMagazine.DataAccessLayer.Entities
{
    public class Author: BaseEntity
    {
        public string Name { get; set; }
        [Computed]
        public ICollection<AuthorInPrintedEdition> AuthorsInPrintedEdition { get; set; }
        public Author()
        {
            AuthorsInPrintedEdition = new List<AuthorInPrintedEdition>();
        }
    }
}

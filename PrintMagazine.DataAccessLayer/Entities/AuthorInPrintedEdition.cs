﻿using Dapper.Contrib.Extensions;
using PrintMagazine.DataAccessLayer.Entities.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace PrintMagazine.DataAccessLayer.Entities
{
    public class AuthorInPrintedEdition : BaseEntity
    {
        [ForeignKey("Author")]
        public long AuthorId { get; set; }
        [Computed]
        public Author Author { get; set; }
        [ForeignKey("PrintingEdition")]
        
        public long PrintingEditionsId{ get; set;}
        [Computed]
        public PrintingEdition PrintingEditions { get; set; }
    }
}

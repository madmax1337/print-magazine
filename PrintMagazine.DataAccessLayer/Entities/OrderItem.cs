﻿using Dapper.Contrib.Extensions;
using PrintMagazine.DataAccessLayer.Entities.Base;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using System.ComponentModel.DataAnnotations.Schema;

namespace PrintMagazine.DataAccessLayer.Entities
{
    public class OrderItem:BaseEntity
    {
        public decimal Amount { get; set; }
        public Curency Curency { get; set; }
        [ForeignKey("PrintingEdition")]
        public long PrintingEditionId { get; set; }
        [Computed]
        public PrintingEdition PrintingEdition { get; set; }
        public int Count { get; set; }
        [ForeignKey("Order")]
        public long OrderId { get; set; }
        [Computed]
        public Order Order { get; set; }
    }
}

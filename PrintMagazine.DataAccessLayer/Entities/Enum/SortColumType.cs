﻿namespace PrintMagazine.DataAccessLayer.Entities.Enum
{
    public enum SortColumType
    {
        ItemId,
        ItemPrice,
        Date
    }
}

﻿namespace PrintMagazine.DataAccessLayer.Entities.Enum
{
    public enum StatusType
    {
       InStock,
       Deliveryexpected,
       NotAvailable
    }
}

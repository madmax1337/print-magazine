﻿namespace PrintMagazine.DataAccessLayer.Entities.Enum
{
    public enum PrintingEditionType
    {
        Book,
        Magazines,
        Newspaper,
    }
}

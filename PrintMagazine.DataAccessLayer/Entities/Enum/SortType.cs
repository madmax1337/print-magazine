﻿
namespace PrintMagazine.DataAccessLayer.Entities.Enum
{
    public enum SortType
    {
        Asc,
        Desc
    }
}

﻿namespace PrintMagazine.DataAccessLayer.Entities.Enum
{
    public enum LockoutStatus
    {
        Active,
        Blocked,
        All
    }
}

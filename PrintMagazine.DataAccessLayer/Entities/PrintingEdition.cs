﻿using Dapper.Contrib.Extensions;
using PrintMagazine.DataAccessLayer.Entities.Base;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using System.Collections.Generic;

namespace PrintMagazine.DataAccessLayer.Entities
{
    public class PrintingEdition : BaseEntity
    {
        public string Title { get; set; }
        public string Descriptyon { get; set; }
        public decimal Price { get; set; }
        public StatusType Status { get; set; }
        public Curency Curency { get; set; }
        public PrintingEditionType Type { get; set; }
        [Computed]
        public ICollection<AuthorInPrintedEdition> AuthorsInPrintedEdition { get; set; }

        public PrintingEdition()
        {
            AuthorsInPrintedEdition = new List<AuthorInPrintedEdition>();
        }
    }
}

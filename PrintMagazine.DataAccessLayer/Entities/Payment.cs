﻿using PrintMagazine.DataAccessLayer.Entities.Base;

namespace PrintMagazine.DataAccessLayer.Entities
{
    public class Payment:BaseEntity
    {
        public string TransactionId { get; set; }
    }
}

﻿using Dapper.Contrib.Extensions;
using PrintMagazine.DataAccessLayer.Entities.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PrintMagazine.DataAccessLayer.Entities
{
    public class Order : BaseEntity
    {
        public string Description { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        [Computed]
        public ApplicationUser User { get; set; }
        [ForeignKey("Payment")]
        public long? PaymentId { get; set; }
        [Computed]
        public Payment Payment { get; set; }
        public decimal OrderAmout { get; set; }
        [Computed]
        public ICollection<OrderItem> OrderItems { get; set; }
        public Order()
        {
            OrderItems = new List<OrderItem>();    
        }
    }
}

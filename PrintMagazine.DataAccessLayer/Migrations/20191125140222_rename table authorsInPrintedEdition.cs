﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PrintMagazine.DataAccessLayer.Migrations
{
    public partial class renametableauthorsInPrintedEdition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AuthorsInPrintedEditions_Authors_AuthorId",
                table: "AuthorsInPrintedEditions");

            migrationBuilder.DropForeignKey(
                name: "FK_AuthorsInPrintedEditions_PrintingEditions_PrintingEditionsId",
                table: "AuthorsInPrintedEditions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AuthorsInPrintedEditions",
                table: "AuthorsInPrintedEditions");

            migrationBuilder.RenameTable(
                name: "AuthorsInPrintedEditions",
                newName: "AuthorInPrintedEditions");

            migrationBuilder.RenameIndex(
                name: "IX_AuthorsInPrintedEditions_PrintingEditionsId",
                table: "AuthorInPrintedEditions",
                newName: "IX_AuthorInPrintedEditions_PrintingEditionsId");

            migrationBuilder.RenameIndex(
                name: "IX_AuthorsInPrintedEditions_AuthorId",
                table: "AuthorInPrintedEditions",
                newName: "IX_AuthorInPrintedEditions_AuthorId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AuthorInPrintedEditions",
                table: "AuthorInPrintedEditions",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorInPrintedEditions_Authors_AuthorId",
                table: "AuthorInPrintedEditions",
                column: "AuthorId",
                principalTable: "Authors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorInPrintedEditions_PrintingEditions_PrintingEditionsId",
                table: "AuthorInPrintedEditions",
                column: "PrintingEditionsId",
                principalTable: "PrintingEditions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AuthorInPrintedEditions_Authors_AuthorId",
                table: "AuthorInPrintedEditions");

            migrationBuilder.DropForeignKey(
                name: "FK_AuthorInPrintedEditions_PrintingEditions_PrintingEditionsId",
                table: "AuthorInPrintedEditions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AuthorInPrintedEditions",
                table: "AuthorInPrintedEditions");

            migrationBuilder.RenameTable(
                name: "AuthorInPrintedEditions",
                newName: "AuthorsInPrintedEditions");

            migrationBuilder.RenameIndex(
                name: "IX_AuthorInPrintedEditions_PrintingEditionsId",
                table: "AuthorsInPrintedEditions",
                newName: "IX_AuthorsInPrintedEditions_PrintingEditionsId");

            migrationBuilder.RenameIndex(
                name: "IX_AuthorInPrintedEditions_AuthorId",
                table: "AuthorsInPrintedEditions",
                newName: "IX_AuthorsInPrintedEditions_AuthorId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AuthorsInPrintedEditions",
                table: "AuthorsInPrintedEditions",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorsInPrintedEditions_Authors_AuthorId",
                table: "AuthorsInPrintedEditions",
                column: "AuthorId",
                principalTable: "Authors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AuthorsInPrintedEditions_PrintingEditions_PrintingEditionsId",
                table: "AuthorsInPrintedEditions",
                column: "PrintingEditionsId",
                principalTable: "PrintingEditions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

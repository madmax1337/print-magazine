﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PrintMagazine.DataAccessLayer.Migrations
{
    public partial class addcolumtoordertable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "OrderAmout",
                table: "Orders",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrderAmout",
                table: "Orders");
        }
    }
}

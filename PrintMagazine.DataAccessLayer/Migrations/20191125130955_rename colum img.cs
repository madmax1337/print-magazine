﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PrintMagazine.DataAccessLayer.Migrations
{
    public partial class renamecolumimg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Img",
                table: "AspNetUsers",
                newName: "Image");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Image",
                table: "AspNetUsers",
                newName: "Img");
        }
    }
}

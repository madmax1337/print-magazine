﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PrintMagazine.DataAccessLayer.AppContext;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Initializations;
using PrintMagazine.DataAccessLayer.Repositories.DapperRepositories;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;

namespace PrintMagazine.DataAccessLayer.Extension
{
    public static class ServiceInitializeExtension
    {
        public static void AddServices(this IServiceCollection services, IConfiguration Configuration)
        {
            #region DbOption
            services.AddDbContext<ApplicationDbContext>(options =>
             options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, UserRole>(options => {
                options.Password.RequireNonAlphanumeric = false;
                options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";
            }).AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();
            #endregion

            #region EFRepository
            services.AddTransient<IUserRepository, Repositories.EFRepositories.UserRepository>();
            //services.AddTransient<IPrintingEditionsRepository, Repositories.EFRepositories.PrintingEditionRepository>();
            //services.AddTransient<IAuthorRepository, Repositories.EFRepositories.AuthorRepository>();
            //services.AddTransient<IAuthorsInPrintedEditionsRepsitory, Repositories.EFRepositories.AuthorsInPrintedEditionsRepository>();
            //services.AddTransient<IPaymentRepository, Repositories.EFRepositories.PaymentRepository>();
            //services.AddTransient<IOrderRepository, Repositories.EFRepositories.OrderRepository>();
            //services.AddTransient<IOrderItemRepository, Repositories.EFRepositories.OrderItemRepository>();
            #endregion

            #region DapperRepository
            services.AddTransient<IPrintingEditionsRepository, PrintingEditionRepository>();
            services.AddTransient<IAuthorRepository, AuthorRepository>();
            services.AddTransient<IAuthorsInPrintedEditionsRepsitory, AuthorsInPrintedEditionsRepository>();
            services.AddTransient<IPaymentRepository, PaymentRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IOrderItemRepository, OrderItemRepository>();
            #endregion

            #region Other
            services.AddTransient<DatabaseInitialization>();
            #endregion
        }
    }
}

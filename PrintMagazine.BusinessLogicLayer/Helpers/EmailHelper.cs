﻿using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;
using System;
using PrintMagazine.BusinessLogicLayer.Constant;
using Microsoft.Extensions.Configuration;
using PrintMagazine.BusinessLogicLayer.Models.Option;
using Microsoft.Extensions.Options;

namespace PrintMagazine.BusinessLogicLayer.Helpers
{
    public class EmailHelper:IEmailHelper
    {
        private readonly EmailOption _emailOption;

        public EmailHelper (IOptions<EmailOption> emailOption)
        {
            _emailOption = emailOption.Value;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(Constants.Email.Name, _emailOption.Email));
            emailMessage.To.Add(new MailboxAddress(email, email));
            emailMessage.Subject = subject;

            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_emailOption.Email, _emailOption.Port, MailKit.Security.SecureSocketOptions.StartTls);
                await client.AuthenticateAsync(_emailOption.Email, _emailOption.Password);
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }

        }
    }
}

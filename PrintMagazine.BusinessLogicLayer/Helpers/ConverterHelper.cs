﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PrintMagazine.BusinessLogicLayer.Constant;
using PrintMagazine.BusinessLogicLayer.Helpers.Interfaces;
using PrintMagazine.DataAccessLayer.Entities.Enum;

namespace PrintMagazine.BusinessLogicLayer.Helpers
{
    public static class ConverterHelper
    {
        public static decimal CurencyConverterAnyToUsd(this decimal price, Curency curency)
        {
            foreach (var keyValue in Constants.CurencyPrice.curencyDictionaryUsd)
            {
                if (keyValue.Key == curency)
                {
                    price = price / keyValue.Value;
                }
            }
            return price;
        }
        public static decimal CurencyConveterUsdToAny(this decimal price, Curency curency)
        {
            foreach (var keyValue in Constants.CurencyPrice.curencyDictionaryUsd)
            {
                if (keyValue.Key == curency)
                {
                    price = price * keyValue.Value;
                }
            }
            return price;
        }
    }
}

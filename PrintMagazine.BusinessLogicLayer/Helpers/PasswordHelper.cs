﻿using PrintMagazine.BusinessLogicLayer.Constant;
using PrintMagazine.BusinessLogicLayer.Helpers.Interfaces;
using System;
using System.Text;

namespace PrintMagazine.BusinessLogicLayer.Helpers
{
    public class GenerateNewPassword : IPasswordHelper
    {
        public string PasswordHelper()
        {
            var charsetforcode = Constants.PasswordGeneration.CharsetForCode;
            var stringBuilder = new StringBuilder();
            var random = new Random();

            for (int i = 0; i < 14; i++)
            {
                stringBuilder.Append(charsetforcode[random.Next(charsetforcode.Length)]);
            }
            return stringBuilder.ToString();
        }

    }
}

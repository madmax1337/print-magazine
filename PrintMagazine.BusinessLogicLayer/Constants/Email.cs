﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.BusinessLogicLayer.Constant
{
    public partial class Constants
    {
        public partial class Email
        {
            public const string EmailConfirm = "Confirm your account";
            public const string ForgotPassword = "Forgot Password";
            public const string Name = "Administration";
        }
    }
}

﻿using PrintMagazine.DataAccessLayer.Entities.Enum;
using System.Collections.Generic;

namespace PrintMagazine.BusinessLogicLayer.Constant
{
    public partial class Constants
    {
        public partial class CurencyPrice
        {
            public const decimal GBR = 0.81060M;
            public const decimal UAH = 24.77398M;
            public const decimal EUR = 0.91138M;

            public static readonly Dictionary<Curency, decimal> curencyDictionaryUsd = new Dictionary<Curency, decimal> {
                { Curency.GBR, GBR },
                { Curency.UAH, UAH },
                { Curency.EUR, EUR }
            };
        }
    }
}

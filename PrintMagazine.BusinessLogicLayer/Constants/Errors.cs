﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.BusinessLogicLayer.Constant
{
    public partial class Constants
    {
        public partial class Errors
        {
            public const string UserError = "Users is empty";
            public const string UserNotFound = "User not found";
            public const string AddError = "Creating item error";
            public const string SerchError = "Item no found";
            public const string DeleteError = "Remove item error";
            public const string AfterUpdateError = "These items could not be deleted: ";
            public const string AfterAddError = "These items could not be creating: ";
            public const string UpdateError = "Update item error";
            public const string PrintingEditionError = "PrintingEditions not found";
            public const string OrderError = "Orders not found";
            public const string PaymentError = "Transaction error";
            public const string AuthorError = "Authors not found";
            public const string AuthorNootFound = "Author not found";
            public const string RefreshTokenError = "Refresh token outdated";
            public const string ConfirmEmailError = "Error confirm account";
            public const string ForgotError = "Forgot password Error";
            public const string SignUpError = "SignUp error";
            public const string UserCheckError = "This user exists";
            public const string SignInError = "Incorrect user name and / or password";
            public const string ErrorBuying = "Buying Printing Edition error";
            public const string VerifiEmailError = "You have not verified email";
            public const string EmailSendError = "Sending letter in your email error";
            public const string UserBlock = "This user has been blocked. Contact the administration.";
            public const string TokenError = "Token expire";
        }
    }
}

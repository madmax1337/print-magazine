﻿using System.Linq;
using System.Threading.Tasks;
using PrintMagazine.BusinessLogicLayer.Constant;
using PrintMagazine.BusinessLogicLayer.Mapper;
using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.BusinessLogicLayer.Models.Filter;
using PrintMagazine.BusinessLogicLayer.Models.PrintingEditions;
using PrintMagazine.BusinessLogicLayer.Services.Interfeces;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;

namespace PrintMagazine.BusinessLogicLayer.Services
{
    public class PrintingEditionService : IPrintingEditionService
    {
        private readonly IPrintingEditionsRepository _printingEditionsRepository;
        private readonly IAuthorsInPrintedEditionsRepsitory _authorsInPrintedEditionsRepsitory;

        public PrintingEditionService(IPrintingEditionsRepository printingEditionsRepository, IAuthorsInPrintedEditionsRepsitory authorsInPrintedEditionsRepsitory)
        {
            _printingEditionsRepository = printingEditionsRepository;
            _authorsInPrintedEditionsRepsitory = authorsInPrintedEditionsRepsitory;
        }

        public async Task<BaseModel> AddItemsAsync(PrintingEditionModelItem printingEditionModelItem)
        {
            if (printingEditionModelItem.Equals(null) || string.IsNullOrWhiteSpace(printingEditionModelItem.Title) || string.IsNullOrWhiteSpace(printingEditionModelItem.Description))
            {
                printingEditionModelItem.Errors.Add(Constants.Errors.AddError);
                return printingEditionModelItem;
            }

            var printingEdition = printingEditionModelItem.MapToPrintingEdition();

            var result = await _printingEditionsRepository.CreateAsync(printingEdition);

            if (!result)
            {
                printingEditionModelItem.Errors.Add(Constants.Errors.AddError);
                return printingEditionModelItem;
            }

            foreach (var author in printingEditionModelItem.Authors)
            {
                var authorInPrintingEdition = PrintingEditionMapper.MapToAuthorInPrintingEdition(printingEdition.Id, author.AuthorId);

                result = await _authorsInPrintedEditionsRepsitory.CreateAsync(authorInPrintingEdition);

                if(!result)
                {
                    printingEditionModelItem.Errors.Add(Constants.Errors.AfterAddError + authorInPrintingEdition.Id.ToString() + Constants.Errors.AddError);
                }
            }

            return printingEditionModelItem;
        }

        public async Task<BaseModel> UpdateAsync(PrintingEditionModelItem printingEditionModelItem) 
        {
            var printingEdition = await _printingEditionsRepository.FindByIdAsync(printingEditionModelItem.PrintingEditionId); 

            if (printingEdition.Equals(null))
            {
                printingEditionModelItem.Errors.Add(Constants.Errors.PrintingEditionError); 
                return printingEditionModelItem;
            }

            var result = await _printingEditionsRepository.UpdateAsync(PrintingEditionMapper.MapForUpdateEntity(printingEdition, printingEditionModelItem));

            if (!result)
            {
                printingEditionModelItem.Errors.Add(printingEditionModelItem.IsRemove.Equals(true) ? Constants.Errors.DeleteError : Constants.Errors.UpdateError); 
                return printingEditionModelItem;
            }

            var resultId = await _authorsInPrintedEditionsRepsitory.DeleteItemsAsync(printingEditionModelItem.PrintingEditionId, printingEditionModelItem.Authors); 

            if (!string.IsNullOrWhiteSpace(resultId))
            {
                printingEditionModelItem.Errors.Add(Constants.Errors.AfterUpdateError + resultId);
                return printingEditionModelItem;
            }

            return printingEditionModelItem;
        }

        public async Task<PrintingEditionModel> GetPrintingEditionModel(PrintingEditionFilterModel printingEditionFilterModel)
        {
            var printingEditionModel = new PrintingEditionModel();

            var printingEdition = await _printingEditionsRepository.GetPrintingEditionsAsync(printingEditionFilterModel.MapPrintingEditionFilter());

            if (!printingEdition.Items.Any())
            {
                printingEditionModel.Errors.Add(Constants.Errors.PrintingEditionError);
                return printingEditionModel;
            }

            printingEditionModel.ItemCount = printingEdition.ItemsCount;

            foreach (var items in printingEdition.Items)
            {
                printingEditionModel.Items.Add(items.MapToPrintingEditionModel(printingEditionFilterModel.Curency));
            }

            return printingEditionModel;
        }
        public async Task<PrintingEditionModelItem> GetInfoPrintingEdition(long printingEditionId, Curency curency)
        {
            var printingEditionModelItem = new PrintingEditionModelItem();

            var printingEditionDataModel = await _printingEditionsRepository.GetPrintingEditionInfoAsync(printingEditionId);

            if (printingEditionDataModel == null)
            {
                printingEditionModelItem.Errors.Add(Constants.Errors.PrintingEditionError);
                return printingEditionModelItem;
            }

            printingEditionModelItem = printingEditionDataModel.MapToPrintingEditionModel(curency);

            return printingEditionModelItem;
        }
    }
}

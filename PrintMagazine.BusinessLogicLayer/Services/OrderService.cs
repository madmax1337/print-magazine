﻿using System;
using System.Linq;
using System.Threading.Tasks;
using PrintMagazine.BusinessLogicLayer.Constant;
using PrintMagazine.BusinessLogicLayer.Mapper;
using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.BusinessLogicLayer.Models.Cart;
using PrintMagazine.BusinessLogicLayer.Models.Filter;
using PrintMagazine.BusinessLogicLayer.Models.Order;
using PrintMagazine.BusinessLogicLayer.Models.Payment;
using PrintMagazine.BusinessLogicLayer.Services.Interfeces;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;

namespace PrintMagazine.BusinessLogicLayer.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IOrderItemRepository _orderItemRepository;

        public OrderService(IPaymentRepository paymentRepository, IOrderRepository orderRepository, IUserRepository userRepository, IOrderItemRepository orderItemRepository)
        {
            _paymentRepository = paymentRepository;
            _orderRepository = orderRepository;
            _orderItemRepository = orderItemRepository;
        }

        public async Task<OrderModel> GetAllAsync(OrderFilterModel orderFilterModel)
        {
            var orderModel = new OrderModel();

            if (orderFilterModel.Equals(null))
            {
                orderModel.Errors.Add(Constants.Errors.OrderError);
                return orderModel;
            }

            var orders = await _orderRepository.GetOrderAsync(orderFilterModel.MapOrderFilter());

            if (!orders.Items.Any())
            {
                orderModel.Errors.Add(Constants.Errors.OrderError);
                return orderModel;
            }

            orderModel.ItemCount = orders.ItemsCount;

            foreach (var order in orders.Items)
            {
                orderModel.Items.Add(order.MapToOrderModelItem());
            }

            return orderModel;
        }

        private async Task<Payment> CreatePaymentAsync(string transactionId)
        {
            var payment = new Payment();

            if (string.IsNullOrWhiteSpace(transactionId))
            {
                return payment;
            }

            payment.TransactionId = transactionId;
            payment.CreationDate = DateTime.Now;

            await _paymentRepository.CreateAsync(payment);

            return payment;
        }

        public async Task<OrderModelItem> CreateOrderAsync(CartModel cartModel, string userId)
        {
            var orderModelItem = new OrderModelItem();

            var payment = await CreatePaymentAsync(cartModel.TransactionId);

            var order = OrderMapper.MapToOrder(userId, payment.Id, cartModel.OrderAmount);

            var result = await _orderRepository.CreateAsync(order);

            if (!result)
            {
                orderModelItem.Errors.Add(Constants.Errors.ErrorBuying);
                return orderModelItem;
            }

            orderModelItem.OrderId = order.Id;

            foreach (var cart in cartModel.Items)
            {
                var orderItem = cart.MapToOrderItem(order.Id);

                result = await _orderItemRepository.CreateAsync(orderItem);

                if (!result)
                {
                    orderModelItem.Errors.Add(Constants.Errors.AfterAddError + orderItem.Id.ToString() + Constants.Errors.ErrorBuying);
                }
            }

            return orderModelItem;
        }

        public async Task<BaseModel> UpdateAsync(long orderId, string transactioId) 
        {
            var baseModel = new BaseModel();

            var payment = await CreatePaymentAsync(transactioId); 

            if (string.IsNullOrWhiteSpace(payment.TransactionId))
            {
                baseModel.Errors.Add(Constants.Errors.ErrorBuying);
                return baseModel;
            }

            var order = await _orderRepository.FindByIdAsync(orderId);

            if (order.Equals(null))
            {
                baseModel.Errors.Add(Constants.Errors.OrderError);
                return baseModel;
            }

            order.PaymentId = payment.Id;

            var result = await _orderRepository.UpdateAsync(order);

            if (!result)
            {
                baseModel.Errors.Add(Constants.Errors.ErrorBuying);
            }

            return baseModel;
        }
    }
}

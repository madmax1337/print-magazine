﻿using System;
using System.Linq;
using System.Threading.Tasks;
using PrintMagazine.BusinessLogicLayer.Constant;
using PrintMagazine.BusinessLogicLayer.Helpers;
using PrintMagazine.BusinessLogicLayer.Mapper;
using PrintMagazine.BusinessLogicLayer.Models.Account.SignUpModel;
using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.BusinessLogicLayer.Models.SingUpModel;
using PrintMagazine.BusinessLogicLayer.Models.User;
using PrintMagazine.BusinessLogicLayer.Services.Interfeces;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;

namespace PrintMagazine.BusinessLogicLayer.Services
{
    public class AccountServices : IAccountService
    {
        private readonly IUserRepository _userRepository;
        private readonly IEmailHelper _emailHelper;

        public AccountServices(IUserRepository userRepository, IEmailHelper emailHelper)
        {
            _userRepository = userRepository;
            _emailHelper = emailHelper;
        }

        public async Task<BaseModel> ConfirmEmailAsync(string userId, string code)
        {
            var baseModel = new BaseModel();

            if (string.IsNullOrWhiteSpace(userId)  || string.IsNullOrWhiteSpace(code))
            {
                baseModel.Errors.Add(Constants.Errors.ConfirmEmailError);
                return baseModel;
            }

            var user = await _userRepository.FindByIdAsync(userId);

            if (user.Equals(null))
            {
                baseModel.Errors.Add(Constants.Errors.UserNotFound);
                return baseModel;
            }

            var result = await _userRepository.ConfirmEmailAsync(user, code);

            if (!result.Succeeded)
            {
                baseModel.Errors.AddRange(result.Errors.Select(x => x.Description));
            }

            return baseModel;
        }

        public async Task<BaseModel> EmailSenderAsync(string callbackUrl, string email, string code)
        {
            var baseModel = new BaseModel();

            var msg = string.Empty;

            if (string.IsNullOrWhiteSpace(email))
            {
                baseModel.Errors.Add(Constants.Errors.EmailSendError);
                return baseModel;
            }

            msg = string.IsNullOrWhiteSpace(code) ? $"Confirm the registration by clicking on the link:<a href={callbackUrl}>link</a>" : $"Your new password: {code}";

            await _emailHelper.SendEmailAsync(email, Constants.Email.EmailConfirm, msg);

            return baseModel;
        }

        public async Task<BaseModel> ForgotPasswordAsync(string email, string code)
        {
            var baseModel = new BaseModel();

            var user = await _userRepository.FindByEmailAsync(email);

            if (user.Equals(null))
            {
                baseModel.Errors.Add(Constants.Errors.UserNotFound);
                return baseModel;
            }

            var result = await _userRepository.ResetPasswordAsync(user, code);

            if (!result.Succeeded)
            {
                baseModel.Errors.AddRange(result.Errors.Select(x => x.Description));
            }

            return baseModel;
        }


        public async Task LogOutAsync()
        {
            await _userRepository.LogOutAsync();
        }

        public async Task<UserModelItem> SignInAsync(string email, string password)
        {
            var userModelItem = new UserModelItem();

            var user = await _userRepository.FindByEmailAsync(email); 

            if (user.Equals(null))
            {
                userModelItem.Errors.Add(Constants.Errors.UserNotFound);
                return userModelItem;
            }

            var baseModel = await SignInConfirmEmailAsync(email);

            if (baseModel.Errors.Any())
            {
                userModelItem.Errors.AddRange(baseModel.Errors);
                return userModelItem;
            }

            var dateLockOut = await _userRepository.CheckLockoutStatusAsync(user);

            if (!dateLockOut.Equals(null) && dateLockOut <= DateTime.Now) 
            {
                userModelItem.Errors.Add(Constants.Errors.UserBlock);
                return userModelItem;
            }

            var result = await _userRepository.LoginAsync(email, password); 

            if (!result.Succeeded || result.Equals(null))
            {
                userModelItem.Errors.Add(Constants.Errors.SignInError);
                return userModelItem;
            }

            userModelItem = user.MapToUserModel();

            userModelItem.Role = (await _userRepository.GetRoleAsync(user)).FirstOrDefault();

            return userModelItem;
        }

        private async Task<BaseModel> SignInConfirmEmailAsync(string email)
        {
            var baseModel = new BaseModel();

            var user = await _userRepository.FindByEmailAsync(email);

            if (user.Equals(null))
            {
                baseModel.Errors.Add(Constants.Errors.SignInError);
                return baseModel;
            }

            var result = !await _userRepository.IsEmailConfirmedAsync(user);

            if (result)
            {
                baseModel.Errors.Add(Constants.Errors.VerifiEmailError);
            }

            return baseModel;
        }

        public async Task<ConfirmEmailModel> SignUpAsync(SignUpModel signUpModel)
        {
            var confirmEmailModel = new ConfirmEmailModel();

            if (signUpModel.Equals(null))
            {
                confirmEmailModel.Errors.Add(Constants.Errors.UserCheckError);
                return confirmEmailModel;
            }

            var user = await _userRepository.FindByEmailAsync(signUpModel.Email);

            if (user != null)
            {
                confirmEmailModel.Errors.Add(Constants.Errors.UserCheckError);
                return confirmEmailModel;
            }

            var applicationUser = signUpModel.MapToAplicationUser();

            var result = await _userRepository.CreateAsync(applicationUser, signUpModel.Password);

            if (!result.Succeeded)
            {
                confirmEmailModel.Errors.AddRange(result.Errors.Select(x=>x.Description));
                return confirmEmailModel;
            }

            await _userRepository.AddToRoleAsync(applicationUser, Role.User.ToString());

            var code = await _userRepository.GenerateEmailTokenAsync(applicationUser);

            confirmEmailModel.Id = applicationUser.Id;
            confirmEmailModel.Code = code;

            return confirmEmailModel;
        }
    }
}

﻿
using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.BusinessLogicLayer.Models.Filter;
using PrintMagazine.BusinessLogicLayer.Models.User;
using PrintMagazine.DataAccessLayer.Model.Filter;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PrintMagazine.BusinessLogicLayer.Services.Interfeces
{
    public interface IUserService
    {
        Task<BaseModel> UpdateAsync(ClaimsPrincipal claims, UserModelItem userModelItem);
        Task<BaseModel> SetStatusAsync(UserModelItem userModelItem);
        Task<UserModel> GetAllAsync(UserFilterModel userFilterModel);
    }
}

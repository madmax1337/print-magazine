﻿using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.BusinessLogicLayer.Models.Filter;
using PrintMagazine.BusinessLogicLayer.Models.PrintingEditions;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.Filter;
using System.Threading.Tasks;

namespace PrintMagazine.BusinessLogicLayer.Services.Interfeces
{
    public interface IPrintingEditionService
    {
        Task<PrintingEditionModel> GetPrintingEditionModel(PrintingEditionFilterModel printingEditionFilterModel);
        Task<BaseModel> AddItemsAsync(PrintingEditionModelItem printingEditionModelItem);
        Task<BaseModel> UpdateAsync(PrintingEditionModelItem printingEditionModelItem);
        Task<PrintingEditionModelItem> GetInfoPrintingEdition(long printingEditionId, Curency curency);
    }
}

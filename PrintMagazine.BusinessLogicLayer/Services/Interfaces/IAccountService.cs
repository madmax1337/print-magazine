﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using PrintMagazine.BusinessLogicLayer.Models.SingUpModel;
using PrintMagazine.BusinessLogicLayer.Models.Account.SignUpModel;
using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.BusinessLogicLayer.Models.User;

namespace PrintMagazine.BusinessLogicLayer.Services.Interfeces
{
    public interface IAccountService
    {
        Task<UserModelItem> SignInAsync(string email, string password);
        Task<BaseModel> EmailSenderAsync(string callbackUrl, string email, string code);
        Task<ConfirmEmailModel> SignUpAsync(SignUpModel signUpModel);
        Task<BaseModel> ForgotPasswordAsync(string email, string cod);
        Task<BaseModel> ConfirmEmailAsync(string userId, string code);
        Task LogOutAsync();
    }
}

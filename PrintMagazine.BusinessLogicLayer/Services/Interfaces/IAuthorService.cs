﻿using PrintMagazine.BusinessLogicLayer.Models.Authors;
using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.BusinessLogicLayer.Models.Filter;
using System.Threading.Tasks;

namespace PrintMagazine.BusinessLogicLayer.Services.Interfeces
{
    public interface IAuthorService
    {
        Task<BaseModel> CreateAsync(AuthorModelItem authorModelItem);
        Task<BaseModel> UpdateAsync(AuthorModelItem authorModelItem);
        Task<AuthorModel> GetAllAsync(AuthorFilterModel authorFilterModel);
        Task<AuthorModel> GetAuthorForPrintingEdition();
    }
}

﻿using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.BusinessLogicLayer.Models.Cart;
using PrintMagazine.BusinessLogicLayer.Models.Filter;
using PrintMagazine.BusinessLogicLayer.Models.Order;
using PrintMagazine.BusinessLogicLayer.Models.Payment;
using PrintMagazine.DataAccessLayer.Model.Filter;
using System.Threading.Tasks;

namespace PrintMagazine.BusinessLogicLayer.Services.Interfeces
{
    public interface IOrderService
    {
        Task<BaseModel> UpdateAsync(long orderId, string transactionId);
        Task<OrderModelItem> CreateOrderAsync(CartModel cartModel, string userId);
        Task<OrderModel> GetAllAsync(OrderFilterModel orderFilterModel);
    }
}

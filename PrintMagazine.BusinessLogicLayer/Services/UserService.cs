﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using PrintMagazine.BusinessLogicLayer.Constant;
using PrintMagazine.BusinessLogicLayer.Mapper;
using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.BusinessLogicLayer.Models.Filter;
using PrintMagazine.BusinessLogicLayer.Models.User;
using PrintMagazine.BusinessLogicLayer.Services.Interfeces;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;

namespace PrintMagazine.BusinessLogicLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<UserModel> GetAllAsync(UserFilterModel userFilterModel)
        {
            var userModel = new UserModel();

            if (userFilterModel.Equals(null))
            {
                userModel.Errors.Add(Constants.Errors.UserError);
                return userModel;
            }

            var users = await _userRepository.GetAllAsync(userFilterModel.MapUserFilter());

            if (!users.Items.Any())
            {
                userModel.Errors.Add(Constants.Errors.UserError);
                return userModel;
            }

            userModel.ItemCount = users.ItemsCount;

            foreach (var user in users.Items)
            {
                userModel.Items.Add(UserMapper.MapToUserModel(user));
            }

            return userModel;
        }

        public async Task<BaseModel> SetStatusAsync(UserModelItem userModelItem)
        {
            var result = await _userRepository.SetLockOutAsync(userModelItem.Email, userModelItem.IsLockout);

            if (result.Equals(null))
            {
                userModelItem.Errors.Add(Constants.Errors.UserNotFound);
            }

            userModelItem.Errors.AddRange(result.Errors.Select(x => x.Description));

            return userModelItem;
        }

        public async Task<BaseModel> UpdateAsync(ClaimsPrincipal claims, UserModelItem userModelItem)
        {
            var user = string.IsNullOrWhiteSpace(userModelItem.UserId) ? await _userRepository.GetUserAsync(claims) : await _userRepository.FindByIdAsync(userModelItem.UserId);

            if (user.Equals(null))
            {
                userModelItem.Errors.Add(Constants.Errors.UserNotFound);
                return userModelItem;
            }

            var result = await _userRepository.UpdateAsync(UserMapper.MapForUpdateEntity(user,userModelItem));

            if (!result.Succeeded)
            {
                userModelItem.Errors.Add(userModelItem.IsRemove.Equals(false) ? Constants.Errors.UpdateError : Constants.Errors.DeleteError);
                return userModelItem;
            }

            if (string.IsNullOrWhiteSpace(userModelItem.NewPassword) && string.IsNullOrWhiteSpace(userModelItem.NewPassword))
            {
                return userModelItem;
            }

            result = await _userRepository.ChangePasswordAsync(user, userModelItem.Password, userModelItem.NewPassword);

            if (!result.Succeeded)
            {
                userModelItem.Errors.Add(Constants.Errors.UpdateError);
                return userModelItem;
            }

            return userModelItem;
        }
    }
}

﻿using System.Linq;
using System.Threading.Tasks;
using PrintMagazine.BusinessLogicLayer.Constant;
using PrintMagazine.BusinessLogicLayer.Mapper;
using PrintMagazine.BusinessLogicLayer.Models.Authors;
using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.BusinessLogicLayer.Models.Filter;
using PrintMagazine.BusinessLogicLayer.Services.Interfeces;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Model.Filter;
using PrintMagazine.DataAccessLayer.Repositories.Interfaces;

namespace PrintMagazine.BusinessLogicLayer.Services
{
    public class AuthortServices : IAuthorService
    {
        private readonly IAuthorRepository _authorRepsitory;

        public AuthortServices(IAuthorRepository authorRepsitory)
        { 
            _authorRepsitory = authorRepsitory;
        }

        public async Task<AuthorModel> GetAllAsync(AuthorFilterModel authorFilterModel)
        {
            var authorModel = new AuthorModel();

            if (authorFilterModel.Equals(null))
            {
                authorModel.Errors.Add(Constants.Errors.AuthorError);
                return authorModel;
            }

            var authors = await _authorRepsitory.GetAuthorsAsync(authorFilterModel.MapAuthorFilter());

            if (!authors.Items.Any())
            {
                authorModel.Errors.Add(Constants.Errors.AuthorError);
                return authorModel;
            }

            authorModel.ItemCount = authors.ItemsCount;

            foreach (var author in authors.Items)
            {
                authorModel.Items.Add(author.MapToAuthorModelAsync());
            }

            return authorModel;
        }

        public async Task<AuthorModel> GetAuthorForPrintingEdition()
        {
            var authorModel = new AuthorModel();

            var authors = await _authorRepsitory.GetAllAsync();

            if (!authors.Any())
            {
                authorModel.Errors.Add(Constants.Errors.AuthorError);
                return authorModel;
            }

            authorModel.ItemCount = authors.Count();

            foreach (var author in authors)
            {
                authorModel.Items.Add(author.MapToAuthorModel());
            }

            return authorModel;
        }

        public async Task<BaseModel> CreateAsync(AuthorModelItem authorModelItem) 
        {
            if (authorModelItem.Equals(null))
            {
                var baseModel = new BaseModel();
                baseModel.Errors.Add(Constants.Errors.AddError);
                return baseModel;
            }

            var author = authorModelItem.MapToAuthorAsync();

            var result = await _authorRepsitory.CreateAsync(author);

            if (!result)
            {
                authorModelItem.Errors.Add(Constants.Errors.AddError);
            }

            return authorModelItem;
        }

        public async Task<BaseModel> UpdateAsync(AuthorModelItem authorModelItem)
        {
            if (authorModelItem.Equals(null))
            {
                var baseModel = new BaseModel();
                baseModel.Errors.Add(Constants.Errors.AuthorError);
                return baseModel;
            }

            var author = await _authorRepsitory.FindByIdAsync(authorModelItem.AuthorId);

            if (author.Equals(null))
            {
                authorModelItem.Errors.Add(Constants.Errors.AuthorError);
                return authorModelItem;
            }

            var result = await _authorRepsitory.UpdateAsync(AuthorMapper.MapForUpdateEntity(author,authorModelItem));

            if (!result)
            {
                authorModelItem.Errors.Add(authorModelItem.IsRemove.Equals(true) ? Constants.Errors.DeleteError : Constants.Errors.UpdateError);
            }

            return authorModelItem;
        }
    }
}

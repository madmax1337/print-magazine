﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace PrintMagazine.BusinessLogicLayer.Common
{
    public class FileLogger : ILogger
    {
        private readonly string filePath;
        private readonly object _lock = new object();

        public FileLogger(string path)
        {
            filePath = path;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (formatter == null || logLevel != LogLevel.Error) 
            {
                return;
            }
            lock (_lock)
            {
                File.AppendAllText(filePath, DateTime.Now.ToString() + ":" + formatter(state, exception) + Environment.NewLine);
            }
        }
    }
}

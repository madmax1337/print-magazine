﻿using PrintMagazine.BusinessLogicLayer.Helpers;
using PrintMagazine.BusinessLogicLayer.Services;
using PrintMagazine.BusinessLogicLayer.Services.Interfeces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using PrintMagazine.BusinessLogicLayer.Helpers.Interfaces;
using dataservice = PrintMagazine.DataAccessLayer.Extension;

namespace PrintMagazine.BusinessLogicLayer.Extension
{
    public static class ServiceInitializeExtension
    {
        public static void AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            dataservice.ServiceInitializeExtension.AddServices(services, configuration);
            #region Helpers
            services.AddScoped<IEmailHelper, EmailHelper>();
            services.AddScoped<IPasswordHelper, GenerateNewPassword>();
            #endregion

            #region Services
            services.AddScoped<IAccountService, AccountServices>();
            services.AddScoped<IAuthorService, AuthortServices>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IPrintingEditionService, PrintingEditionService>();
            services.AddScoped<IUserService, UserService>();
            #endregion
        }
    }
}

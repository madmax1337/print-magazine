﻿using PrintMagazine.BusinessLogicLayer.Models.PrintingEditions;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.PrtintingEdition;
using PrintMagazine.BusinessLogicLayer.Helpers;
using System;

namespace PrintMagazine.BusinessLogicLayer.Mapper
{
    public static class PrintingEditionMapper
    {
        public static PrintingEditionModelItem MapToPrintingEditionModel(this PrintingEditionDataModel printingEdition, Curency curency)
        {
            var item = new PrintingEditionModelItem();

            item.PrintingEditionId = printingEdition.PrintingEdition.Id;
            item.Title = printingEdition.PrintingEdition.Title;
            item.Description = printingEdition.PrintingEdition.Descriptyon;
            item.Price = (printingEdition.PrintingEdition.Price).CurencyConveterUsdToAny(curency);
            item.Status = printingEdition.PrintingEdition.Status;
            item.Curency = printingEdition.PrintingEdition.Curency;
            item.Type = printingEdition.PrintingEdition.Type;
            item.Authors = printingEdition.Authors;

            return item;
        }
        public static PrintingEdition MapToPrintingEdition(this PrintingEditionModelItem printingEditionModel)
        {
            var item = new PrintingEdition();

            item.Id = printingEditionModel.PrintingEditionId;
            item.Title = printingEditionModel.Title;
            item.Descriptyon = printingEditionModel.Description;
            item.Price = (printingEditionModel.Price).CurencyConverterAnyToUsd(printingEditionModel.Curency);
            item.Status = printingEditionModel.Status;
            item.Curency = Curency.USD;
            item.Type = printingEditionModel.Type;
            item.CreationDate = DateTime.Now;

            return item;
        }
        public static PrintingEdition MapForUpdateEntity(PrintingEdition printingEdition, PrintingEditionModelItem printingEditionModelItem)
        {
            printingEdition.Title = printingEditionModelItem.Title;
            printingEdition.Descriptyon = printingEditionModelItem.Description;
            printingEdition.Price = (printingEditionModelItem.Price).CurencyConverterAnyToUsd(printingEditionModelItem.Curency);
            printingEdition.Status = printingEditionModelItem.Status;
            printingEdition.Curency = Curency.USD;
            printingEdition.Type = printingEditionModelItem.Type;
            printingEdition.IsRemove = printingEditionModelItem.IsRemove;
            printingEdition.CreationDate = DateTime.Now;

            return printingEdition;
        }

        public static AuthorInPrintedEdition MapToAuthorInPrintingEdition(long printingEditionId, long authorId)
        {
            var authorInPrintingEdition = new AuthorInPrintedEdition();

            authorInPrintingEdition.AuthorId = authorId;
            authorInPrintingEdition.PrintingEditionsId = printingEditionId;
            authorInPrintingEdition.CreationDate = DateTime.Now;

            return authorInPrintingEdition;
        }
    }
}

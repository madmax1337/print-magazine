﻿using PrintMagazine.BusinessLogicLayer.Models.Filter;
using PrintMagazine.DataAccessLayer.Model.Filter;

namespace PrintMagazine.BusinessLogicLayer.Mapper
{
    public static class MapModelForDataAcces
    {
        public static UserFilterModelData MapUserFilter(this UserFilterModel userFilterModel)
        {
            var userFilterModelData = new UserFilterModelData();

            userFilterModelData.PageSize = userFilterModel.PageSize;
            userFilterModelData.PageIndex = userFilterModel.PageIndex;
            userFilterModelData.LockoutStatus = userFilterModel.LockoutStatus;
            userFilterModelData.SerchString = userFilterModel.SerchString;

            return userFilterModelData;
        }
        public static AuthorFilterModelData MapAuthorFilter(this AuthorFilterModel authorFilterModel)
        {
            var authorFilterModelData = new AuthorFilterModelData();

            authorFilterModelData.PageSize = authorFilterModel.PageSize;
            authorFilterModelData.PageIndex = authorFilterModel.PageIndex;
            authorFilterModelData.SerchString = authorFilterModel.SerchString;
            authorFilterModelData.SortColumType = authorFilterModelData.SortColumType;
            authorFilterModelData.SortType = authorFilterModel.SortType;

            return authorFilterModelData;
        }
        public static PrintingEditionFilterModelData MapPrintingEditionFilter(this PrintingEditionFilterModel printingEditionFilterModel)
        {
            var printingEditionFilterModelData = new PrintingEditionFilterModelData();

            printingEditionFilterModelData.PageSize = printingEditionFilterModel.PageSize;
            printingEditionFilterModelData.PageIndex = printingEditionFilterModel.PageIndex;
            printingEditionFilterModelData.SerchString = printingEditionFilterModel.SerchString;
            printingEditionFilterModelData.SortType = printingEditionFilterModel.SortType;
            printingEditionFilterModelData.MinPrice = printingEditionFilterModel.MinPrice;
            printingEditionFilterModelData.MaxPrice = printingEditionFilterModel.MaxPrice;
            printingEditionFilterModelData.PrintingEditionTypes = printingEditionFilterModel.PrintingEditionTypes;
            printingEditionFilterModelData.SortColumType = printingEditionFilterModel.SortColumType;

            return printingEditionFilterModelData;
        }
        public static OrderFilterModelData MapOrderFilter(this  OrderFilterModel orderFilterModel)
        {
            var orderFilterModelData = new OrderFilterModelData();

            orderFilterModelData.PageSize = orderFilterModel.PageSize;
            orderFilterModelData.PageIndex = orderFilterModel.PageIndex;
            orderFilterModelData.SortType = orderFilterModel.SortType;
            orderFilterModelData.OrderStatus = orderFilterModel.OrderStatus;
            orderFilterModelData.SortColumType = orderFilterModel.SortColumType;
            orderFilterModelData.UserId = orderFilterModel.UserId;

            return orderFilterModelData;
        }
    }
}

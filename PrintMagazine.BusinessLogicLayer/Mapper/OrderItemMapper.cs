﻿using PrintMagazine.BusinessLogicLayer.Models.BuyModel;
using PrintMagazine.DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PrintMagazine.BusinessLogicLayer.Mapper
{
    public static class OrderItemMapper
    {
        public static OrderItem MapToOrderItem(this CartModelItems cartModelItems, long orderId)
        {
            var item = new OrderItem();

            item.CreationDate = DateTime.Now.Date;
            item.Amount = cartModelItems.OrderAmount;
            item.Curency = cartModelItems.Curency;
            item.Count = cartModelItems.Count;
            item.OrderId = orderId;
            item.PrintingEditionId = cartModelItems.PrintingEditionId;

            return item;
        }
    }
}

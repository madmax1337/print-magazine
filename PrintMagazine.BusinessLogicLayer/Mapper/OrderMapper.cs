﻿using PrintMagazine.BusinessLogicLayer.Models.Order;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.OrderData;
using System;

namespace PrintMagazine.BusinessLogicLayer.Mapper
{
    public static class OrderMapper
    {
        public static OrderModelItem MapToOrderModelItem(this OrderDataModel orderDataModel)
        {
            var orderStatus = orderDataModel.Order.PaymentId == null ? OrderStatus.Unpayd : OrderStatus.Payd;

            var item = new OrderModelItem();

            item.OrderId = orderDataModel.Order.Id;
            item.Date = orderDataModel.Order.CreationDate;
            item.UserName = orderDataModel.Order.User.FirstName + " " + orderDataModel.Order.User.LastName;
            item.Email = orderDataModel.Order.User.Email;
            item.OrderItems = orderDataModel.OrderItems;
            item.OrderAmount = orderDataModel.Order.OrderAmout;
            item.OrderStatus = orderStatus;

            return item;
        }

        public static Order MapToOrder(string userId, long paymentId, decimal orderAmount)
        {
            var order = new Order();

            order.CreationDate = DateTime.Now.Date;
            order.UserId = userId;
            order.OrderAmout = orderAmount;

            if (paymentId != 0)
            {
                order.PaymentId = paymentId;
            }

            return order;
        }
    }
}

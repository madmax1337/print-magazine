﻿using PrintMagazine.BusinessLogicLayer.Models.SingUpModel;
using PrintMagazine.BusinessLogicLayer.Models.User;
using PrintMagazine.DataAccessLayer.Entities;

namespace PrintMagazine.BusinessLogicLayer.Mapper
{
    public static class UserMapper
    {
        public static UserModelItem MapToUserModel(this ApplicationUser user)
        {
            var isLockOut = user.LockoutEnd.Equals(null) ? false : true;

            var item = new UserModelItem();

            item.UserId = user.Id;
            item.UserName = user.UserName;
            item.FirstName = user.FirstName;
            item.LastName = user.LastName;
            item.Email = user.Email;
            item.Image = user.Image;
            item.IsLockout = isLockOut;

            return item;
        }
        public static ApplicationUser MapToAplicationUser(this SignUpModel signUpModel)
        {
            var items = new ApplicationUser();

            items.UserName = signUpModel.FirstName + " " + signUpModel.LastName;
            items.FirstName = signUpModel.FirstName;
            items.LastName = signUpModel.LastName;
            items.Email = signUpModel.Email;
            items.PasswordHash = signUpModel.Password;
            items.Image = signUpModel.Image;

            return items;
        }
        public static ApplicationUser MapForUpdateEntity(ApplicationUser user, UserModelItem userModelItem)
        {
            user.UserName = userModelItem.FirstName+" "+userModelItem.LastName;
            user.FirstName = userModelItem.FirstName;
            user.LastName = userModelItem.LastName;
            user.Email = userModelItem.Email;
            user.Image = userModelItem.Image;
            user.IsRemove = userModelItem.IsRemove;

            return user;
        }
    }
}

﻿using PrintMagazine.BusinessLogicLayer.Models.Authors;
using PrintMagazine.DataAccessLayer.Entities;
using PrintMagazine.DataAccessLayer.Model.AuthorData;
using System;

namespace PrintMagazine.BusinessLogicLayer.Mapper
{
    public static class AuthorMapper
    {
        public static AuthorModelItem MapToAuthorModelAsync(this AuthorDataModel author)
        {
            var item = new AuthorModelItem();

            item.AuthorId = author.Author.Id;
            item.Name = author.Author.Name;
            item.ProductTitles = author.ProductTitles;

            return item;
        }

        public static AuthorModelItem MapToAuthorModel(this Author author)
        {
            var item = new AuthorModelItem();

            item.AuthorId = author.Id;
            item.Name = author.Name;

            return item;
        }

        public static Author MapToAuthorAsync(this AuthorModelItem author)
        {
            var item = new Author();

            item.Name = author.Name;
            item.CreationDate = DateTime.Now;

            return item;
        }

        public static Author MapForUpdateEntity(Author author, AuthorModelItem authorModelItem)
        {
            author.Name = authorModelItem.Name;
            author.IsRemove = authorModelItem.IsRemove;
            author.CreationDate = DateTime.Now;

            return author;
        }
    }
}

﻿using PrintMagazine.BusinessLogicLayer.Models.Filter.Base;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using System.Collections.Generic;

namespace PrintMagazine.BusinessLogicLayer.Models.Filter
{
    public class PrintingEditionFilterModel : BaseFilterModel
    {
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public List<PrintingEditionType> PrintingEditionTypes { get; set; }
        public SortColumType SortColumType { get; set; } 
        public Curency Curency { get; set; }
    }
}

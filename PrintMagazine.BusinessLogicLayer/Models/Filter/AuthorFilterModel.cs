﻿using PrintMagazine.BusinessLogicLayer.Models.Filter.Base;
using PrintMagazine.DataAccessLayer.Entities.Enum;

namespace PrintMagazine.BusinessLogicLayer.Models.Filter
{
    public class AuthorFilterModel: BaseFilterModel
    {
        public SortColumType SortColumType { get; set; }
    }
}

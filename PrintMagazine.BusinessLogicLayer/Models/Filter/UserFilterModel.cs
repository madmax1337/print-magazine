﻿using PrintMagazine.BusinessLogicLayer.Models.Filter.Base;
using PrintMagazine.DataAccessLayer.Entities.Enum;

namespace PrintMagazine.BusinessLogicLayer.Models.Filter
{
    public class UserFilterModel : BaseFilterModel
    {
        public LockoutStatus LockoutStatus { get; set; }
    }
}

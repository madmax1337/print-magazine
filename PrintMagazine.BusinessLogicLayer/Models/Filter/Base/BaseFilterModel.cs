﻿using PrintMagazine.DataAccessLayer.Entities.Enum;

namespace PrintMagazine.BusinessLogicLayer.Models.Filter.Base
{
    public class BaseFilterModel
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string SerchString { get; set; }
        public SortType SortType { get; set; }
    }
}

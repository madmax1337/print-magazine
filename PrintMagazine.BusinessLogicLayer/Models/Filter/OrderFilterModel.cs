﻿using PrintMagazine.BusinessLogicLayer.Models.Filter.Base;
using PrintMagazine.DataAccessLayer.Entities.Enum;

namespace PrintMagazine.BusinessLogicLayer.Models.Filter
{
    public class OrderFilterModel : BaseFilterModel
    { 
        public string UserId {get; set;}
        public OrderStatus OrderStatus { get; set; }
        public SortColumType SortColumType { get; set; }
    }
}

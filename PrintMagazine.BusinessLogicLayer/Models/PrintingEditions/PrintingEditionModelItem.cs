﻿using PrintMagazine.DataAccessLayer.Entities.Enum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.DataAccessLayer.Model.PrtintingEdition;

namespace PrintMagazine.BusinessLogicLayer.Models.PrintingEditions
{
    public class PrintingEditionModelItem: BaseModel
    {
        public long PrintingEditionId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public StatusType Status { get; set; }
        public Curency Curency { get; set; }
        public PrintingEditionType Type { get; set; }
        public IEnumerable<PrintingEditionAuthor> Authors { get; set; }
        public bool IsRemove { get; set; }
    }
}

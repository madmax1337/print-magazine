﻿using PrintMagazine.BusinessLogicLayer.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.BusinessLogicLayer.Models.PrintingEditions
{
    public class PrintingEditionModel: BaseModel
    { 
        public decimal ItemCount { get; set; }
        public List<PrintingEditionModelItem> Items = new List<PrintingEditionModelItem>();
    }
}

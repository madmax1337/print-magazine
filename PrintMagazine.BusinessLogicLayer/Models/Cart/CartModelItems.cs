﻿using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.DataAccessLayer.Entities.Enum;

namespace PrintMagazine.BusinessLogicLayer.Models.BuyModel
{
    public class CartModelItems:BaseModel
    {
        public long PrintingEditionId { get; set; }
        public string Product { get; set; }
        public Curency Curency { get; set; }
        public decimal Price { get; set; }
        public int Count { get; set; }
        public decimal OrderAmount { get; set; }
    }
}

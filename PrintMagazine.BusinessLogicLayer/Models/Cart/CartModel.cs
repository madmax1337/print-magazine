﻿using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.BusinessLogicLayer.Models.BuyModel;
using System.Collections.Generic;

namespace PrintMagazine.BusinessLogicLayer.Models.Cart
{
    public class CartModel:BaseModel
    {
        public List<CartModelItems> Items = new List<CartModelItems>();
        public string TransactionId { get; set; }
        public decimal OrderAmount { get; set; }
    }
}

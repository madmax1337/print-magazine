﻿using System.Collections.Generic;

namespace PrintMagazine.BusinessLogicLayer.Models.Base
{
    public class BaseModel
    {
        public List<string> Errors = new List<string>();
    }
}

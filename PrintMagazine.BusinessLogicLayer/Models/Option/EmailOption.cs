﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.BusinessLogicLayer.Models.Option
{
    public class EmailOption
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Smtyp { get; set; }
        public int Port { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.BusinessLogicLayer.Models.Option
{
    public class JwtOption
    {
        public string Issues { get; set; }
        public string Audience { get; set; }
        public string JwtKey { get; set; }
    }
}

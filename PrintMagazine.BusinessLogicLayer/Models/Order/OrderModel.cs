﻿using PrintMagazine.BusinessLogicLayer.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.BusinessLogicLayer.Models.Order
{
    public class OrderModel: BaseModel
    {
        public decimal ItemCount { get; set; }
        public List<OrderModelItem> Items = new List<OrderModelItem>();
    }
}

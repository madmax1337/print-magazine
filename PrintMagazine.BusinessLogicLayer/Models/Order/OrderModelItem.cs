﻿using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using PrintMagazine.DataAccessLayer.Model.OrderData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PrintMagazine.BusinessLogicLayer.Models.Order
{
    public class OrderModelItem:BaseModel
    {
        public long OrderId { get; set; }
        public DateTime Date { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public IEnumerable<OrderItemDataModel> OrderItems { get; set; }
        public decimal OrderAmount { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public string TransactionId { get; set; }
    }
}

﻿using PrintMagazine.BusinessLogicLayer.Models.Base;
using System.Collections.Generic;

namespace PrintMagazine.BusinessLogicLayer.Models.Authors
{
    public class AuthorModel:BaseModel
    {
        public decimal ItemCount { get; set; }
        public List<AuthorModelItem> Items = new List<AuthorModelItem>();
    }
}

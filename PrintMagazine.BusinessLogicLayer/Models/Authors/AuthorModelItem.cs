﻿using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PrintMagazine.BusinessLogicLayer.Models.Authors
{
    public class AuthorModelItem:BaseModel
    {
        public long AuthorId { get; set; }
        public string Name { get; set; }
        public IEnumerable<string> ProductTitles { get; set; }
        public bool IsRemove { get; set; }
    }
}

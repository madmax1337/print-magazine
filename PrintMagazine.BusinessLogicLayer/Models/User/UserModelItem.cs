﻿using PrintMagazine.BusinessLogicLayer.Models.Base;
using PrintMagazine.DataAccessLayer.Entities.Enum;
using System.ComponentModel.DataAnnotations;

namespace PrintMagazine.BusinessLogicLayer.Models.User
{
    public class UserModelItem : BaseModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string Role { get; set; }
        public string Image { get; set; }
        public bool IsLockout { get; set; }
        public bool IsRemove { get; set; }
    }
}

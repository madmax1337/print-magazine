﻿using PrintMagazine.BusinessLogicLayer.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.BusinessLogicLayer.Models.User
{
    public class UserModel : BaseModel
    {
        public decimal ItemCount {get; set;}
        public List<UserModelItem> Items = new List<UserModelItem>();
    }
}

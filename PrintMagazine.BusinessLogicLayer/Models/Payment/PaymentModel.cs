﻿using PrintMagazine.BusinessLogicLayer.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.BusinessLogicLayer.Models.Payment
{
    public class PaymentModel: BaseModel
    {
        public List<PaymentModelItem> Items = new List<PaymentModelItem>();
    }
}

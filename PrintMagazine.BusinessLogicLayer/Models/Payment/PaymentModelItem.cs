﻿using PrintMagazine.BusinessLogicLayer.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.BusinessLogicLayer.Models.Payment
{
    public class PaymentModelItem:BaseModel
    { 
        public string TransactionId { get; set; }
        public string Description { get; set; }
    }
}

﻿using PrintMagazine.BusinessLogicLayer.Models.Base;

namespace PrintMagazine.BusinessLogicLayer.Models.Token
{
    public class TokenModel
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace PrintMagazine.BusinessLogicLayer.Models.PasswordModels
{
    public class ForgotPasswordModel
    {
        [Required]
        public string Email { get; set; }
    }
}

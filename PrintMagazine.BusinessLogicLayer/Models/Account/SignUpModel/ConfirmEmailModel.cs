﻿using Microsoft.AspNetCore.Identity;
using PrintMagazine.BusinessLogicLayer.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace PrintMagazine.BusinessLogicLayer.Models.Account.SignUpModel
{
    public class ConfirmEmailModel: BaseModel
    {
        public string Id { get; set; }
        public string Code { get; set; }
    }
}

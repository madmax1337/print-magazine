import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule,
         MatInputModule,
         MatButtonModule,
         MatCheckboxModule,
         MatCardModule,
         MatIconModule,
         MatTooltipModule,
         MatGridListModule,
         MatTableModule,
         MatSlideToggleModule,
         MatPaginatorModule,
         MatDialogModule,
         MatMenuModule} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserRoutes } from 'src/app/user/user.routing';
import { UserProfileComponent } from 'src/app/user/user-profile/user-profile.component';
import { AllUsersComponent } from 'src/app/user/all-users/all-users.component';
import { DialogEditUserComponent } from 'src/app/user/all-users/dialog-edit-user/dialog-edit-user.component';


@NgModule({
  imports: [
    CommonModule,
    UserRoutes,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatGridListModule,
    MatTableModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatPaginatorModule,
    MatDialogModule,
    MatMenuModule
  ],
  declarations: [
    UserProfileComponent,
    AllUsersComponent,
    DialogEditUserComponent,
  ],
  entryComponents: [
    DialogEditUserComponent,
  ]
})
export class UserModule { }

import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UserModelItem, BaseModel } from 'src/app/shared/models/index';
import { UserService, AccountService, LocalStorageService } from 'src/app/shared/services/index';
import { PatternConstants } from 'src/app/shared/constants';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})

export class UserProfileComponent {

  hidePasword: boolean;
  hideNewPassword: boolean;
  userForm: FormGroup;
  isEdit: boolean;
  userModelItem: UserModelItem = new UserModelItem();
  isReadOnly = true;
  image: string;
  isPhotoEmpty: boolean;
  reader = new FileReader();

  constructor(
    private formBuilder: FormBuilder,
    private localStorageService: LocalStorageService,
    private accountSrvices: AccountService,
    private userService: UserService,
    private patternConstatn: PatternConstants) {
    this.formGroup();
    this.initializationDate();
  }

  initializationDate(): void {
    this.userModelItem.firstName = this.localStorageService.FirstName;
    this.userModelItem.lastName = this.localStorageService.LastName;
    this.userModelItem.email = this.localStorageService.Email;
    this.userModelItem.userId = this.localStorageService.UserId;
    this.userModelItem.role = this.localStorageService.Role;
    this.image = this.localStorageService.Image;
    this.isPhotoEmpty = !this.localStorageService.Image ? false : true;
  }

  formGroup(): void {
    this.userForm = this.formBuilder.group({
      firstName: new FormControl(null, [Validators.required, Validators.pattern(this.patternConstatn.namePattern)]),
      lastName: new FormControl(null, [Validators.required, Validators.pattern(this.patternConstatn.namePattern)]),
      email: new FormControl(null, [Validators.required, Validators.pattern(this.patternConstatn.emailPattern)]),
      password: new FormControl(null, [Validators.pattern(this.patternConstatn.passwordPattern)]),
      newPassword: new FormControl(null, [Validators.pattern(this.patternConstatn.passwordPattern)])
    });
  }

  onSelectFile(event): void {
    if (event.target.files && event.target.files[0]) {
      this.reader.readAsDataURL(event.target.files[0]);
      this.reader.onload = () => {
        this.image = this.reader.result.toString();
      };
    }

    this.isPhotoEmpty = this.image ? true : false;
  }

  edit(): void {
    this.isEdit = true;
    this.isReadOnly = false;
  }

  cancel(): void {
    this.isEdit = false;
    this.isReadOnly = true;
    this.isPhotoEmpty = !this.localStorageService.Image ? false : true;
  }

  save(): void {
    if (this.userForm.invalid) {
      return;
    }

    this.userModelItem.image = this.image;

    this.userService.update(this.userModelItem).subscribe((data: BaseModel) => {
      if (data.errors.length === 0) {
        this.localStorageService.clear();

        this.addToLocalStorage();

        this.accountSrvices.NotifieSignInResult();

        this.isEdit = false;
        this.isReadOnly = true;
      }
      this.userModelItem.errors = data.errors;
    });
  }

  addToLocalStorage(): void {
    this.localStorageService.setItem('userId', this.userModelItem.userId);
    let userName = this.userModelItem.firstName + ' ' + this.userModelItem.lastName;
    this.localStorageService.setItem('userName', userName);
    this.localStorageService.setItem('firstName', this.userModelItem.firstName);
    this.localStorageService.setItem('lastName', this.userModelItem.lastName);
    this.localStorageService.setItem('email', this.userModelItem.email);
    this.localStorageService.setItem('role', this.userModelItem.role);
    this.localStorageService.setItem('image', this.userModelItem.image);
  }

}

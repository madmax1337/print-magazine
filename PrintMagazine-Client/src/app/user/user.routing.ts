import { Routes, RouterModule } from '@angular/router';
import { UserProfileComponent } from 'src/app/user/user-profile/user-profile.component';
import { AllUsersComponent } from 'src/app/user/all-users/all-users.component';
import { AuthGuard } from 'src/app/shared/helper/index';

const routes: Routes = [
  { path: 'userProfile', component: UserProfileComponent, canActivate: [AuthGuard]},
  { path: 'allUser', component: AllUsersComponent, canActivate: [AuthGuard], data: {role: 'Admin'}},
];

export const UserRoutes = RouterModule.forChild(routes);

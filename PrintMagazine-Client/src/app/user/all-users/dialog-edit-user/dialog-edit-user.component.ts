import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UserModelItem } from 'src/app/shared/models';
import { PatternConstants } from 'src/app/shared/constants';

@Component({
  selector: 'app-dialog-edit-user',
  templateUrl: './dialog-edit-user.component.html',
  styleUrls: ['./dialog-edit-user.component.css']
})
export class DialogEditUserComponent {

  userForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<DialogEditUserComponent>,
    private patternConstant: PatternConstants,
    @Inject(MAT_DIALOG_DATA) public userData: UserModelItem) {

    this.formGroup();

  }

formGroup(): void {
  this.userForm = this.formBuilder.group({
    firstName: new FormControl(null, [Validators.required, Validators.pattern(this.patternConstant.namePattern)]),
    lastName: new FormControl(null, [Validators.required, Validators.pattern(this.patternConstant.namePattern)]),
    email: new FormControl(null, [Validators.required, Validators.pattern(this.patternConstant.emailPattern)])
  });
}

  closeWindow(): void {
    this.dialogRef.close();
  }
}

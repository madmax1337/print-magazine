import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator, MatDialog, MatCheckboxChange } from '@angular/material';
import { UserService } from 'src/app/shared/services';
import { UserModel, UserFilter, CheckBoxItem, UserModelItem, PaginationModel, BaseModel } from 'src/app/shared/models/index';
import { LockoutStatus } from 'src/app/shared/enum/index';
import { DialogEditUserComponent } from 'src/app/user/all-users/dialog-edit-user/dialog-edit-user.component';
import { DialogDeleteItemComponent } from 'src/app/shared/components/dialog-delete-item/dialog-delete-item.component';
import { TableConstant } from 'src/app/shared/constants/index';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.css']
})
export class AllUsersComponent {

  filterForm: FormGroup;
  userFilter: UserFilter;
  userModel: UserModel;
  paginationModel: PaginationModel;
  displayedColumns: string[];
  lockoutStatusOption: Array<CheckBoxItem>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    public dialog: MatDialog,
    private gb: FormBuilder,
    private userService: UserService,
    private tableConstant: TableConstant) {

    this.lockoutStatusOption = new Array<CheckBoxItem>();
    this.userFilter = new UserFilter();
    this.userModel = new UserModel();
    this.paginationModel = new PaginationModel();
    this.displayedColumns = this.tableConstant.displayedColumUser;

    this.lockoutStatusOption.push(new CheckBoxItem(LockoutStatus.Active, LockoutStatus[LockoutStatus.Active], true));
    this.lockoutStatusOption.push(new CheckBoxItem(LockoutStatus.Blocked, LockoutStatus[LockoutStatus.Blocked], true));

    this.getUsers(this.paginationModel.pageSize, this.paginationModel.pageIndex);
  }

  filterUser(): void {
    this.paginator.pageIndex = this.paginationModel.pageIndex;
    this.getUsers(this.paginator.pageSize, this.paginator.pageIndex);
  }

  getUsers(pageSize: number, pageNumber: number): void {
    this.onToggle();

    this.userFilter.pageSize = pageSize;
    this.userFilter.pageIndex = pageNumber;

    this.userService.getAll(this.userFilter).subscribe((data: UserModel) => {
      if (data.errors.length === 0 && data !== null) {
        this.userModel = data;
        this.paginator.length = data.itemCount;
      }
      if (data.errors.length > 0) {
        this.userModel.errors = data.errors;
      }
    });

  }

  deleteUser(element: UserModelItem): void {
    const dialogRef = this.dialog.open(DialogDeleteItemComponent, {
      data:  true
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result == true) {
        element.isRemove = true;
        this.userService.delete(element).subscribe((data: BaseModel) => {
          if (data.errors.length > 0 && data !== null) {
            this.userModel.errors = data.errors;
          }
          this.filterUser();
        });
      }
    });
  }

  setLockout(element: UserModelItem, checked: boolean): void {
    element.isLockout = !checked;
    this.userService.setLockout(element).subscribe((data: BaseModel) => {
      if (data.errors.length !== 0 && data !== null) {
        this.userModel.errors = data.errors;
      }
    });

  }

  editUser(element: UserModelItem): void {
    const dialogRef = this.dialog.open(DialogEditUserComponent, {
      data: {
        userId: element.userId,
        userName: element.userName,
        firstName: element.firstName,
        lastName: element.lastName,
        email: element.email
      }
    });

    dialogRef.afterClosed().subscribe((result: UserModelItem) => {
      if (result != null) {
        this.userService.update(result).subscribe((data: BaseModel) => {
          if (data.errors.length > 0 && data !== null) {
            this.userModel.errors = data.errors;
          }
          this.filterUser();
        });
      }
    });
  }

  onChecked(eventEmitter: MatCheckboxChange, id: number): void {
    if (!this.lockoutStatusOption.some(x => x.checked === true)) {
      eventEmitter.source.toggle();
      this.lockoutStatusOption[id].checked = true;
    }
  }

  onToggle(): void {
    const checkedOptions = this.lockoutStatusOption.filter(x => x.checked);
    this.userFilter.lockoutStatus = checkedOptions[0].value;
    if (checkedOptions.length === 2) {
      this.userFilter.lockoutStatus = LockoutStatus.All;
    }
  }
}

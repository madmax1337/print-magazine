import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { AppComponent } from 'src/app/app.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { httpInterceptorProviders } from 'src/app/shared/interceptor/index';


@NgModule({
   declarations: [
      AppComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      AppRoutingModule,
      SharedModule,
      BrowserAnimationsModule,
      MatFormFieldModule,
   ],
   providers: [
      httpInterceptorProviders,
      CookieService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }

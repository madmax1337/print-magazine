import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {  MatTableModule,
  MatPaginatorModule,
  MatButtonModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSortModule,
  MatInputModule,
  MatDialogModule} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuthorComponent } from 'src/app/author/author.component';
import { AuthorRoutes } from 'src/app/author/author.routing';

import { DialogAuthorComponent } from 'src/app/author/dialog-author/dialog-author.component';

@NgModule({
  imports: [
    CommonModule,
    AuthorRoutes,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatSortModule,
    ReactiveFormsModule,
    MatInputModule,
    FormsModule,
    MatDialogModule
  ],
  declarations: [
    AuthorComponent,
    DialogAuthorComponent
  ],
  entryComponents: [
    DialogAuthorComponent
  ]
})
export class AuthorModule { }

import { Component, ViewChild } from '@angular/core';
import { MatPaginator, Sort, MatDialog } from '@angular/material';
import { AuthorFilter, AuthorModel, AuthorModelItem, PaginationModel } from 'src/app/shared/models/index';
import { AuthorService } from 'src/app/shared/services/index';
import { DialogAuthorComponent } from 'src/app/author/dialog-author/dialog-author.component';
import { SortColumType, SortType } from 'src/app/shared/enum/index';
import { DialogDeleteItemComponent } from 'src/app/shared/components/dialog-delete-item/dialog-delete-item.component';
import { TableConstant, EmptyStringConstants } from 'src/app/shared/constants/index';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent {
  haveError: boolean;
  paginationModel: PaginationModel;
  displayedColumns: string[];
  authorFilter: AuthorFilter;
  authorModel: AuthorModel;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private authorService: AuthorService,
    public dialog: MatDialog,
    private tableConstant: TableConstant,
    private emptyStringConstants: EmptyStringConstants) {

    this.paginationModel = new PaginationModel();
    this.authorFilter = new AuthorFilter();
    this.authorModel = new AuthorModel();

    this.displayedColumns = this.tableConstant.displayColumAuthor;

    this.getAuthorData(this.paginationModel.pageSize, this.paginationModel.pageIndex);
  }

  sortDataAuthor(sort: Sort): void {
    if (sort.active || sort.direction !== this.emptyStringConstants.emptyString) {
      this.authorFilter.sortColumType = SortColumType[sort.active];
      this.authorFilter.sortType = SortType[sort.direction];
    }

    this.paginator.pageIndex = this.paginationModel.pageIndex;
    this.getAuthorData(this.paginator.pageSize, this.paginator.pageIndex);
  }

  filterAuthor(): void {
    this.paginator.pageIndex = this.paginationModel.pageIndex;
    this.getAuthorData(this.paginator.pageSize, this.paginator.pageIndex);
  }

  getAuthorData(pageSize: number, pageIndex: number): void {
    this.authorFilter.pageSize = pageSize;
    this.authorFilter.pageIndex = pageIndex;

    this.authorService.getAll(this.authorFilter).subscribe((data: AuthorModel) => {
      if (data.errors.length === 0) {
        this.paginator.length = data.itemCount;
        this.authorModel = data;
      }

      if (data.errors.length > 0) {
        this.haveError = true;
        this.authorModel = data;
      }
    });
  }

  deleteAuthor(element: AuthorModelItem): void {

    const dialogRef = this.dialog.open(DialogDeleteItemComponent, {
      data:  true
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result == true) {
        element.isRemove = true;
        this.authorService.delete(element).subscribe((data: AuthorModelItem) => {
          if (data.errors.length > 0) {
            this.authorModel.errors = data.errors;
            return;
          }
          this.filterAuthor();
        });
      }
    });

  }

  addAuthor(): void {
    const dialogRef = this.dialog.open(DialogAuthorComponent, {});

    dialogRef.afterClosed().subscribe((result: AuthorModelItem) => {
      if (result != null) {
        this.authorService.create(result).subscribe((data: AuthorModelItem) => {
          if (data.errors.length > 0) {
            this.authorModel.errors = data.errors;
            return;
          }
          this.filterAuthor();
        });
      }
    });
  }

  updateAuthor(element: AuthorModelItem): void {

    const dialogRef = this.dialog.open(DialogAuthorComponent, {
      data: {
        authorId: element.authorId,
        name: element.name
      }
    });

    dialogRef.afterClosed().subscribe((result: AuthorModelItem) => {
      if (result != null) {
        this.authorService.update(result).subscribe((data: AuthorModelItem) => {
          if (data.errors.length > 0) {
            this.authorModel.errors = data.errors;
            return;
          }
          this.filterAuthor();
        });
      }
    });

  }
}

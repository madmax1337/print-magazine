import { Routes, RouterModule } from '@angular/router';
import { AuthorComponent } from 'src/app/author/author.component';
import { AuthGuard } from 'src/app/shared/helper/index';

const routes: Routes = [
  { path: 'viewAll', component: AuthorComponent, canActivate: [AuthGuard], data: {role: 'Admin'} },
];

export const AuthorRoutes = RouterModule.forChild(routes);

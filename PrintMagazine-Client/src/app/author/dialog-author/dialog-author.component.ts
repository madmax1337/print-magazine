import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthorModelItem } from 'src/app/shared/models/index';
import { PatternConstants } from 'src/app/shared/constants/index';

@Component({
  selector: 'app-dialog-author',
  templateUrl: './dialog-author.component.html',
  styleUrls: ['./dialog-author.component.css']
})
export class DialogAuthorComponent {

  isUpdate: boolean;
  authorForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<DialogAuthorComponent>,
    private patternConstatn: PatternConstants,
    @Inject(MAT_DIALOG_DATA) public authorData: AuthorModelItem) {

    this.intializationData();

    this.formGroup();
  }

  intializationData(): void {
    if (this.authorData !== null) {
      this.isUpdate = true;
    }

    if (this.authorData === null) {
      this.authorData = new AuthorModelItem();
      this.isUpdate = false;
    }
  }

  formGroup(): void {
    this.authorForm = this.formBuilder.group({
      name: new FormControl(null, [Validators.required, Validators.pattern(this.patternConstatn.authorNamePattern)])
    });
  }

  closeWindow(): void {
    this.dialogRef.close();
  }

}

import { Component } from '@angular/core';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ForgotPasswordModel, BaseModel } from 'src/app/shared/models/index';
import { AccountService } from 'src/app/shared/services/index';
import { PatternConstants } from 'src/app/shared/constants/index';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {

  baseModel: BaseModel;
  forgotPasswordModel: ForgotPasswordModel;
  forgotPasswordForm: FormGroup;
  isValidFormSubmitted: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private patterConstant: PatternConstants) {

    this.forgotPasswordModel = new ForgotPasswordModel();
    this.baseModel = new BaseModel();

    this.formGroup();
  }

  formGroup(): void {
    this.forgotPasswordForm = this.formBuilder.group({
      email: new FormControl(null, [Validators.required, Validators.pattern(this.patterConstant.emailPattern)])
    });
  }

  submit(): void {
    if (this.forgotPasswordForm.invalid) {
      return;
    }

    this.accountService.forgotPassword(this.forgotPasswordModel).subscribe((data: BaseModel) => {
      if (data.errors === null) {
        this.isValidFormSubmitted = true;
        console.log(this.baseModel);
      }
      this.baseModel = data;
    });
  }
}

import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from 'src/app/account/sign-in/sign-in.component';
import { ForgotPasswordComponent} from 'src/app/account/forgot-password/forgot-password.component';
import { SignUpComponent } from 'src/app/account/sign-up/sign-up.component';
import { ConfirmEmailComponent } from 'src/app/account/confirm-email/confirm-email.component';

const routes: Routes = [
  { path : 'signIn', component: SignInComponent},
  { path : 'forgotPassword', component: ForgotPasswordComponent},
  { path : 'signUp', component: SignUpComponent},
  { path : 'confirmEmail', component: ConfirmEmailComponent}
];

export const AccountRoutes = RouterModule.forChild(routes);

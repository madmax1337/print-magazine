import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalStorageService } from 'src/app/shared/services/index';
import { NavigateConstants } from 'src/app/shared/constants/index';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.css']
})
export class ConfirmEmailComponent implements OnInit {

  hideConfirmEmail: boolean;
  error: string;

  get firstName() {
    return this.localStorageService.FirstName;
  }
  get lastName() {
    return this.localStorageService.LastName;
  }


  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private route: ActivatedRoute,
    private navigateConstant: NavigateConstants) {
  }

  ngOnInit() {
    this.getErrors();
  }

  getErrors(): void {
    if (this.route.snapshot.queryParamMap.get('errors')) {
      this.error = this.route.snapshot.queryParamMap.get('errors');
      this.hideConfirmEmail = true;
    }
  }

  continue(): void {
    this.router.navigate([this.navigateConstant.signIn]);
    this.localStorageService.clear();
  }
}

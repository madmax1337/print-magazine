import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule,
         MatInputModule,
         MatButtonModule,
         MatCheckboxModule,
         MatCardModule,
         MatIconModule,
         MatProgressSpinnerModule  } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccountRoutes } from 'src/app/account/account.routing';
import { SignInComponent } from 'src/app/account/sign-in/sign-in.component';
import { SignUpComponent } from 'src/app/account/sign-up/sign-up.component';
import { ForgotPasswordComponent } from 'src/app/account/forgot-password/forgot-password.component';
import { ConfirmEmailComponent } from 'src/app/account/confirm-email/confirm-email.component';

@NgModule({
  imports: [
    CommonModule,
    AccountRoutes,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule
  ],
  declarations: [
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    ConfirmEmailComponent
  ],
  providers: []
})
export class AccountModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { Subscription } from 'rxjs';
import { SignInModel, BaseModel, RememberMeModel, UserModelItem } from 'src/app/shared/models/index';
import { AccountService, LocalStorageService } from 'src/app/shared/services/index';
import { PatternConstants, NavigateConstants } from 'src/app/shared/constants/index';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
})
export class SignInComponent implements OnInit {

  signInModel: SignInModel;
  hide: boolean;
  loginForm: FormGroup;
  baseModel: BaseModel;
  rememberMe: boolean;
  rememberMeModel: RememberMeModel;
  isLogInAdmin: boolean;

  subscription: Subscription;

  constructor(
    private accountService: AccountService,
    private fb: FormBuilder,
    private router: Router,
    private localStorageServise: LocalStorageService,
    private patterConstant: PatternConstants,
    private navigateConstants: NavigateConstants,
    private cookieService: CookieService) {

    this.rememberMeModel = new RememberMeModel();
    this.signInModel = new SignInModel();
    this.baseModel = new BaseModel();

    this.formGroup();
  }



  ngOnInit() {
    this.subscription = this.accountService.getLoggedIn().subscribe((data: boolean) => {
      if (history.state.data != null) {
        this.isLogInAdmin = history.state.data.param;
      }

      if (data === false) {
        this.isLogInAdmin = data;
      }
    });
  }

  formGroup(): void {
    this.loginForm = this.fb.group({
      email: new FormControl(null, [Validators.required, Validators.pattern(this.patterConstant.emailPattern)]),
      password: new FormControl(null, [Validators.required, Validators.pattern(this.patterConstant.passwordPattern)])
    });
  }

  addToLocalStorage(data: UserModelItem): void {
    this.localStorageServise.setItem('userId', data.userId);
    this.localStorageServise.setItem('userName', data.userName);
    this.localStorageServise.setItem('firstName', data.firstName);
    this.localStorageServise.setItem('lastName', data.lastName);
    this.localStorageServise.setItem('email', data.email);
    this.localStorageServise.setItem('role', data.role);

    if (data.image) {
      this.localStorageServise.setItem('image', data.image);
    }
  }

  checkRole(role: string): void {
    if (role === 'Admin') {
      this.router.navigate([this.navigateConstants.printingEditionAdmin]);
    }

    if (role === 'User') {
      this.router.navigate([this.navigateConstants.printingEditionUser]);
    }
  }

  rememberMeFunction(userId: string, hours: number): void {
    let expire = new Date();
    let time = Date.now() + ((3600 * 1000) * hours);

    expire.setTime(time);
    this.rememberMeModel.rememberMe = this.rememberMe;
    this.rememberMeModel.userId = userId;
    this.cookieService.set('rememberMe', JSON.stringify(this.rememberMeModel), expire, '/');
  }

  submit(): void {
    if (this.loginForm.invalid) {
      return;
    }

    this.accountService.signIn(this.signInModel).subscribe((data: UserModelItem) => {
      if (data.errors.length === 0 && data !== null) {
        this.addToLocalStorage(data);
        this.accountService.NotifieSignInResult();
        this.checkRole(data.role);

        if (this.rememberMe) {
          this.rememberMeFunction(data.userId, 720);
        }

        if (!this.rememberMe) {
          this.rememberMeFunction(data.userId, 12);
        }

      }
      this.baseModel = data;
    });
  }

  submitAdmin(): void {
    if (this.loginForm.invalid) {
      return;
    }

    this.accountService.signIn(this.signInModel).subscribe(data => {
      if (data.errors.length === 0 && data !== null) {
        if (data.role !== 'Admin') {
          this.baseModel.errors = new Array<string>();
          this.baseModel.errors.push('Your not a admin!!!');
          return;
        }

        this.addToLocalStorage(data);
        this.accountService.NotifieSignInResult();
        this.rememberMeFunction(data.userId, 12);
        this.router.navigate([this.navigateConstants.printingEditionAdmin]);

      }
      this.baseModel = data;
    });
  }

}

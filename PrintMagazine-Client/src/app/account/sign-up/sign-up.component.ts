import { Component } from '@angular/core';
import { Validators, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { SignUpModel, BaseModel } from 'src/app/shared/models/index';
import { AccountService, LocalStorageService } from 'src/app/shared/services/index';
import { PatternConstants } from 'src/app/shared/constants/index';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent {
  signUpModel: SignUpModel;
  baseModel: BaseModel;
  hidePasword: boolean;
  hideConfirmPassword: boolean;
  signUpForm: FormGroup;
  isValidFormSubmitted: boolean;
  visibleSpiner: boolean;
  url: string;
  reader: FileReader;

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private patterConstant: PatternConstants,
    private localStorageServices: LocalStorageService) {

    this.signUpModel = new SignUpModel();
    this.baseModel = new BaseModel();
    this.reader = new FileReader();

    this.formGroup();
  }

  formGroup(): void {
    this.signUpForm = this.formBuilder.group({
      firstName: new FormControl(null, [Validators.required, Validators.pattern(this.patterConstant.namePattern)]),
      lastName: new FormControl(null, [Validators.required, Validators.pattern(this.patterConstant.namePattern)]),
      email: new FormControl(null, [Validators.required, Validators.pattern(this.patterConstant.emailPattern)]),
      password: new FormControl(null, [Validators.required, Validators.pattern(this.patterConstant.passwordPattern)]),
      passwordConfirm: new FormControl(null, [Validators.required, Validators.pattern(this.patterConstant.passwordPattern)])
    });
  }

  onSelectFile(event): void {
    if (event.target.files && event.target.files[0]) {
      this.reader.readAsDataURL(event.target.files[0]);
      this.reader.onload = () => {
        this.url = this.reader.result.toString();
      };
    }
  }

  submit(): void {

    if (this.signUpForm.invalid) {
      return;
    }

    this.visibleSpiner = true;
    this.signUpModel.img = this.url;

    this.accountService.signUp(this.signUpModel).subscribe((data: BaseModel) => {
      if (data.errors.length === 0) {
        this.isValidFormSubmitted = true;
        localStorage.setItem('firstName', this.signUpModel.firstName);
        localStorage.setItem('lastName', this.signUpModel.lastName);
      }

      if (data.errors.length > 0) {
        this.visibleSpiner = false;
      }

      this.baseModel = data;
    });
  }
}

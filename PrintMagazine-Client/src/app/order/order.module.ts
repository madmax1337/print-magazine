import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatButtonModule,
  MatSortModule,
  MatSelectModule,
  MatCheckboxModule,
  MatMenuModule,
  MatDialogModule} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AdminOrderComponent } from 'src/app/order/admin-order/admin-order.component';
import { UserOrderComponent } from 'src/app/order/user-order/user-order.component';
import { OrderRoutes } from 'src/app/order/order.routing';


@NgModule({
  imports: [
    CommonModule,
    OrderRoutes,
    MatTableModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatButtonModule,
    MatSortModule,
    MatSelectModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    FormsModule,
    MatMenuModule,
  ],
  declarations: [
    AdminOrderComponent,
    UserOrderComponent,
  ]
})
export class OrderModule { }

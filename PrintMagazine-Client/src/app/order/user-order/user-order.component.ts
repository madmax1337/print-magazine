import { Component, Predicate } from '@angular/core';
import { MatDialog } from '@angular/material';
import { OrderModelItem, BaseModel } from 'src/app/shared/models/index';
import { OrderService, LocalStorageService, PaymentService } from 'src/app/shared/services/index';
import { OrderStatus } from 'src/app/shared/enum/index';
import { AdminOrderComponent } from 'src/app/order/admin-order/admin-order.component';
import { DialogAfterPayComponent } from 'src/app/shared/components/dialog-after-pay/dialog-after-pay.component';
import { TableConstant, EmptyStringConstants } from 'src/app/shared/constants/index';

@Component({
  selector: 'app-user-order',
  templateUrl: './user-order.component.html',
  styleUrls: ['./user-order.component.css']
})
export class UserOrderComponent extends AdminOrderComponent {
  displayedColumns: string[];
  callBack: Predicate<string>;
  orderModelItem: OrderModelItem;
  errorMsg: string[];
  orderId: number;


  constructor(
    orderService: OrderService,
    tableConstant: TableConstant,
    emptyStringConstant: EmptyStringConstants,
    private localStorageService: LocalStorageService,
    private paymentService: PaymentService,
    public dialog: MatDialog) {

    super(orderService, tableConstant, emptyStringConstant);

    this.orderModelItem = new OrderModelItem();
    this.displayedColumns = this.tableConstant.displayColumOrderUser;
    this.orderFilter.userId = this.localStorageService.UserId;

    this.getOrderData(this.paginationModel.pageSize, this.paginationModel.pageIndex);

    this.paymentService.loadStripe();

    this.callBack = this.updateOrder.bind(this);

  }

  updateOrder(paymentId: string): void {
    this.update(paymentId);
  }


  pay(amount, orderId: number): void {
    this.paymentService.pay(amount, this.callBack, orderId);
    this.orderId = orderId;
  }

  update(transactionId: string): void {

    this.orderModelItem.transactionId = transactionId;
    this.orderModelItem.orderId = this.orderId;

    this.orderService.updateOrder(this.orderModelItem).subscribe((data: BaseModel) => {
      if (data.errors.length === 0) {
        this.errorMsg = data.errors;
        this.haveError = true;
      }
    });

    let dialogRef = this.dialog.open(DialogAfterPayComponent, { data: this.orderId });
    dialogRef.afterClosed().subscribe((result: number) => {
      if (result !== 0) {
        this.paginator.pageIndex = this.paginationModel.pageIndex;
        this.getOrderData(this.paginator.pageSize, this.paginationModel.pageIndex);
      }
    });

  }

  checkOrderStatus(status: OrderStatus): boolean {
    return status === OrderStatus.Unpayd ? true : false;
  }
}

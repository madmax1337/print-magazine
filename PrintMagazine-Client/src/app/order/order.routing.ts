import { Routes, RouterModule } from '@angular/router';
import { UserOrderComponent } from 'src/app/order/user-order/user-order.component';
import { AdminOrderComponent } from 'src/app/order/admin-order/admin-order.component';
import { AuthGuard } from 'src/app/shared/helper/index';

const routes: Routes = [
  {path: 'userOrder', component: UserOrderComponent, canActivate: [AuthGuard], data: {role: 'User'}},
  {path: 'adminOrder', component: AdminOrderComponent, canActivate: [AuthGuard], data: {role: 'Admin'}}
];

export const OrderRoutes = RouterModule.forChild(routes);

import { Component, ViewChild } from '@angular/core';
import { MatPaginator, Sort, MatCheckboxChange } from '@angular/material';
import { OrderModel, OrderFilter, CheckBoxItem, PaginationModel } from 'src/app/shared/models/index';
import { PrintingEditionType, OrderStatus, SortType, SortColumType } from 'src/app/shared/enum/index';
import { TableConstant, EmptyStringConstants } from 'src/app/shared/constants/index';
import { OrderService } from 'src/app/shared/services/index';

@Component({
  selector: 'app-admin-order',
  templateUrl: './admin-order.component.html',
  styleUrls: ['./admin-order.component.css']
})
export class AdminOrderComponent {

  haveError: boolean;
  paginationModel: PaginationModel;
  displayedColumns: string[];
  orderModel: OrderModel;
  orderFilter: OrderFilter;
  orderStatusOption: Array<CheckBoxItem>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    public orderService: OrderService,
    public tableConstant: TableConstant,
    public emptyStringConstant: EmptyStringConstants) {

    this.orderModel = new OrderModel();
    this.orderFilter = new OrderFilter();
    this.orderStatusOption = new Array<CheckBoxItem>();
    this.paginationModel = new PaginationModel();
    this.displayedColumns = this.tableConstant.displayColumOrderAdmin;

    this.orderStatusOption.push(new CheckBoxItem(OrderStatus.Payd, OrderStatus[OrderStatus.Payd], true));
    this.orderStatusOption.push(new CheckBoxItem(OrderStatus.Unpayd, OrderStatus[OrderStatus.Unpayd], true));

    this.getOrderData(this.paginationModel.pageSize, this.paginationModel.pageIndex);
  }

  getTypeProduct(id: number): string {
    return PrintingEditionType[id];
  }

  getOrderStatus(id: number): string {
    return OrderStatus[id];
  }

  filterOrder(): void {
    this.paginator.pageIndex = this.paginationModel.pageIndex;
    this.getOrderData(this.paginator.pageSize, this.paginator.pageIndex);
  }

  sortDataOrder(sort: Sort): void {

    if (sort.active || sort.direction !== this.emptyStringConstant.emptyString) {
      this.orderFilter.sortColumType = SortColumType[sort.active];
      this.orderFilter.sortType = SortType[sort.direction];
    }

    this.paginator.pageIndex = this.paginationModel.pageIndex;
    this.getOrderData(this.paginator.pageSize, this.paginator.pageIndex);
  }

  getOrderData(pageSize: number, pageIndex: number): void {

    this.onToggle();

    this.orderFilter.pageSize = pageSize;
    this.orderFilter.pageIndex = pageIndex;

    this.orderService.getAll(this.orderFilter).subscribe((data: OrderModel) => {

      if (data.errors.length === 0) {
        this.orderModel = data;
        this.paginator.length = data.itemCount;
      }

      if (data.errors.length > 0) {
        this.orderModel = data;
        this.haveError = true;
      }

    });

  }

  onChecked(eventEmitter: MatCheckboxChange, id: number): void {
    if (!this.orderStatusOption.some(x => x.checked === true)) {
      eventEmitter.source.toggle();
      this.orderStatusOption[id].checked = true;
    }
  }


  onToggle(): void {
    const checkedOptions = this.orderStatusOption.filter(x => x.checked);
    this.orderFilter.orderStatus = checkedOptions[0].value;
    if (checkedOptions.length === 2) {
      this.orderFilter.orderStatus = OrderStatus.All;
    }
  }
}

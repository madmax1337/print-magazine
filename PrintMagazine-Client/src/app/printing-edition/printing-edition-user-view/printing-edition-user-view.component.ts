import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatPaginator, MatCheckboxChange } from '@angular/material';
import { Subscription } from 'rxjs';
import { PrintingEditionService, AccountService } from 'src/app/shared/services/index';
import { PaginationModel, PrintingEditionFilter, PrintingEditionModel, CheckBoxItem, PrintingEditionModelItem } from 'src/app/shared/models/index';
import { PrintingEditionType, SortColumType, SortType, Curency } from 'src/app/shared/enum/index';
import { MultiSelectItem } from 'src/app/shared/models/multi-select-model/multi-select-item';

@Component({
  selector: 'app-printing-edition-user-view',
  templateUrl: './printing-edition-user-view.component.html',
  styleUrls: ['./printing-edition-user-view.component.css']
})
export class PrintingEditionUserViewComponent {

  printingEditionFilter: PrintingEditionFilter;
  printingEditionModel: PrintingEditionModel;
  printingEditionTypeOption: Array<CheckBoxItem>;
  paginationModel: PaginationModel;
  curencyTypeOption: Array<MultiSelectItem>;
  sortTypeOption: Array<MultiSelectItem>;
  curencyType: string;
  subscriptionLogin: Subscription;
  isLoggedIn: boolean;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    public printingEditionService: PrintingEditionService,
    public router: Router,
    private accountService: AccountService) {

    this.printingEditionFilter = new PrintingEditionFilter();
    this.printingEditionModel = new PrintingEditionModel();
    this.paginationModel = new PaginationModel();
    this.curencyType = Curency.USD.toString();

    this.subscriptionLogin = this.accountService.getLoggedIn().subscribe((data: boolean) => {
      this.isLoggedIn = data;
    });

    this.getSortType();

    this.getPrintingEditionTypeOption();

    this.getCurencyTypeOption();

    this.getPrintingEditionData(this.paginationModel.pageSize, this.paginationModel.pageIndex);
  }

  getSortType(): void {
    this.sortTypeOption = new Array<MultiSelectItem>();
    this.sortTypeOption.push(new MultiSelectItem(SortType.asc, 'Low to Hight'));
    this.sortTypeOption.push(new MultiSelectItem(SortType.desc, 'Hight to Low'));
  }

  getInformation(printingEditionId: number): void {
    this.router.navigate(['printingEdition/printingEditionDetails', printingEditionId], { state: { data: { curency: this.curencyType } } });
  }

  logInAdmin(): void {
    this.router.navigate(['account/signIn'], { state: { data: { param: true } } });
  }

  getCurencyTypeOption(): void {
    this.curencyTypeOption = new Array<MultiSelectItem>();

    for (let type of Object.keys(Curency)) {
      let isValueProperty = parseInt(type, 10) >= 0;
      if (isValueProperty) {
        this.curencyTypeOption.push(new MultiSelectItem(type, Curency[type]));
      }
    }

  }

  getPrintingEditionTypeOption(): void {
    this.printingEditionTypeOption = new Array<CheckBoxItem>();

    for (let type of Object.keys(PrintingEditionType)) {
      let isValueProperty = parseInt(type, 10) >= 0;
      if (isValueProperty) {
        this.printingEditionTypeOption.push(new CheckBoxItem(type, PrintingEditionType[type], true));
      }
    }

  }

  getCurencyType(id: number): any {
    return Curency[id];
  }

  getType(id: number): any {
    return PrintingEditionType[id];
  }

  filterPrintingEdition(): void {
    this.paginator.pageIndex = this.paginationModel.pageIndex;
    this.printingEditionFilter.sortColumType = SortColumType.ItemPrice;
    this.getPrintingEditionData(this.paginator.pageSize, this.paginator.pageIndex);
  }

  onToggle(): void {
    const checkedOptions = this.printingEditionTypeOption.filter(x => !x.checked);
    this.printingEditionFilter.printingEditionTypes = checkedOptions.map(x => x.value);
  }

  getPrintingEditionData(pageSize: number, pageIndex: number): void {

    this.onToggle();

    this.printingEditionFilter.pageSize = pageSize;
    this.printingEditionFilter.pageIndex = pageIndex;
    this.printingEditionFilter.curency = Curency[this.curencyType];
    this.printingEditionService.getAll(this.printingEditionFilter).subscribe((data: PrintingEditionModel) => {

      if (data.errors.length === 0) {
        this.paginator.length = data.itemCount;
        this.printingEditionModel = data;
      }

      if (data.errors.length > 0) {
        this.printingEditionModel = data;
      }

    });
  }

  onChecked(eventEmitter: MatCheckboxChange, id: number): void {
    if (!this.printingEditionTypeOption.some(x => x.checked === true)) {
      eventEmitter.source.toggle();
      this.printingEditionTypeOption[id].checked = true;
    }
  }

}

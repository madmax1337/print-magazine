import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  MatTableModule,
  MatPaginatorModule,
  MatButtonModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSortModule,
  MatMenuModule,
  MatCheckboxModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatDialogModule,
  MatListModule,
  MatGridListModule,
  MatCardModule,
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PrintingEditionRoutes } from 'src/app/printing-edition/printing-edition.routing';
import { PrintingEditionAdminViewComponent } from 'src/app/printing-edition/printing-edition-admin-view/printing-edition-admin-view.component';
import { PrintingEditionUserViewComponent } from 'src/app/printing-edition/printing-edition-user-view/printing-edition-user-view.component';
import { DialogPrintingEditionComponent } from 'src/app/printing-edition/printing-edition-admin-view/dialog-printing-edition/dialog-printing-edition.component';
import {
  DialogDescriptyonPrintingEdfitionComponent
} from 'src/app/printing-edition/printing-edition-admin-view/dialog-descriptyon-printing-edfition/dialog-descriptyon-printing-edfition.component';
import { PrintingEditionDetailsComponent } from 'src/app/printing-edition/printing-edition-details/printing-edition-details.component';

@NgModule({
  imports: [
    CommonModule,
    PrintingEditionRoutes,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatFormFieldModule,
    MatTooltipModule,
    MatSortModule,
    MatMenuModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatDialogModule,
    MatListModule,
    MatGridListModule,
    MatCardModule
  ],
  declarations: [
    PrintingEditionAdminViewComponent,
    PrintingEditionUserViewComponent,
    DialogPrintingEditionComponent,
    DialogDescriptyonPrintingEdfitionComponent,
    PrintingEditionDetailsComponent
  ],
  entryComponents: [
    DialogPrintingEditionComponent,
    DialogDescriptyonPrintingEdfitionComponent
  ]
})
export class PrintingEditionModule { }

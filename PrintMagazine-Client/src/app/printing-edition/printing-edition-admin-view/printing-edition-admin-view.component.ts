import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatDialog, Sort } from '@angular/material';
import { Router } from '@angular/router';
import { PrintingEditionModelItem } from 'src/app/shared/models/index';
import { PrintingEditionService, AccountService } from 'src/app/shared/services/index';
import { DialogDeleteItemComponent } from 'src/app/shared/components/dialog-delete-item/dialog-delete-item.component';
import { DialogPrintingEditionComponent } from 'src/app/printing-edition/printing-edition-admin-view/dialog-printing-edition/dialog-printing-edition.component';
import {
  DialogDescriptyonPrintingEdfitionComponent
} from 'src/app/printing-edition/printing-edition-admin-view/dialog-descriptyon-printing-edfition/dialog-descriptyon-printing-edfition.component';
import { PrintingEditionUserViewComponent } from '../printing-edition-user-view/printing-edition-user-view.component';
import { TableConstant, EmptyStringConstants } from 'src/app/shared/constants/index';
import { SortType, SortColumType } from 'src/app/shared/enum/index';

@Component({
  selector: 'app-printing-edition-admin-view',
  templateUrl: './printing-edition-admin-view.component.html',
  styleUrls: ['./printing-edition-admin-view.component.css']
})
export class PrintingEditionAdminViewComponent extends PrintingEditionUserViewComponent {

  displayedColumns: string[];


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    printingEditionService: PrintingEditionService,
    router: Router,
    accountService: AccountService,
    private tableConstatn: TableConstant,
    private emptyStringConstant: EmptyStringConstants,
    public dialog: MatDialog) {

    super(printingEditionService, router, accountService);

    this.displayedColumns = this.tableConstatn.displayedColumPrintingEdition;

    this.getPrintingEditionTypeOption();
    this.getPrintingEditionData(this.paginationModel.pageSize, this.paginationModel.pageIndex);

  }


  descriptionDialogOpen(description: string): void {
    const dialogRef = this.dialog.open(DialogDescriptyonPrintingEdfitionComponent, { data: description });
  }

  sortDataPrintingEdition(sortData: Sort): void {
    if (sortData.active || sortData.direction !== this.emptyStringConstant.emptyString) {
      this.printingEditionFilter.sortColumType = SortColumType[sortData.active];
      this.printingEditionFilter.sortType = SortType[sortData.direction];
    }

    this.paginator.pageIndex = this.paginationModel.pageIndex;
    this.getPrintingEditionData(this.paginator.pageSize, this.paginator.pageIndex);
  }

  addPrintingEdition(): void {
    const dialogRef = this.dialog.open(DialogPrintingEditionComponent, {});

    dialogRef.afterClosed().subscribe((result: PrintingEditionModelItem) => {
      if (result != null) {
        this.printingEditionService.create(result).subscribe((data: PrintingEditionModelItem) => {
          if (data.errors.length > 0) {
            this.printingEditionModel.errors = data.errors;
            return;
          }
          this.filterPrintingEdition();
        });
      }
    });

  }

  updatePrintingEdition(element: PrintingEditionModelItem): void {

    const dialogRef = this.dialog.open(DialogPrintingEditionComponent, {
      data: {
        printingEditionId: element.printingEditionId,
        title: element.title,
        description: element.description,
        authors: element.authors,
        price: element.price,
        type: element.type,
        curency: element.curency,
      }
    });

    dialogRef.afterClosed().subscribe((result: PrintingEditionModelItem) => {
      if (result != null) {
        this.printingEditionService.update(result).subscribe((data: PrintingEditionModelItem) => {
          if (data.errors.length > 0) {
            this.printingEditionModel.errors = data.errors;
            return;
          }
          this.filterPrintingEdition();
        });
      }
    });

  }

  delete(element: PrintingEditionModelItem): void {

    const dialogRef = this.dialog.open(DialogDeleteItemComponent, {
        data: true
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result == true) {
        element.isRemove = true;
        this.printingEditionService.delete(element).subscribe((data: PrintingEditionModelItem) => {
          if (data.errors.length > 0) {
            this.printingEditionModel.errors = data.errors;
            return;
          }
          this.filterPrintingEdition();
        });
      }
    });

  }
}

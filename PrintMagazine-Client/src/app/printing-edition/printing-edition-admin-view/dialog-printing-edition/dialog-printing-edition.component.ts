import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthorModel, PrintingEditionModelItem, PrintingEditionAuthor, AuthorModelItem } from 'src/app/shared/models/index';
import { MultiSelectItem } from 'src/app/shared/models/multi-select-model/multi-select-item';
import { AuthorService } from 'src/app/shared/services/index';
import { PrintingEditionType, Curency } from 'src/app/shared/enum/index';

@Component({
  selector: 'app-dialog-printing-edition',
  templateUrl: './dialog-printing-edition.component.html',
  styleUrls: ['./dialog-printing-edition.component.css']
})
export class DialogPrintingEditionComponent {

  categoryTypeOption: Array<MultiSelectItem>;
  curencyTypeOption: Array<MultiSelectItem>;
  authorOption: Array<PrintingEditionAuthor>;
  printingEditionForm: FormGroup;
  printingEditionTest: PrintingEditionAuthor[];
  curency: string;
  type: string;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<DialogPrintingEditionComponent>,
    private authorService: AuthorService,
    @Inject(MAT_DIALOG_DATA) public printingEdition: PrintingEditionModelItem) {

    this.getAuthor();

    this.getCategoryTypeOption();
    this.getCurencyTypeOption();

    this.initializationData();

    this.formGroup();
  }

  initializationData() {
    if (this.printingEdition != null) {
      this.curency = this.printingEdition.curency.toString();
      this.type = this.printingEdition.type.toString();
    }

    if (this.printingEdition == null) {
      this.printingEdition = new PrintingEditionModelItem();
    }
  }

  getAuthor(): void {
    this.authorService.getAuthorForPrintingEdition().subscribe((data: AuthorModel) => {
      if (data.errors.length === 0) {
        this.getAuthorOption(data);
      }
    });
  }

  compareFunction(authorFirst: PrintingEditionAuthor, authorSecond: PrintingEditionAuthor): boolean {
    return authorFirst && authorSecond ? authorFirst.authorId === authorSecond.authorId : authorFirst === authorSecond;
  }

  getCategoryTypeOption(): void {
    this.categoryTypeOption = new Array<MultiSelectItem>();
    for (let type of Object.keys(PrintingEditionType)) {
      let isValueProperty = parseInt(type, 10) >= 0;
      if (isValueProperty) {
        this.categoryTypeOption.push(new MultiSelectItem(type, PrintingEditionType[type]));
      }
    }
  }

  getCurencyTypeOption(): void {
    this.curencyTypeOption = new Array<MultiSelectItem>();
    for (let type of Object.keys(Curency)) {
      let isValueProperty = parseInt(type, 10) >= 0;
      if (isValueProperty) {
        this.curencyTypeOption.push(new MultiSelectItem(type, Curency[type]));
      }
    }
  }

  getAuthorOption(author: AuthorModel): void {
    this.authorOption = new Array<PrintingEditionAuthor>();
    for (let i = 0; i < author.items.length; i++) {
      this.authorOption.push(new PrintingEditionAuthor(author.items[i].authorId, author.items[i].name));
    }
  }

  checkAuthorSelectedState(author: AuthorModelItem): boolean {
    if (this.printingEdition.authors != null) {
      return this.printingEdition.authors.some(x => x.authorId === author.authorId);
    }
    return false;
  }

  formGroup(): void {
    this.printingEditionForm = this.formBuilder.group({
      title: new FormControl(null, [Validators.required]),
      descriptyon: new FormControl(null, [Validators.required]),
      category: new FormControl(null, [Validators.required]),
      author: new FormControl(null, [Validators.required]),
      price: new FormControl(null, [Validators.required]),
      curency: new FormControl(null, [Validators.required])
    });
  }

  submit(): void {
    this.printingEdition.curency = Curency[this.curency];
    this.printingEdition.type = PrintingEditionType[this.type];
  }

  closeWindow(): void {
    this.dialogRef.close();
  }
}

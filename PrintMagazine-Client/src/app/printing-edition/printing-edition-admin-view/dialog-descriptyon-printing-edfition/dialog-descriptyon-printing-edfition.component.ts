import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-descriptyon-printing-edfition',
  templateUrl: './dialog-descriptyon-printing-edfition.component.html',
  styleUrls: ['./dialog-descriptyon-printing-edfition.component.css']
})
export class DialogDescriptyonPrintingEdfitionComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public description: string) { }

}

import { Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { PrintingEditionModelItem, CartModelItem, CartModel } from 'src/app/shared/models/index';
import { Curency } from 'src/app/shared/enum/index';
import { MultiSelectItem } from 'src/app/shared/models/multi-select-model/multi-select-item';
import { PrintingEditionService, CartService, AccountService } from 'src/app/shared/services/index';


@Component({
  selector: 'app-printing-edition-details',
  templateUrl: './printing-edition-details.component.html',
  styleUrls: ['./printing-edition-details.component.css']
})
export class PrintingEditionDetailsComponent {

  printingEditionModelItem: PrintingEditionModelItem;
  cartModelItem: CartModelItem;
  curencyType: string;
  curency: Curency;
  curencyTypeOption: Array<MultiSelectItem>;
  subscription: Subscription;
  subscriptionCart: Subscription;
  cartModel: CartModel;
  isAdding: boolean;
  subscriptionLogin: Subscription;
  isLoggedIn: boolean;

  constructor(
    private printingEditionService: PrintingEditionService,
    private route: ActivatedRoute,
    private cartService: CartService,
    private accountService: AccountService) {

    this.printingEditionModelItem = new PrintingEditionModelItem();
    this.cartModelItem = new CartModelItem();
    this.curencyType = Curency.USD.toString();
    this.cartModel = new CartModel();
    this.curencyTypeOption = new Array<MultiSelectItem>();

    this.getSubscription();

    this.getInformation();
    this.getCurencyTypeOption();
  }

  getSubscription(): void {
    this.subscription = this.route.params.subscribe((params: Params) => {
      this.printingEditionModelItem.printingEditionId = params.id;
    });

    this.subscriptionCart = this.cartService.getCartSubject().subscribe((data: CartModelItem[]) => {
      this.isAdding = this.cartService.checkItem(this.printingEditionModelItem.printingEditionId, data);
    });

    this.subscriptionLogin = this.accountService.getLoggedIn().subscribe((data: boolean) => {
      this.isLoggedIn = data;
    });
  }

  getState(): void {
    if (history.state.data != null) {
      this.curencyType = history.state.data.curency;
    }
  }

  getInformation(): void {
    this.curency = Curency[this.curencyType];
    this.printingEditionService.getInformation(this.printingEditionModelItem.printingEditionId, this.curency).subscribe((data: PrintingEditionModelItem) => {
      if (data.errors.length === 0) {
        this.printingEditionModelItem = data;
      }
    });
  }

  getCurencyType(curencyType: any): string {
    return Curency[curencyType];
  }

  getCurencyTypeOption(): void {
    for (const type of Object.keys(Curency)) {
      const isValueProperty = parseInt(type, 10) >= 0;
      if (isValueProperty) {
        this.curencyTypeOption.push(new MultiSelectItem(type, Curency[type]));
      }
    }
  }

  addToCrard(): void {
    this.cartModelItem.printingEditionId = this.printingEditionModelItem.printingEditionId;
    this.cartModelItem.curency = Curency.USD;
    this.cartModelItem.orderAmount = this.converToUsd(this.printingEditionModelItem.price * this.cartModelItem.count, Curency[this.curencyType]);
    this.cartModelItem.product = this.printingEditionModelItem.title;
    this.cartModelItem.price = this.converToUsd(this.printingEditionModelItem.price, Curency[this.curencyType]);
    this.isAdding = true;
    this.cartService.addToCart(this.cartModelItem);
  }

  converToUsd(price: number, curency: Curency): number {

    let curencyPrice = [];
    curencyPrice.push({ key: Curency.EUR, value: 0.91138 });
    curencyPrice.push({ key: Curency.GBR, value: 0.81060 });
    curencyPrice.push({ key: Curency.UAH, value: 24.77398 });

    for (let item of curencyPrice) {
      if (item.key === Curency[curency]) {
        price = price / item.value;
      }
    }
    return price;
  }
}

import { Routes, RouterModule } from '@angular/router';
import { PrintingEditionAdminViewComponent } from 'src/app/printing-edition/printing-edition-admin-view/printing-edition-admin-view.component';
import { PrintingEditionUserViewComponent } from 'src/app/printing-edition/printing-edition-user-view/printing-edition-user-view.component';
import { AuthGuard, AuthUserGuard } from 'src/app/shared/helper/index';
import { PrintingEditionDetailsComponent } from 'src/app/printing-edition/printing-edition-details/printing-edition-details.component';

const routes: Routes = [
  { path: 'printingEditionAdmin', component: PrintingEditionAdminViewComponent, canActivate: [AuthGuard], data: {role: 'Admin'} },
  { path: 'printingEditionUser', component: PrintingEditionUserViewComponent, canActivate: [AuthUserGuard]},
  { path: 'printingEditionDetails/:id', component: PrintingEditionDetailsComponent, canActivate: [AuthUserGuard]}
];

export const PrintingEditionRoutes = RouterModule.forChild(routes);

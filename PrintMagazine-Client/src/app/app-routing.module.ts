import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from 'src/app/shared/components/not-found/not-found.component';


const routes: Routes = [
  {path: 'account' , loadChildren: () => import('src/app/account/account.module').then(a => a.AccountModule)},
  {path: 'printingEdition',
  loadChildren: () => import('src/app/printing-edition/printing-edition.module').then(p => p.PrintingEditionModule)},
  {path: 'user', loadChildren: () => import('src/app/user/user.module').then(u => u.UserModule)},
  {path: 'order', loadChildren: () => import('src/app/order/order.module').then(o => o.OrderModule)},
  {path: 'author', loadChildren: () => import('src/app/author/author.module').then(a => a.AuthorModule)},
  {path: '', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, Subscription } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { AccountService, LocalStorageService } from 'src/app/shared/services/index';
import { RememberMeModel } from 'src/app/shared/models/index';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  private isRefreshing: boolean;
  private rememberMeModel: RememberMeModel;
  private isLogIn: boolean;
  private subscriptionLogin: Subscription;
  constructor(
    private cookiesService: CookieService,
    private accountService: AccountService,
    private localStorageService: LocalStorageService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const accessToken = this.cookiesService.get('accessToken');
    const refreshToken = this.cookiesService.get('refreshToken');
    this.rememberMeModel = new RememberMeModel();
    this.subscriptionLogin = this.accountService.getLoggedIn().subscribe((data: boolean) => {
      this.isLogIn = data;
    });

    if (this.cookiesService.get('rememberMe')) {
      this.rememberMeModel = JSON.parse(this.cookiesService.get('rememberMe'));
    }

    if (accessToken) {
      request = this.addToken(request, accessToken);
    }

    if (!this.cookieCheck(this.rememberMeModel.userId) && this.isLogIn) {
      this.accountService.logOut();
    }

    return next.handle(request).pipe(catchError(error => {
      if (error instanceof HttpErrorResponse && error.status === 401) {
        if (refreshToken) {
          return this.handle401Error(request, next);
        }
        this.accountService.logOut();
        return throwError('Token Expire');
      }
      return throwError(error);
    }));
  }

  private cookieCheck(userId: string): boolean {
    if (userId || userId === this.localStorageService.UserId) {
      return true;
    }
    return false;
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      return this.accountService.refreshToken().pipe(
        switchMap((token: any) => {
          token = this.cookiesService.get('accessToken');

          if (this.rememberMeModel.rememberMe === true) {
            this.rememberMeFunction(this.rememberMeModel.userId, this.rememberMeModel.rememberMe, 720);
          }

          if (this.rememberMeModel.rememberMe === false) {
            this.rememberMeFunction(this.rememberMeModel.userId, this.rememberMeModel.rememberMe, 12);
          }

          this.isRefreshing = false;
          return next.handle(this.addToken(request, token));
        }
        ));
    }
  }

  private rememberMeFunction(userId: string, rememberMe: boolean, hours: number): void {

    let expire = new Date();
    let time = Date.now() + ((3600 * 1000) * hours);

    expire.setTime(time);
    this.rememberMeModel.rememberMe = rememberMe;
    this.rememberMeModel.userId = userId;
    this.cookiesService.set('rememberMe', JSON.stringify(this.rememberMeModel), expire, '/');
  }

  private addToken(request: HttpRequest<any>, token: string): HttpRequest<any> {
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${token}`
      }
    });
  }

}

export enum SortColumType {
  ItemId = 0,
  ItemPrice = 1,
  Date = 2
}

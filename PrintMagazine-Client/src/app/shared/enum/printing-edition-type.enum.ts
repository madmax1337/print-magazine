export enum PrintingEditionType {
  Book = 0,
  Magazines = 1,
  Newspaper = 2
}

export class RememberMeModel {

  userId: string;
  rememberMe: boolean;

}

import { PrintingEditionType, SortColumType, Curency } from 'src/app/shared/enum/index';
import { BaseFilter } from 'src/app/shared/models/filter/base-filter/base-filter';

export class PrintingEditionFilter extends BaseFilter {

  minPrice = 0;
  maxPrice = 100000;
  printingEditionTypes: PrintingEditionType[];
  sortColumType: SortColumType;
  curency: Curency;

}

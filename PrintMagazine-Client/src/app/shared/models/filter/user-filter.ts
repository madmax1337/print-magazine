import { LockoutStatus } from 'src/app/shared/enum/index';
import { BaseFilter } from 'src/app/shared/models/filter/base-filter/base-filter';

export class UserFilter extends BaseFilter {

  lockoutStatus: LockoutStatus;

}

import { SortType } from 'src/app/shared/enum/index';

export class BaseFilter {

  pageSize: number;
  pageIndex: number;
  serchString: string;
  sortType: SortType;

}

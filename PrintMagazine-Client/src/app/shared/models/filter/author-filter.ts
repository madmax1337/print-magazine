import { SortColumType } from 'src/app/shared/enum/index';
import { BaseFilter } from 'src/app/shared/models/filter/base-filter/base-filter';

export class AuthorFilter extends BaseFilter {

  sortColumType: SortColumType;

}

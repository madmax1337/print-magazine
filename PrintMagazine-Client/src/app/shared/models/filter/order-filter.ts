import { OrderStatus, SortColumType } from 'src/app/shared/enum/index';
import { BaseFilter } from 'src/app/shared/models/filter/base-filter/base-filter';

export class OrderFilter extends BaseFilter {

  userId: string;
  orderStatus: OrderStatus;
  sortColumType: SortColumType;

}

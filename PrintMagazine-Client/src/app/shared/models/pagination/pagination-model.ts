export class PaginationModel {

  length = 0;
  pageSize = 3;
  pageIndex = 0;
  pageSizeOptions: number[] = [3, 6, 9, 12, 15];

}

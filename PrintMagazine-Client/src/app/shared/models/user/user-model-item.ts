import { BaseModel } from 'src/app/shared/models/base/base-model';

export class UserModelItem extends BaseModel {

  userId: string;
  userName: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  newPassword: string;
  isLockout: boolean;
  isRemove: boolean;
  role: string;
  image: string;

}

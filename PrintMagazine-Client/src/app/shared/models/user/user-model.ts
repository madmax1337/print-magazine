import { BaseModel } from 'src/app/shared/models/base/base-model';
import { UserModelItem } from 'src/app/shared/models/user/user-model-item';

export class UserModel extends BaseModel {

  itemCount: number;
  items: UserModelItem[];

}

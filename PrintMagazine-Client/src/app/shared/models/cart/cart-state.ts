import { CartModelItem } from 'src/app/shared/models/cart/cart-model-item';

export class CartState {

  loaded: boolean;
  items: CartModelItem[];

}

import { Curency } from 'src/app/shared/enum/index';

export class CartModelItem {

  printingEditionId: number;
  product: string;
  curency: Curency;
  price: number;
  count = 1;
  orderAmount: number;

}

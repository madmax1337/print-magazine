import { BaseModel } from 'src/app/shared/models/base/base-model';
import { CartModelItem } from 'src/app/shared/models/cart/cart-model-item';

export class CartModel extends BaseModel {

  items: CartModelItem[];
  transactionId: string;
  orderAmount: number;

}

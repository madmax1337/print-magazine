export class MultiSelectItem {

  id: number;
  label: string;

  constructor(id: any, label: any) {
    this.id = id;
    this.label = label;
  }

}

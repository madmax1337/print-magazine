import { BaseModel } from 'src/app/shared/models/base/base-model';
import { AuthorModelItem } from 'src/app/shared/models/author/author-model-item';


export class AuthorModel extends BaseModel {

  itemCount: number;
  items: AuthorModelItem[];

}

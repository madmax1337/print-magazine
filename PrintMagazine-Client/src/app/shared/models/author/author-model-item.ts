import { BaseModel } from 'src/app/shared/models/base/base-model';

export class AuthorModelItem extends BaseModel {

  authorId: number;
  name: string;
  productTitles: string[];
  isRemove: boolean;

}

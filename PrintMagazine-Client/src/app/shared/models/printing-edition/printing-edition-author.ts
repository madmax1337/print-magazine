export class PrintingEditionAuthor {

  authorId: number;
  authorName: string;

  constructor(authorId: any, authorName: any) {
    this.authorId = authorId;
    this.authorName = authorName;
  }

}

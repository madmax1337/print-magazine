import { Curency, PrintingEditionType, StatusType } from 'src/app/shared/enum/index';
import { BaseModel } from 'src/app/shared/models/base/base-model';
import { PrintingEditionAuthor } from 'src/app/shared/models/printing-edition/printing-edition-author';

export class PrintingEditionModelItem extends BaseModel {

  printingEditionId: number;
  title: string;
  description: string;
  price: number;
  status: StatusType;
  curency: Curency;
  type: PrintingEditionType;
  authors: PrintingEditionAuthor[];
  isRemove: boolean;

}

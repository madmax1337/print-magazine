import { OrderStatus } from 'src/app/shared/enum/index';
import { OrderItemDataModel } from 'src/app/shared/models/order-item/order-item-data-model';
import { BaseModel } from 'src/app/shared/models/base/base-model';

export class OrderModelItem extends BaseModel {

  orderId: number;
  date: Date;
  userName: string;
  email: string;
  orderItems: OrderItemDataModel[];
  orderAmount: number;
  orderSatus: OrderStatus;
  transactionId: string;

}

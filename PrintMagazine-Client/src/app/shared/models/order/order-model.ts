import { BaseModel } from 'src/app/shared/models/base/base-model';
import { OrderModelItem } from 'src/app/shared/models/order/order-model-item';


export class OrderModel extends BaseModel {

  itemCount: number;
  items: OrderModelItem[];

}

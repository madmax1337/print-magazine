import { PrintingEditionType } from 'src/app/shared/enum/index';

export class OrderItemDataModel {

  printingEditionType: PrintingEditionType;
  title: string;
  count: number;
  amount: number;

}

import { Injectable } from '@angular/core';

@Injectable()

export class NavigateConstants {
  readonly signIn = '/account/signIn';
  readonly printingEditionAdmin = '/printingEdition/printingEditionAdmin';
  readonly printingEditionUser = '/printingEdition/printingEditionUser';
}

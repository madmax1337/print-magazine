import { Injectable } from '@angular/core';

@Injectable()

export class PatternConstants {
  readonly namePattern = '^[A-Za-z]+(?:[ _-][A-Za-z]+)*$';
  readonly emailPattern = '^[A-Za-z]{1}[A-Za-z0-9_-]*@[A-Za-z]{1}[A-Za-z-]*\.[a-z]{2,3}$';
  readonly passwordPattern = '^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$';
  readonly authorNamePattern = '^[A-Za-z]+(?:[ -][A-Za-z]+)*$';
}

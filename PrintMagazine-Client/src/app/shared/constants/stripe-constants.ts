import { Injectable } from '@angular/core';

@Injectable()

export class StripeConstants {
  readonly stripeScript = 'stripe-script';
  readonly scriptType = 'text/javascript';
  readonly scriptSrc = 'https://checkout.stripe.com/checkout.js';
  readonly stripeKey = 'pk_test_rkyJ6Ga03PyAJj3Za8ykRlEV00VElPJZka';
}

import { Injectable } from '@angular/core';

@Injectable()

export class TableConstant {
  readonly displayColumAuthor: string[] = ['author-id', 'name', 'product', 'crud'];
  readonly displayColumOrderAdmin: string[] = ['order-id', 'date', 'user-name', 'email', 'product', 'title', 'count', 'order-amout', 'status'];
  readonly displayColumOrderUser: string[] = ['order-id', 'date', 'product', 'title', 'count', 'order-amout', 'status'];
  readonly displayedColumPrintingEdition: string[] = ['printing-edition-id', 'name', 'description', 'category', 'author', 'price', 'crud'];
  readonly displayedColumCart: string[] = ['product', 'unit-price', 'count', 'order-amouth', 'crud'];
  readonly displayedColumUser: string[] = ['user-name', 'email', 'status', 'crud'];
}

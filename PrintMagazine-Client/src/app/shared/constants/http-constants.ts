import { Injectable } from '@angular/core';

@Injectable()

export class HttpConstants {
  readonly accountHttp = 'https://localhost:44313/api/account/';
  readonly authorHttp = 'https://localhost:44313/api/author/';
  readonly orderHttp = 'https://localhost:44313/api/order/';
  readonly  printingEditionHttp = 'https://localhost:44313/api/printingEdition/';
  readonly userHttp = 'https://localhost:44313/api/user/';
}

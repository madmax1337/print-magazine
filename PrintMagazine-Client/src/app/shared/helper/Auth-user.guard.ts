import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate
} from '@angular/router';
import { LocalStorageService } from 'src/app/shared/services/index';

@Injectable({ providedIn: 'root' })

export class AuthUserGuard implements CanActivate {

  constructor(public localStorageService: LocalStorageService, public router: Router) { }

  canActivate(): boolean {
    if (this.localStorageService.Role === 'Admin') {
      return false;
    }
    return true;
  }
}

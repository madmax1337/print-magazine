import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';
import { LocalStorageService, AccountService } from 'src/app/shared/services/index';

@Injectable({ providedIn: 'root' })

export class AuthGuard implements CanActivate {

  constructor(public localStorageService: LocalStorageService, public accountService: AccountService, public router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (route.data.role && this.localStorageService.UserName !== null) {
      let role = route.data.role;
      return role === this.localStorageService.Role;
    }

    if (this.localStorageService.UserName !== null) {
      return true;
    }

    this.accountService.logOut();
    this.router.navigate(['account/signIn']);
    return false;
  }
}

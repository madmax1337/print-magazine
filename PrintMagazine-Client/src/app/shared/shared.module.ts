import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatIconModule,
  MatToolbarModule,
  MatMenuModule,
  MatButtonModule,
  MatDialogModule,
  MatTableModule,
  MatFormFieldModule,
  MatBadgeModule,
  MatInputModule} from '@angular/material';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HeaderComponent } from 'src/app/shared/components/header/header.component';
import { AccountService } from 'src/app/shared/services/index';
import { DialogDeleteItemComponent } from 'src/app/shared/components/dialog-delete-item/dialog-delete-item.component';
import {
  PatternConstants,
  HttpConstants,
  NavigateConstants,
  StripeConstants,
  TableConstant,
  EmptyStringConstants } from 'src/app/shared/constants/index';
import { NotFoundComponent } from 'src/app/shared/components/not-found/not-found.component';
import { DialogCartItemComponent } from 'src/app/shared/components/dialog-cart-item/dialog-cart-item.component';
import { DialogAfterPayComponent } from 'src/app/shared/components/dialog-after-pay/dialog-after-pay.component';
import { DialogLogoutComponent } from 'src/app/shared/components/dialog-logout/dialog-logout.component';



@NgModule({
  declarations: [
    HeaderComponent,
    DialogDeleteItemComponent,
    NotFoundComponent,
    DialogCartItemComponent,
    DialogAfterPayComponent,
    DialogLogoutComponent
 ],
  imports: [
    CommonModule,
    MatIconModule,
    MatToolbarModule,
    MatMenuModule,
    MatButtonModule,
    RouterModule,
    MatDialogModule,
    MatTableModule,
    MatFormFieldModule,
    MatBadgeModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule
  ],
  exports : [HeaderComponent],
  providers: [
    AccountService,
    PatternConstants,
    HttpConstants,
    NavigateConstants,
    StripeConstants,
    TableConstant,
    EmptyStringConstants
  ],
  entryComponents: [
    DialogDeleteItemComponent,
    DialogCartItemComponent,
    DialogAfterPayComponent,
    DialogLogoutComponent
  ]
})
export class SharedModule { }

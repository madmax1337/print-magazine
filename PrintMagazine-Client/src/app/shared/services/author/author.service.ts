import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthorFilter, AuthorModel, AuthorModelItem } from 'src/app/shared/models/index';
import { HttpConstants } from 'src/app/shared/constants/index';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {
constructor(
  private httpClient: HttpClient,
  private httpConstants: HttpConstants) { }

getAll(authorFilter: AuthorFilter): Observable<AuthorModel> {
  return this.httpClient.post<AuthorModel>(this.httpConstants.authorHttp + 'viewAll', authorFilter);
}

update(authorModelItem: AuthorModelItem): Observable<AuthorModelItem> {
  return this.httpClient.post<AuthorModelItem>(this.httpConstants.authorHttp + 'update', authorModelItem);
}

delete(authorModelItem: AuthorModelItem): Observable<AuthorModelItem> {
  return this.httpClient.post<AuthorModelItem>(this.httpConstants.authorHttp + 'delete', authorModelItem);
}

create(authorModelItem: AuthorModelItem): Observable<AuthorModelItem> {
  return this.httpClient.post<AuthorModelItem>(this.httpConstants.authorHttp + 'create', authorModelItem);
}

getAuthorForPrintingEdition(): Observable<AuthorModel> {
  return this.httpClient.get<AuthorModel>(this.httpConstants.authorHttp + 'authorForPrintingEdition');
}
}

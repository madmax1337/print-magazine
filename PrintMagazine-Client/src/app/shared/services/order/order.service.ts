import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OrderFilter, OrderModel, CartModel, OrderModelItem, BaseModel } from 'src/app/shared/models/index';
import { Observable } from 'rxjs';
import { HttpConstants } from 'src/app/shared/constants/index';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

constructor(private httpClient: HttpClient, private httpConstants: HttpConstants) {

 }
 getAll(orderFilter: OrderFilter): Observable<OrderModel> {
  return this.httpClient.post<OrderModel>(this.httpConstants.orderHttp + 'viewAll', orderFilter);
}

createOrder(cartModel: CartModel): Observable<OrderModelItem> {
  return this.httpClient.post<OrderModelItem>(this.httpConstants.orderHttp + 'buyPrintingEdition', cartModel);
}

updateOrder(orderModelItem: OrderModelItem): Observable<BaseModel> {
  return this.httpClient.post<BaseModel>(this.httpConstants.orderHttp + 'update', orderModelItem);
}

}

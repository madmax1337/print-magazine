import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserModelItem, BaseModel, UserModel, UserFilter } from 'src/app/shared/models/index';
import { HttpConstants } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {

constructor(
  private httpClient: HttpClient,
  private httpConstants: HttpConstants) { }

update(userModelItem: UserModelItem): Observable<BaseModel> {
  return this.httpClient.post<BaseModel>(this.httpConstants.userHttp + 'update', userModelItem);
}

delete(userModelItem: UserModelItem): Observable<BaseModel> {
  return this.httpClient.post<BaseModel>(this.httpConstants.userHttp + 'delete', userModelItem);
}

getAll(userFilter: UserFilter): Observable<UserModel> {
  return this.httpClient.post<UserModel>(this.httpConstants.userHttp + 'viewAll', userFilter);
}

setLockout(userModelItem: UserModelItem): Observable<BaseModel> {
  return this.httpClient.post<BaseModel>(this.httpConstants.userHttp + 'setLockout', userModelItem);
}

}

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LocalStorageService } from 'src/app/shared/services/local-storage/local-storage.service';
import { CartModel, CartModelItem } from 'src/app/shared/models/index';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cartModel: CartModel;
  cartSubject: BehaviorSubject<CartModelItem[]>;

  constructor(private localStorageService: LocalStorageService) {
    this.cartModel = new CartModel();
    this.cartSubject = new BehaviorSubject<CartModelItem[]>([]);
  }

  loadCart(): CartModel {
    this.cartModel.items = [];
    if (this.localStorageService.Cart) {
      this.cartModel.items = JSON.parse(this.localStorageService.Cart);
      return this.cartModel;
    }
    return this.cartModel;
  }

  deleteItem(id: number): CartModel {
    for (let i = 0; i < this.cartModel.items.length; i++) {
      if (this.cartModel.items[i].printingEditionId === id) {
        this.cartModel.items.splice(i, 1);
        break;
      }
    }
    this.localStorageService.setItem('cart', JSON.stringify(this.cartModel.items));
    this.cartSubject.next(this.cartModel.items);
    return this.loadCart();
  }

  editItem(element: CartModelItem): CartModel {
    this.cartModel.items.find(x => x.printingEditionId === element.printingEditionId).orderAmount = element.price * element.count;
    this.localStorageService.setItem('cart', JSON.stringify(this.cartModel.items));
    return this.loadCart();
  }

  orderAmouth(): number {
    let totalPrice = 0;
    for (let i = 0; i < this.cartModel.items.length; i++) {
      totalPrice += this.cartModel.items[i].orderAmount;
    }
    return totalPrice;
  }

  checkItem(id: number, data: CartModelItem[]): boolean {
    if (data.length === 0) {
      data = this.localStorageService.Cart ? JSON.parse(this.localStorageService.Cart) : [];
    }
    if (data.some(x => x.printingEditionId == id)) {
      return true;
    }
    return false;
  }

  addToCart(cartModelItem: CartModelItem): void {

    this.cartModel.items = [];

    if (this.localStorageService.Cart) {
      this.cartModel.items = JSON.parse(this.localStorageService.Cart);
    }

    this.cartModel.items.push(cartModelItem);
    this.cartSubject.next(this.cartModel.items);
    this.localStorageService.setItem('cart', JSON.stringify(this.cartModel.items));
  }

  getCartSubject(): Observable<CartModelItem[]> {
    return this.cartSubject.asObservable();
  }
}

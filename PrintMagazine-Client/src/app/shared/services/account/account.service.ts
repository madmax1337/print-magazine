import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { SignInModel, UserModelItem, SignUpModel, BaseModel, ForgotPasswordModel } from 'src/app/shared/models/index';
import { LocalStorageService } from 'src/app/shared/services/local-storage/local-storage.service';
import { HttpConstants } from 'src/app/shared/constants/index';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private loggedIn: BehaviorSubject<boolean>;


  constructor(
    private httpClient: HttpClient,
    private localStorageService: LocalStorageService,
    private cookiesService: CookieService,
    private httpConstants: HttpConstants,
    private router: Router) {
    this.loggedIn = new BehaviorSubject<boolean>(this.isAuntitificate());
  }

  signIn(signInModel: SignInModel): Observable<UserModelItem> {
    return this.httpClient.post<UserModelItem>(this.httpConstants.accountHttp + 'signIn', signInModel, { withCredentials: true });
  }

  signUp(signUpModel: SignUpModel): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(this.httpConstants.accountHttp + 'signUp', signUpModel);
  }

  forgotPassword(forgotPasswordModel: ForgotPasswordModel): Observable<BaseModel> {
    return this.httpClient.post<BaseModel>(this.httpConstants.accountHttp + 'forgotPassword', forgotPasswordModel);
  }

  refreshToken(): Observable<BaseModel> {
    return this.httpClient.get<BaseModel>(this.httpConstants.accountHttp + 'refreshToken', { withCredentials: true });
  }

  logOut(): Observable<BaseModel> {

    this.localStorageService.clear();

    this.cookiesService.deleteAll('/');

    this.router.navigate(['account/signIn'], {state: {data : {param: false}}});

    this.loggedIn.next(this.isAuntitificate());

    return this.httpClient.get<BaseModel>(this.httpConstants.accountHttp + 'logOut');
  }

  getLoggedIn(): Observable<boolean> {
    return this.loggedIn.asObservable();
  }

  isAuntitificate(): boolean {
    return this.localStorageService.UserId !== null && this.localStorageService.UserName !== null;
  }

  NotifieSignInResult(): void {
    this.loggedIn.next(this.isAuntitificate());
  }
}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

constructor() { }

get Cart() {
  return localStorage.getItem('cart');
}

get UserId() {
  return localStorage.getItem('userId');
}
get UserName() {
 return localStorage.getItem('userName');
}
get FirstName() {
return  localStorage.getItem('firstName');
}
get LastName() {
  return localStorage.getItem('lastName');
}
get Email() {
  return localStorage.getItem('email');
}
get Role() {
  return localStorage.getItem('role');
}

get Image() {
  return localStorage.getItem('image');
}

setItem(key: string, value: string): void {
  localStorage.setItem(key, value);
}

removeCart(): void {
localStorage.removeItem('cart');
}

clear(): void {
  localStorage.clear();
}

}

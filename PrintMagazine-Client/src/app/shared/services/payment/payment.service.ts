import { Injectable, Predicate } from '@angular/core';
import { LocalStorageService } from 'src/app/shared/services/local-storage/local-storage.service';
import { OrderModelItem } from 'src/app/shared/models/index';
import { StripeConstants } from 'src/app/shared/constants/index';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  orderModelItem: OrderModelItem;

  constructor(
    private localStorageService: LocalStorageService,
    private stripeConstants: StripeConstants) { }

  loadStripe(): void {

    if (!window.document.getElementById('stripe-script')) {

      let script = window.document.createElement('script');

      script.id = this.stripeConstants.stripeScript;
      script.type = this.stripeConstants.scriptType;
      script.src = this.stripeConstants.scriptSrc;

      window.document.body.appendChild(script);
    }
  }

  pay(amount, callBack: Predicate<string>, orderId: number): void {

    let handler = (window as any).StripeCheckout.configure({
      key: this.stripeConstants.stripeKey,
      locale: 'auto',
      token: ((data: any) => {
        callBack(data.id);
      }),
    });
    handler.open({
      email: this.localStorageService.Email,
      amount: amount * 100,
    });

  }
}

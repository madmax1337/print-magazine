import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PrintingEditionModel, PrintingEditionFilter, PrintingEditionModelItem } from 'src/app/shared/models/index';
import { Curency } from 'src/app/shared/enum/index';
import { HttpConstants } from 'src/app/shared/constants/index';

@Injectable({
  providedIn: 'root'
})
export class PrintingEditionService {

  constructor(
    private httpClient: HttpClient,
    private httpConstants: HttpConstants) { }

  getAll(printingEditionFilter: PrintingEditionFilter): Observable<PrintingEditionModel> {
    return this.httpClient.post<PrintingEditionModel>(this.httpConstants.printingEditionHttp + 'viewAll', printingEditionFilter);
  }

  delete(printingEditionModelItem: PrintingEditionModelItem): Observable<PrintingEditionModelItem> {
    return this.httpClient.post<PrintingEditionModelItem>(this.httpConstants.printingEditionHttp + 'delete', printingEditionModelItem);
  }

  update(printingEditionModelItem: PrintingEditionModelItem): Observable<PrintingEditionModelItem> {
    return this.httpClient.post<PrintingEditionModelItem>(this.httpConstants.printingEditionHttp + 'update', printingEditionModelItem);
  }

  create(printingEditionModelItem: PrintingEditionModelItem): Observable<PrintingEditionModelItem> {
    return this.httpClient.post<PrintingEditionModelItem>(this.httpConstants.printingEditionHttp + 'create', printingEditionModelItem);
  }

  getInformation(id: number, curency: Curency): Observable<PrintingEditionModelItem> {
    return this.httpClient.get<PrintingEditionModelItem>(this.httpConstants.printingEditionHttp + 'getInformation?printingEditionId=' + id + '&curency=' + curency);
  }

}

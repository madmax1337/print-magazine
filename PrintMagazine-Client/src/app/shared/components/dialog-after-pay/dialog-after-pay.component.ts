import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-after-pay',
  templateUrl: './dialog-after-pay.component.html',
  styleUrls: ['./dialog-after-pay.component.css']
})
export class DialogAfterPayComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogAfterPayComponent>,
    @Inject(MAT_DIALOG_DATA) public data: number) {
  }

  closeWindow(): void {
    this.dialogRef.close();
  }

}

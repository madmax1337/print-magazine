import { Component, Predicate } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { CartModel, CartModelItem, OrderModelItem } from 'src/app/shared/models/index';
import { LocalStorageService, CartService, OrderService, PaymentService } from 'src/app/shared/services/index';
import { DialogAfterPayComponent } from 'src/app/shared/components/dialog-after-pay/dialog-after-pay.component';
import { TableConstant } from 'src/app/shared/constants/index';

@Component({
  selector: 'app-dialog-cart-item',
  templateUrl: './dialog-cart-item.component.html',
  styleUrls: ['./dialog-cart-item.component.css']
})
export class DialogCartItemComponent {

  private subscription: Subscription;
  displayedColumns: string[];
  cartModel: CartModel;
  totalPrice: number;
  emptyCart: boolean;
  handler: any;
  errorMsg: string[];
  haveError: boolean;
  orderId: number;
  orderConfirm: boolean;
  transactionId: string;
  callBack: Predicate<string>;

  constructor(
    private localStorageService: LocalStorageService,
    public dialogRef: MatDialogRef<DialogCartItemComponent>,
    private cartService: CartService,
    private orderService: OrderService,
    private tableConstant: TableConstant,
    public dialog: MatDialog,
    private paymentService: PaymentService) {

    this.cartModel = new CartModel();

    this.cartModel = this.cartService.loadCart();
    this.totalPrice = this.cartService.orderAmouth();
    this.displayedColumns = this.tableConstant.displayedColumCart;

    this.checkCart();

    this.paymentService.loadStripe();
    this.callBack = this.createOrder.bind(this);
  }

  checkCart(): void {
    if (this.cartModel.items.length === 0) {
      this.emptyCart = true;
    }
  }

  createOrder(paymentId: string): void {
    this.addOrder(paymentId);
  }

  deleteItem(id: number): void {
    this.cartModel = this.cartService.deleteItem(id);
    this.totalPrice = this.cartService.orderAmouth();

    if (this.cartModel.items.length === 0) {
      this.emptyCart = true;
    }

  }

  editItem(element: CartModelItem): void {
    this.cartModel = this.cartService.editItem(element);
    this.totalPrice = this.cartService.orderAmouth();
  }

  closeWindow(): void {
    this.dialogRef.close();
  }

  pay(amount): void {
    this.paymentService.pay(amount, this.callBack, this.orderId);
  }

  addOrder(transactionId: string): void {

    this.cartModel.items = JSON.parse(this.localStorageService.Cart);
    this.cartModel.transactionId = transactionId;
    this.cartModel.orderAmount = this.totalPrice;

    this.orderService.createOrder(this.cartModel).subscribe((data: OrderModelItem) => {
      if (data.errors.length === 0) {

        this.localStorageService.removeCart();
        this.cartModel.items = [];
        this.cartService.cartSubject.next(this.cartModel.items);
        this.orderId = data.orderId;
        this.orderConfirm = true;

        let dialogRef = this.dialog.open(DialogAfterPayComponent, { data: this.orderId });
        dialogRef.afterClosed().subscribe((result: number) => {
          if (result != 0) {
            this.dialogRef.close();
          }
        });

      }
      this.errorMsg = data.errors;
      this.haveError = true;
    });
  }

}

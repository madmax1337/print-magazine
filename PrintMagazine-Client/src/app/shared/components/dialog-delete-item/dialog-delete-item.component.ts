import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-delete-item',
  templateUrl: './dialog-delete-item.component.html',
  styleUrls: ['./dialog-delete-item.component.css']
})
export class DialogDeleteItemComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogDeleteItemComponent>,
    @Inject(MAT_DIALOG_DATA) public data: boolean) {
  }

  closeWindow(): void {
    this.dialogRef.close();
  }
}

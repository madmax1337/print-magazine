import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { AccountService, LocalStorageService, CartService } from 'src/app/shared/services/index';
import { DialogCartItemComponent } from 'src/app/shared/components/dialog-cart-item/dialog-cart-item.component';
import { DialogLogoutComponent } from 'src/app/shared/components/dialog-logout/dialog-logout.component';
import { CartModelItem } from '../../models';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {

  isLoggedIn: boolean;
  subscriptionLogin: Subscription;
  subscriptionCart: Subscription;
  role: string;
  userName: string;
  route: string;
  cartItemsLength: number;
  isEmptyCart: boolean;
  image: string;
  isPhotoEmpty: boolean;

  constructor(
    private accountService: AccountService,
    private localStorageService: LocalStorageService,
    public dialog: MatDialog,
    private cartService: CartService) {
  }

  ngOnInit() {
    this.getSubscription();
  }

  getSubscription(): void {
    this.subscriptionLogin = this.accountService.getLoggedIn().subscribe((data: boolean) => {
      this.isLoggedIn = data;
      this.UserInforMation();
      this.cartItemsLength = 0;
    });

    this.subscriptionCart = this.cartService.cartSubject.subscribe((data: CartModelItem[]) => {

      if (data.length !== 0) {
        this.cartItemsLength = data.length;
      }
      if (data.length === 0) {
        this.cartItemsLength = this.localStorageService.Cart ? JSON.parse(this.localStorageService.Cart).length : 0;
      }

    });
  }

  logOut(): void {
    const dialogRef = this.dialog.open(DialogLogoutComponent, { data: this.userName });
    dialogRef.afterClosed().subscribe((result: string) => {
      if (result != null) {
        this.accountService.logOut();
      }
    });
  }

  UserInforMation(): void {
    this.userName = this.localStorageService.UserName;
    this.role = this.localStorageService.Role;
    this.image = this.localStorageService.Image;
    this.isPhotoEmpty = !this.localStorageService.Image ? false : true;
  }

  openCart(): void {
    const dialogRef = this.dialog.open(DialogCartItemComponent, {});
    dialogRef.afterClosed().subscribe(() => {
      this.getSubscription();
    });
  }
}

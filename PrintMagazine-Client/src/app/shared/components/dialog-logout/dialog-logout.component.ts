import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-logout',
  templateUrl: './dialog-logout.component.html',
  styleUrls: ['./dialog-logout.component.css']
})
export class DialogLogoutComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogLogoutComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string) {
  }

  closeWindow(): void {
    this.dialogRef.close();
  }
}
